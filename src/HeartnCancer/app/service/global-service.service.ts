import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalServiceService {
  flag;
  constructor() {
    this.flag = {
      editScreen: {
        personalInfo: {
          removeWifeCover: true
        },
        sec2: false,
        sec3: false,
        sec4: false
      },
      cover: {
        SpouseCover: false,
        SpouseHeartCover: false,
        HeartCover: true,
        CancerCover: true
      },
      plusminus: {
        heart: false,
        cancer: false
      },
      benefits: {
        editshowIncrBenefit: false,
        editshowHospBenefit: false,
        editshowIncBenefit: false
      },
      headerDisabled: {
        showPages: false
      }
    };
  }
}
