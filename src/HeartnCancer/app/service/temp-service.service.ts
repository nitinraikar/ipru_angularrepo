import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { CookieService } from 'ngx-cookie-service';
import { WindowRefService } from 'src/Common/services/window-ref.service';
import { BlowfishEncryptionService } from 'src/Common/services/blowfish-encryption.service';

@Injectable({
  providedIn: "root"
})

export class TempServiceService {
  dateMask: any;
  HeartCancer_InputArr: any;
  dataLayerArray: any;
  datalayer: object;
  setPolicyTermDropdownprimary = [];
  premiumHeartCancerEbi;
  DOB;
  age;
  constants;
  Boundary_Conditions;
  Partnerage;
  premiumAmount;
  premium;
  sumAssured;
  sumAssuredJL;
  sumAssuredPL;
  totalSumAssuredWithSpouse;
  hideDiv = false;
  flag = {};
  userInput = {};
  SEM_WS_call_flag = 0;
  premiumToLanding: boolean = false;
  expires;
  heartSumAssured = 1000000;
  cancerSumAssured = 1000000;
  nextPremiumAmount;
  nextPremium;
  device;
  cookieStorage;
  showHealthBenefit = true;
  showIncreasingBenefit = true;
  showIncomeBenefit = true;
  
  HospitalCalculated:"";
  IncreasingCalculated:""; 
  IncomeBenefitCalculated:"";
  
  public subject = new Subject<any>();
  public flagData = new Subject<any>();
  public userInputSubject = new Subject<any>();
  invokeEvent: Subject<any> = new Subject();
  
  // @ViewChild('inclusiveOfTaxes') inclusiveOfTaxes: PopupComponent;
  constructor(public WindowRef: WindowRefService, public blowfishEncrypt: BlowfishEncryptionService, public cookieService: CookieService) {
     this.WindowRef.nativeWindow.dataLayer = [];

    this.flag = {
      editComponent: {},
      Inputfields: {
        validMobNo: false,
        validName: false,
        validEmail: false,
        validPincode: false,
        Male: false,
        Female: false,
        gender: true,
        TobaccoYes: false,
        TobaccoNo: false,
        tobacco: true,
        dateofbirth: false,
        spouseTobaccoYes: false,
        spouseTobaccoNo: false,
        spousedob: false,
        husbandWife: false,
        editAddSpouse: false,
        existingCustomeradded: false,
        TermsAndCond: "",
        showBreakUpFrequency: "",
        spouseTobaccoErrorMsg: "",
        loader: false,
        callRequest: false,
        traiFlag: true,
        LeadFlag: false,
        Select_Plan_Flag: "No",
        QuoteFlag: "No",
        ProceedAppFlag: "No",
        ComboFlag: "No",
        QuoteProceedFlag: "No",
        isDevisetypeMobile: false
      },
      checkValidate: {
        spousedob: false,
        spouseTobacco: false,
        pincodeDetail: false,
        emailDetail: false,
        nameDetail: false,
        mobileDetail: false,
        existingMobile: false,
        existingEmail: false,
      },
      showHide: {
        partnerCoverRemoveAdd: false,
        benefitAdd: true,
        benefitRemove: false,
        existingCustomeradded: false,
        showBreakUp: false,
        ExistingCus: false,
		// groupEmployee: true,
        // groupEmployeeDialog: false,
        notExistingCus: true,
        removeCancerCover: true,
        removeHeartCover: true,
        hideRemoveCancerOption: true,
        hideRemoveHeartOption: true,
        HospitalCalculated: false,
        IncreasingCalculated: false, 
        IncomeBenefitCalculated: false
      },
      InfoSubmitFlag: true
    };

    this.flagData.next(this.flag);

    this.HeartCancer_InputArr = {
      LA1_DOB: "01/01/1990",
      LA2_DOB: "01/01/1985",
      LA1_Gender: "Male",
      LA2_Gender: "Female",
      LA1_HeartSumAssured: "1000000",
      LA2_HeartSumAssured: "1000000",
      LA1_CancerSumAssured: "1000000",
      LA2_CancerSumAssured: "1000000",
      PolicyTerm: "20",
      Frequency: "Monthly",
      PPT: "Regular Pay",
      CoverageOption: "CancerAndHeart",
      SalesChannel: "Online",
      Staff: "No",
      LoyaltyBenefit: "No",
      FamilyBenefit: "No",
      HospitalBenefit: "No",
      IncreasingCoverBenefit: "No",
      IncomeBenefit: "No",
      LA1_Tobacco: "No",
      LA2_Tobacco: "No",
      UID: "480"
    };

    this.subject.next(this.HeartCancer_InputArr);

    this.dateMask = [
      /[0-9]/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      /\d/,
      /\d/
    ];
    this.userInput = {
      gender: "",
      tobacco: "",
      dob: "",
      spousedob: "",
      name: "",
      mobile: "",
      email: "",
      annualPackage: "Please select your income",
      pincode: "",
      age: "",
      getMale: "",
      getFemale: "",
      Partnerage: "",
      premiumYears: "",
      Frequency: "",
      hospitalbenefit: "",
      increasingbenefit: "",
      incomebenefit: "",
      LA1_heartCover: "",
      LA2_heartCover: "",
      LA1_cancerCover: "",
      LA2_cancerCover: "",
      storedMale: "",
      storedFemale: "",
      stringTotalCover: "H" + this.HeartCancer_InputArr.LA1_HeartSumAssured + " " + "C" + this.HeartCancer_InputArr.LA1_CancerSumAssured,
      stringPrimaryCover: "H" + this.HeartCancer_InputArr.LA1_HeartSumAssured + " " + "C" + this.HeartCancer_InputArr.LA1_CancerSumAssured,
      stringSpouseCover: "H" + this.HeartCancer_InputArr.LA2_HeartSumAssured + " " + "C" + this.HeartCancer_InputArr.LA2_CancerSumAssured
    };



    this.userInputSubject.next(this.userInput);

    this.constants = {
      ZERO_Cover: 0,
      MIN_HeartCover: 200000,
      MAX_HeartCover: 2500000,
      MIN_CancerCover: 200000,
      MAX_CancerCover: 5000000
    };

    this.Boundary_Conditions = {
      HSA_Max_LA1: "",
    };

    // this.dataLayerArray = {
    //   MonthlyPremium: "",
    //   YearlyPremium: "",
    //   nypremium: ""
    // };

	if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) ||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))
    {this.flag['Inputfields'].isDevisetypeMobile = true;}
    else
    {this.flag['Inputfields'].isDevisetypeMobile = false;}
    this.device = this.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop"  
  }

  updateUserInputSubject(key, value) {
    this.userInput[key] = value;
    this.userInputSubject.next(this.userInput);
  }

  updateFlagData(key, value) {
    this.flag[key] = value;
    this.flagData.next(this.flag);
  }

  updateHeartCancerInputArrValue(key, value) {
    this.HeartCancer_InputArr[key] = value;
    this.subject.next(this.HeartCancer_InputArr);
  }

  Fieldvalidation(fieldName, fieldValue) {
    if ("mobile-number" === fieldName || fieldName === "mobileNo") {
      let letterRegx = /^[6789]\d{9}$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validMobNo"] = false;
        return "Please Enter a Valid mobile number";
      } else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validMobNo"] = true;
        this.userInput['mobile'] = fieldValue;
        return "";
      }
    } else if ("first-name" == fieldName) {
      // let letterRegx = /^[a-zA-Z ]{2,20}$/;
      let letterRegx = /^(?=.{2,20}$)(([a-zA-Z ])\2?(?!\2))+$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validName"] = false;
        return "Please Enter your name";
      } else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validName"] = true;
        return "";
      }
    } else if ("email" == fieldName) {
      let letterRegx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validEmail"] = false;
        return "Please Enter your Email";
      } else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validEmail"] = true;
        return "";
      }
    } else if ("pincode" == fieldName) {
      let letterRegx = /^[1-9][0-9]{5}$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validPincode"] = false;
        return "Enter 6 digit pin code";
      } else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validPincode"] = true;
        return "";
      }
    } else if ("dobDate" == fieldName) {
      let exp = /(\d{2})\/(\d{2})\/(\d{4})/;
      if (exp.test(fieldValue)) {
        if (this.getAge(fieldValue) >= 18 && this.getAge(fieldValue) <= 65) {
          this.DOB = fieldValue;
          this.age = this.getAge(fieldValue);
          this.setPolicyTermDropdownprimary = this.setPolicyTermDropdown(
            this.age
          );
          this.userInput["age"] = this.getAge(fieldValue);
          this.HeartCancer_InputArr["LA1_DOB"] = fieldValue;
          this.subject.next(this.HeartCancer_InputArr);
          this.flag["Inputfields"]["dateofbirth"] = true;
          // this.calculatePremium();
          this.createHncCookie();
          return "";
        } else {
          this.flag["Inputfields"]["dateofbirth"] = false;

          return "Please enter the age between 18 to 65";
        }
      } else {

      }
    } else if ("SpousedobDate" == fieldName) {
      let exp = /(\d{2})\/(\d{2})\/(\d{4})/;
      if (exp.test(fieldValue)) {
        if (this.getAge(fieldValue) >= 18 && this.getAge(fieldValue) <= 65) {
          this.Partnerage = this.getAge(fieldValue);
          this.userInput["Partnerage"] = this.getAge(fieldValue);
          this.HeartCancer_InputArr["LA2_DOB"] = fieldValue;
          this.subject.next(this.HeartCancer_InputArr);
          this.flag["Inputfields"]["spousedob"] = true;
            this.flag["showHide"]["partnerCoverRemoveAdd"] = true;
            this.flag["showHide"]["showBreakUp"] = true;

            if (this.flag["Inputfields"]["editAddSpouse"] == true) {
              this.flag["husbandWife"] = true;
              this.flag["Male"] = false;
              this.flag["Female"] = false;
            }
          this.createHncCookie();
          return "";
        } else {
          this.flag["Inputfields"]["spousedob"] = false;
            this.flag["showHide"]["partnerCoverRemoveAdd"] = false;
            this.flag["showHide"]["showBreakUp"] = false;
            if (this.flag["Inputfields"]["editAddSpouse"] == true) {
              this.flag["husbandWife"] = false;
              this.flag["Male"] = true;
              this.flag["Female"] = true;
            }
          return "Please enter the age between 18 to 65";
        }
      }
        this.flag["Inputfields"]["spousedob"] = false;
        this.flag["showHide"]["partnerCoverRemoveAdd"] = false;
        this.flag["showHide"]["showBreakUp"] = false;
    }
  }
  getAge(dob) {
    let today = new Date();
    let birthDate = new Date(
      dob.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3")
    );
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  onChangeRadio(evt, fieldID) {
    this.flag["showHide"]["partnerCoverRemoveAdd"] = false;
    this.updateFlagData("husbandWife", false);
    this.updateFlagData("showHide", this.flag["showHide"]);
    let fieldValue = evt.target.value;
    if ("gender" == fieldID) {
      if ("Male" == fieldValue) {
        this.userInput["gender"] = fieldValue;
        this.HeartCancer_InputArr["LA1_Gender"] = fieldValue;
        this.subject.next(this.HeartCancer_InputArr);

        this.userInput["storedMale"] = true;
        this.userInput["storedFemale"] = false;
        this.updateFlagData("Male", true);
        this.updateFlagData("Female", false);
      } else {
        this.userInput["gender"] = fieldValue;

        this.HeartCancer_InputArr["LA1_Gender"] = fieldValue;
        this.userInput["storedFemale"] = true;
        this.userInput["storedMale"] = false;

        this.updateFlagData("Male", false);
        this.updateFlagData("Female", true);
      }
    }
    if ("tobacco" == fieldID) {
      if ("Yes" == fieldValue) {
        this.userInput["tobacco"] = fieldValue;
        this.HeartCancer_InputArr["LA1_Tobacco"] = fieldValue;

        this.updateFlagData("TobaccoYes", true);
        this.updateFlagData("TobaccoNo", false);
      } else {
        this.userInput["tobacco"] = fieldValue;
        this.HeartCancer_InputArr["LA1_Tobacco"] = fieldValue;
        this.subject.next(this.HeartCancer_InputArr);

        this.updateFlagData("TobaccoYes", false);
        this.updateFlagData("TobaccoNo", true);
      }
    }
    if ("spouseTobacco" == fieldID) {
      if ("Yes" == fieldValue) {
        this.userInput["tobacco"] = fieldValue;
        this.HeartCancer_InputArr["LA2_Tobacco"] = fieldValue;
        this.subject.next(this.HeartCancer_InputArr);

        this.updateFlagData("spouseTobaccoYes", true);
        this.updateFlagData("spouseTobaccoNo", false);
        this.flag['Inputfields']['spouseTobaccoErrorMsg'] = "";
      } else {
        this.HeartCancer_InputArr["LA2_Tobacco"] = fieldValue;
        this.subject.next(this.HeartCancer_InputArr);
        this.flag["spouseTobaccoYes"] = false;
        this.flag["spouseTobaccoNo"] = true;
        this.flag['Inputfields']['spouseTobaccoErrorMsg'] = "";
      }
      this.flag['checkValidate']['spouseTobacco'] = true;
        this.flag["showHide"]["partnerCoverRemoveAdd"] = true;
        this.flag["showHide"]["showBreakUp"] = true;

        this.updateFlagData("showHide", this.flag["showHide"]);
        if (this.flag["Inputfields"]["editAddSpouse"] == true) {
          this.updateFlagData("husbandWife", true);
        }
      }
    this.createHncCookie();
  }

  addCommas(nStr) {
    if (nStr != undefined) {
      nStr = nStr.toString().replace(/,/g, "");
      nStr = nStr.replace(/[^0-9 ]/g, "");
      let parts = nStr.toString().split(".");
      let prt1 = parts[0].slice(0, -1);
      let prt2 = parts[0].slice(-1);
      parts[0] = prt1.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + prt2;
      nStr = parts[0];

      return nStr;
    }
  }

  removeCommas(numWidCommas) {
    var numWidoutCommas = numWidCommas.replace(/[,]/g, '');
    return numWidoutCommas;
  }

  valueRoundup(numVal) {
    let tempObj = {}
    tempObj['actVal'] = numVal;
    let sadefault = numVal;
    let sa = "" + numVal + ""; //sa to string
    if (sa.length == 6) {
      let safront = sa.substring(0, 1);
      let samiddle = sa.substring(1, 3);

      if (parseInt(safront) == 1) {
        if (samiddle == "00")
          tempObj['text'] = safront;
        else
          tempObj['text'] = safront + "." + samiddle;
        tempObj['prefix'] = 'lakhs';
      } else {
        if (samiddle == "00")
          tempObj['text'] = safront;
        else
          tempObj['text'] = safront + "." + samiddle;
        tempObj['prefix'] = 'lakhs';
      }
    } else if (sa.length > 6) {
      sa = "" + sa + "";
      sa = sa.substring(2, sa.length);
      let sum = sadefault;
      sum = "" + sum + "";
      sum = sum.substring(0, 2);
      let slice = "" + sadefault + "";
      slice = slice.substring(2, 4);
      if (sa.length == 5) {
        //return sum + "." + slice + " lakhs";
        if (slice == "00")
          tempObj['text'] = sum;
        else
          tempObj['text'] = sum + "." + slice;
        // tempObj['text'] = sum + "." + slice;
        tempObj['prefix'] = 'lakhs';
      } else if (sa.length == 6) {
        let newsa = sadefault;
        newsa = "" + newsa + "";
        newsa = newsa.substring(0, 1);
        let newslice = sadefault;
        newslice = "" + newslice + "";
        newslice = newslice.substring(1, 3);

        if (parseInt(newsa) == 1) {
          //return newsa + "." + newslice + " crore";
          if (newslice == '00')
            tempObj['text'] = newsa;
          else
            tempObj['text'] = newsa + "." + newslice;

          tempObj['prefix'] = 'cr';
        } else {
          //return newsa + "." + newslice + " crores";
          if (newslice == '00')
            tempObj['text'] = newsa;
          else
            tempObj['text'] = newsa + "." + newslice;

          tempObj['prefix'] = 'cr';
        }
      } else if (sa.length == 7) {
        let newsa = sadefault;
        newsa = "" + newsa + "";
        newsa = newsa.substring(0, 2);
        let newslice = sadefault;
        newslice = "" + newslice + "";
        newslice = newslice.substring(2, 4);
        
        if (newslice == '00')
          tempObj['text'] = newsa;
        else
          tempObj['text'] = newsa + "." + newslice;
        tempObj['prefix'] = 'cr';
      }
    } else {
      tempObj['text'] = numVal;
      tempObj['prefix'] = '';
    }
    tempObj['text'] = tempObj['text'].trim()

    return tempObj["text"] + " " + tempObj["prefix"];
  }
  getValueFromStr(val) {
    let returnvalue = "";
    let slice1 = "";
    if (val.indexOf("crores") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "0000000";
    } else if (val.indexOf("lakhs") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "00000";
    } else if (val.indexOf("crore") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "000000";
    } else if (val.indexOf("lakh") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "0000";
    } else {
      returnvalue = val.replace(/\,/g, "");
    }
    return returnvalue;
  }

  setPolicyTermDropdown(age) {
    let policyterm = [];
    if (75 - age >= 40) {
      for (let i = 5; i <= 40; i++) {
        policyterm.push(i);
      }
    } else {
      for (let i = 5; i <= 75 - age; i++) {
        policyterm.push(i);
      }
      if (75 - age <= 20) {
        this.HeartCancer_InputArr["PolicyTerm"] = (75 - age).toString();
        this.subject.next(this.HeartCancer_InputArr);
      }
    }
    return policyterm;
  }

  // OnchangeDefaultParam(ChangedParam) {
    // var splitdob = this.userInput["dob"].split("/");
    // var newdate = splitdob[1] + "/" + splitdob[0];
    // try {
    //   this.WindowRef.nativeWindow.dataLayer.push({
    //     'event': "Desktop_HnCCalculator_DefaultValueChanged",
    //     'Data_Prefilled': "No",
    //     'ProductCode': "T48",
    //     'ProductName': "HeartCancer",
    //     'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
    //     'pagetype': "top-health : heart-cancer-protect-calculator",
    //     'subsection1': "health-insurance-plans",
    //     'subsection2': "heart-cancer-protect-calculator",
    //     'subsection3': "health-insurance-plans-desktop",
    //     'channel': "protection",
    //     'Subchannel': "health",
    //     'PageCategory': "Calculator",
    //     'PageSubCategory': "Calculator-Quote",
    //     'SectionName': "Quote - Existing Customer",
    //     'UserType': "Guest",
    //     'UserRole': "BOL",
    //     'UserVisit': "New",
    //     'Age': this.userInput["age"],
    //     'DOB': newdate,
    //     'Gender': this.userInput["gender"],
    //     'Tobacco': this.userInput["tobacco"],
    //     'CancerCover': parseInt(this.HeartCancer_InputArr["LA1_CancerSumAssured"]),
    //     'HeartCover': parseInt(this.HeartCancer_InputArr["LA1_HeartSumAssured"]),
    //     'totalCover': parseInt(this.getValueFromStr(this.sumAssured)),
    //     'MonthlyPremium': this.dataLayerArray.MonthlyPremium,
    //     'PaymentFrequency': this.HeartCancer_InputArr["Frequency"],
    //     'PolicyTerm': parseInt(this.HeartCancer_InputArr["PolicyTerm"]),
    //     'Premium': parseInt(this.removeCommas(this.premiumAmount)),
    //     'YearlyPremium': this.dataLayerArray.YearlyPremium,
    //     'nypremium': this.dataLayerArray.nypremium,
    //     'PolicyType': this.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
    //     'PaymentTerm': parseInt(this.HeartCancer_InputArr["PolicyTerm"]),
    //     'Device': this.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
    //     'ValueChanged': ChangedParam,
    //     'SpouseAdded': this.HeartCancer_InputArr["FamilyBenefit"],
    //     'UID': this.HeartCancer_InputArr.UID
    //   });
    // } catch (e) { }
  // }

  // boundary Conditon Function for Heart
  ValidateHSA_LA1() {
    var age = this.age;
    if (age >= 18 && age <= 50) {
      this.Boundary_Conditions['HSA_Max_LA1'] = 2500000;
    } else if (age >= 51 && age <= 55) {
      this.Boundary_Conditions['HSA_Max_LA1'] = 2000000;
    } else if (age >= 56 && age <= 65) {
      this.Boundary_Conditions['HSA_Max_LA1'] = 1000000;
    }
  }

  // adConversion Function
  fireConversion() {
    var gcc = new Image();
    var rand_time = (new Date()).getTime();

    gcc.src = window.location.protocol + '//www.googleadservices.com/pagead/conversion/850517359/?label=T_IjCPaByHMQ77rHlQM&amp;guid=ON&amp;script=0&rand_time=' + rand_time;
  }

  InclusiveOfTaxes() {
    this.invokeEvent.next('editTaxes');
  }
  // addIncomebenefits() {
  //   this.invokeAddBenefitEvent.next('IncomeBenefit');
  // }
  // addIncreasings() {
  //   this.invokeIncreasingEvent.next('IncreasingBenefit');
  // }
  // addHospital() {
  //   this.invokeHospitalEvent.next('Hospital');
  // }

  // Cookie Creation Function  
  createHncCookie() {
    // this.calculateNextPremium();
    let Enc_UserData = "HNC_Product_Name=HNC&HNC_Total_Cover="+this.sumAssuredPL+"&HNC_Heart_Cover=" + this.HeartCancer_InputArr['LA1_HeartSumAssured'] + "&HNC_Cancer_Cover=" + this.HeartCancer_InputArr['LA1_CancerSumAssured'] + "&HNC_Gender=" + this.HeartCancer_InputArr['LA1_Gender'] + "&LA1_Gender=" + this.HeartCancer_InputArr['LA1_Gender'] + "&LA2_Gender=" + this.HeartCancer_InputArr['LA2_Gender'] + "&HNC_Cust_FirstName=" + this.userInput['name'] + "&HNC_LeadFlag=" + this.flag['Inputfields']['LeadFlag'] + "&HNC_QuoteFlag=" + this.flag['Inputfields']['QuoteFlag'] + "&HNC_SelectPlanFlag=" + this.flag['Inputfields']['Select_Plan_Flag'] + "&HNC_ProceedAppFlag=" + this.flag['Inputfields']['ProceedAppFlag'] + "&HNC_ComboFlag=" + this.flag['Inputfields']['ComboFlag'] + "&HNC_QuoteProceedFlag=" + this.flag['Inputfields']['QuoteProceedFlag'] + "&HNC_Cust_DOB=" + this.HeartCancer_InputArr['LA1_DOB'] + "&HNC_RiskA=" + this.HeartCancer_InputArr['LA1_Tobacco'] + "&HNC_Premium=" + this.premiumAmount + "&HNC_Next_Dob_Premium=" + this.nextPremiumAmount + "&HNC_Policy_Term=" + this.HeartCancer_InputArr['PolicyTerm'] + "&HNC_Payment_Frequency=" + this.HeartCancer_InputArr['Frequency'] + "&HNC_Select_Your_Plan=" + this.HeartCancer_InputArr['CoverageOption'] + "&HNC_Spouse_Total_Cover=" +this.sumAssuredJL+ "&HNC_Spouse_Heart_Cover=" + this.HeartCancer_InputArr['LA2_HeartSumAssured'] + "&HNC_Spouse_Cancer_Cover=" + this.HeartCancer_InputArr['LA2_CancerSumAssured'] + "&HNC_Spouse_Gender=" + this.HeartCancer_InputArr['LA2_Gender'] + "&HNC_Cust_Spouse_DOB=" + this.HeartCancer_InputArr['LA2_DOB'] + "&HNC_Spouse_RiskA=" + this.HeartCancer_InputArr['LA2_Tobacco'] + "&HNC_Hospital_Cash_Benefit=" + this.HeartCancer_InputArr['HospitalBenefit'] + "&HNC_Increasing_Cover_Benefit=" + this.HeartCancer_InputArr['IncreasingCoverBenefit'] + "&HNC_Income_Replacement_Cover=" + this.HeartCancer_InputArr['IncomeBenefit'] + "&HNC_Existing_Customer=No&ends=";
    var UserDataJSON = this.blowfishEncrypt.encryptData(JSON.stringify(Enc_UserData), "!t@a6@($%(", { outputType: 0, cipherMode: 0 });
    // document.cookie = "encryptedCookie_HNC="+UserDataJSON+";"+this.expires+";path=/";
    // this.cookieService.set('encryptedCookie_HNC', UserDataJSON) + ";" + this.expires + ";path=/" + ";domain=iciciprulife.com";
    // this.cookieService.set('encryptedCookie_HNC', UserDataJSON, 90 , "path?: /" , "domain?:iciciprulife.com");
    this.cookieService.set('encryptedCookie_HNC', UserDataJSON, 90, "/", "iciciprulife.com");//cookieOpts ;
  }
}
