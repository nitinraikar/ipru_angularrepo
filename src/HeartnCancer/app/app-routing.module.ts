import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LandingPageComponent } from './landing-page/landing-page.component';
import {} from './shared/header/header.module'

const routes: Routes = [
  {
    path: 'protected',
    loadChildren: './front-page-modules/protected/protected.module#ProtectedModule',
    outlet: 'protected',
  },
  {
    path:'plan',
    loadChildren: './front-page-modules/plan/plan.module#PlanModule',
    outlet: 'plan',
  },
  {
    path:'video',
    loadChildren: './front-page-modules/video/video.module#VideoModule',
    outlet: 'video',
  },
  { 
    path: 'header',
    loadChildren: './shared/header/header.module#HeaderModule',
    outlet: 'header',
  },
  { 
    path: 'banner',
    loadChildren: './front-page-modules/banner/banner.module#BannerModule',
    outlet: 'banner',
  },
  { 
    path: 'benefits',
    loadChildren: './front-page-modules/benefits/benefits.module#BenefitsModule',
    outlet: 'benefits',
  },
  { 
    path: 'reason',
    loadChildren: './front-page-modules/reason/reason.module#ReasonModule',
    outlet: 'reason',
  },
  {
    path: 'f-question',
    loadChildren: './front-page-modules/question/fquestion.module#FquestionModule',
    outlet: 'fquestion',
  },
  {
    path: 'f-disclaimer',
    loadChildren: './front-page-modules/disclaimer/f-disclaimer.module#FDisclaimerModule',
    outlet: 'disclaimer',
  },
  
  { 
    path: '',
    redirectTo: 'landing-page', 
    pathMatch: 'full'
  },
  { 
    path: 'landing-page',
    component: LandingPageComponent,
	  loadChildren: './main-forms/basic-details/basic-details.module#BasicDetailsModule'
  },
  { 
    path: 'premium',
    loadChildren: './main-forms/Premium/premium/premium.module#PremiumModule'
  },
  { 
    path: 'personal',
    loadChildren: './main-forms/Personal/personal/personal.module#PersonalModule'
  },
  { 
    path: 'benefit',
    loadChildren: './main-forms/Benefit/benefit/benefit.module#BenefitModule'
  },
  { 
    path: 'final-loader',
    loadChildren:'./shared/final-loader/final-loader/final-loader.module#FinalLoaderModule'
  }
];

const config: ExtraOptions = {
  useHash: true
};

@NgModule({
  imports: [ RouterModule.forRoot(routes, config) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
