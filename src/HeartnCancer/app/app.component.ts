import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { TempServiceService } from './service/temp-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})

export class AppComponent {
  constructor(public tempservice: TempServiceService, private router: Router) { }

  ngOnInit() {
    if (this.ipru_getParameterByName("UID", "") != undefined && this.ipru_getParameterByName("UID", "") != "") {
      this.tempservice.HeartCancer_InputArr['UID'] = this.ipru_getParameterByName("UID", "");
    } else {
      this.tempservice.HeartCancer_InputArr['UID'] = "480";
    }
    
    // reDirect to firstPage when refreshed
    this.router.navigate(['']);

    var d = new Date();
    d.setTime(d.getTime() + (91 * 24 * 60 * 60 * 1000));
    this.tempservice.expires = "expires=" + d.toUTCString();
  }

  ipru_getParameterByName(name, query): any {
    if (!query)
      query = window.location.search;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(query);
    if (results == null)
      return "";
    else
      return decodeURIComponent(results[1].replace(/\+/g, " "));
  }
}
