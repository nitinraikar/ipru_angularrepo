import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { HttpClientModule } from '@angular/common/http';

// import { TextMaskModule } from 'angular2-text-mask';
// import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { CookieService } from 'ngx-cookie-service';
// import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';

import { AppRoutingModule } from './app-routing.module';
// import { PopupModule } from './shared/popup/popup/popup.module';
// import { StepsModule } from './shared/steps/steps/steps.module';
import { AppComponent } from './app.component';
// import { HeaderComponent } from './shared/header/header.component';
// import { ReasonComponent } from './front-page-modules/reason/reason.component';
// import { BasicDetailsCardComponent } from './shared/basic-details-card/basic-details-card.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
// import { BasicDetailsComponent } from './main-forms/basic-details/basic-details.component';
import { TempServiceService } from './service/temp-service.service';
import { GlobalServiceService } from './service/global-service.service';
import { BlowfishEncryptionService } from 'src/Common/services/blowfish-encryption.service';
// import { BlowfishEncryptionService } from './service/blowfish-encryption.service';
// import { ReasonComponent } from './front-page-modules/reason/reason.component';

const allDeclarations = [
  AppComponent,
  // HeaderComponent,
  // ReasonComponent,
  // ReasonComponent,
  // BasicDetailsCardComponent,
  LandingPageComponent,
  // BasicDetailsComponent,
];

const importsModule = [
  BrowserModule,
  BrowserAnimationsModule,
  RouterModule,
  // TextMaskModule,
  // FormsModule,
  // HttpClientModule,
  // PopupModule,
  // StepsModule,
  // ScrollToModule.forRoot(),
  AppRoutingModule,
  // LazyLoadImageModule.forRoot({
  //   preset: intersectionObserverPreset
  // }),
];

const allServices = [
  TempServiceService,
  GlobalServiceService,
  BlowfishEncryptionService,
  CookieService
];

@NgModule({
  declarations: [ ...allDeclarations ],
  imports: [ ...importsModule ],
  providers: [ ...allServices ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
