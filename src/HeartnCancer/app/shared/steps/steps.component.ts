import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.component.html',
  styleUrls: ['./steps.component.css']
})
export class StepsComponent implements OnInit {
  showBulletsPage1 = false;
  showBulletsPage2 = false;
  showBulletsPage3 = false;
  showBulletsPage4 = false;

  ngOnInit(): void {}
  showBullet2(): void {
    this.showBulletsPage2 = true;
  }

  showBullets(compt): void {
    switch (compt) {
      case 'BD':
        this.showBulletsPage1 = true;
        this.showBulletsPage2 = false;
        this.showBulletsPage3 = false;
        this.showBulletsPage4 = false;
        break;
      case 'PD':
        this.showBulletsPage1 = true;
        this.showBulletsPage2 = true;
        this.showBulletsPage3 = false;
        this.showBulletsPage4 = false;
        break;
      case 'PRD':
        this.showBulletsPage1 = true;
        this.showBulletsPage2 = true;
        this.showBulletsPage3 = true;
        this.showBulletsPage4 = false;
        break;
      case 'BND':
        this.showBulletsPage1 = true;
        this.showBulletsPage2 = true;
        this.showBulletsPage3 = true;
        this.showBulletsPage4 = true;
        break;
      default:
        this.showBulletsPage1 = true;
        this.showBulletsPage2 = true;
        this.showBulletsPage3 = true;
        this.showBulletsPage4 = true;
    }
  }
}
