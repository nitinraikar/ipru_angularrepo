import { FinalLoaderModule } from './final-loader.module';

describe('FinalLoaderModule', () => {
  let finalLoaderModule: FinalLoaderModule;

  beforeEach(() => {
    finalLoaderModule = new FinalLoaderModule();
  });

  it('should create an instance', () => {
    expect(finalLoaderModule).toBeTruthy();
  });
});
