import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinalLoaderComponent } from './final-loader.component';
import { LoaderComponent } from '../loader/loader.component';

const routes: Routes = [
  {
    path: '',
    component: FinalLoaderComponent,
    children: [
      { path: '', redirectTo: 'loader', pathMatch: 'full' },
      { path: 'loader', component: LoaderComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinalLoaderRoutingModule { }
