import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalLoaderComponent } from './final-loader.component';

describe('FinalLoaderComponent', () => {
  let component: FinalLoaderComponent;
  let fixture: ComponentFixture<FinalLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
