import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinalLoaderRoutingModule } from './final-loader-routing.module';
import { LoaderComponent } from '../loader/loader.component';
import { FinalLoaderComponent } from './final-loader.component';

@NgModule({
  imports: [
    CommonModule,
    FinalLoaderRoutingModule
  ],
  declarations: [
    LoaderComponent,
    FinalLoaderComponent
  ]
})
export class FinalLoaderModule { }
