import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { animate, style, transition, trigger } from '@angular/animations';
import { TempServiceService } from 'src/HeartnCancer/app/service/temp-service.service';
import { BlowfishEncryptionService } from 'src/Common/services/blowfish-encryption.service';
// import { BlowfishEncryptionService } from 'src/HeartnCancer/app/service/blowfish-encryption.service';
// import { BlowfishEncryptionService } from 'HeartnCancer/src/app/service/blowfish-encryption.service';
// import { TempServiceService } from 'HeartnCancer/src/app/service/temp-service.service';

// import { TempServiceService } from 'src/app/service/temp-service.service';
// import { BlowfishEncryptionService } from 'src/app/service/blowfish-encryption.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css'],
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translate3d(100%, 0, 0)' }),
        animate(400)
      ])
    ])
  ]
})
export class LoaderComponent implements OnInit {
  public done1: boolean = false;
  public done2: boolean = false;
  public done3: boolean = false;
  public done4: boolean = false;
  public showtext: string = 'doing the math';
  public ProceedAppFlag: String = 'No';
  public Glb_Staff: String = 'No';
  public Glb_UID: String = '480';
  
  @ViewChild('heartCancerJsonForm') heartCancerJsonForm: ElementRef;
  @ViewChild('bolData') bolData: ElementRef;


  constructor(public tempservice: TempServiceService, private router: Router,
    private blowfishEncrypt: BlowfishEncryptionService, ) {
    this.router.events.subscribe(() => {
      document.body.className = "bodySticky";
      document.getElementById("loader").scrollIntoView(true);
    });
  }

  ngOnInit() {
    setTimeout(() => {
      this.done1 = true;
    }, 2000);
    setTimeout(() => {
      this.done2 = true;
      this.showtext = 'Updating quoute';
    }, 4000);
    setTimeout(() => {
      this.done3 = true;
      this.showtext = 'Adding benefits';
    }, 6000);
    setTimeout(() => {
      this.done4 = true;
      this.showtext = 'Updating personal details';
    }, 8000);
    setTimeout(() => {
      this.showtext = 'Proceeding to application form';
    }, 9000);
    this.tempservice.flag['Inputfields']['loader'] = true
    setTimeout(() => {  
    this.callMethod();
    },10000);
    sessionStorage.removeItem('Glb_UID');
  }


  callMethod() { 
  if(this.tempservice.flag['Inputfields']['loader'] == true) {
    this.ProceedAppFlag = 'Yes';

    let splitDate = this.tempservice.HeartCancer_InputArr['LA1_DOB'].split('/');
    let temp_Current_dob = `${splitDate[0]}${'-'}`;

    if (splitDate[1] == '01') temp_Current_dob += 'JAN-';
    else if (splitDate[1] == '02') temp_Current_dob += 'FEB-';
    else if (splitDate[1] == '03') temp_Current_dob += 'MAR-';
    else if (splitDate[1] == '04') temp_Current_dob += 'APR-';
    else if (splitDate[1] == '05') temp_Current_dob += 'MAY-';
    else if (splitDate[1] == '06') temp_Current_dob += 'JUN-';
    else if (splitDate[1] == '07') temp_Current_dob += 'JUL-';
    else if (splitDate[1] == '08') temp_Current_dob += 'AUG-';
    else if (splitDate[1] == '09') temp_Current_dob += 'SEP-';
    else if (splitDate[1] == '10') temp_Current_dob += 'OCT-';
    else if (splitDate[1] == '11') temp_Current_dob += 'NOV-';
    else if (splitDate[1] == '12') temp_Current_dob += 'DEC-';
    temp_Current_dob += splitDate[2];
    const PropInfDob = temp_Current_dob;

    splitDate = this.tempservice.HeartCancer_InputArr['LA2_DOB'].split('/');
    temp_Current_dob = `${splitDate[0]}${'-'}`;
    if (splitDate[1] == '01') temp_Current_dob += 'JAN-';
    else if (splitDate[1] == '02') temp_Current_dob += 'FEB-';
    else if (splitDate[1] == '03') temp_Current_dob += 'MAR-';
    else if (splitDate[1] == '04') temp_Current_dob += 'APR-';
    else if (splitDate[1] == '05') temp_Current_dob += 'MAY-';
    else if (splitDate[1] == '06') temp_Current_dob += 'JUN-';
    else if (splitDate[1] == '07') temp_Current_dob += 'JUL-';
    else if (splitDate[1] == '08') temp_Current_dob += 'AUG-';
    else if (splitDate[1] == '09') temp_Current_dob += 'SEP-';
    else if (splitDate[1] == '10') temp_Current_dob += 'OCT-';
    else if (splitDate[1] == '11') temp_Current_dob += 'NOV-';
    else if (splitDate[1] == '12') temp_Current_dob += 'DEC-';
    temp_Current_dob = `${temp_Current_dob}${splitDate[2]}`;
    let SpouseDob = temp_Current_dob;

    let JointLifePolicyFor;
    let jointLifeFlag;
    //let Coverage_Option;
    let LoyaltyBenefit;
    let FamilyBenefit;
    //let premiumterm;
    //let ExistingCustomerToPass;
    let spouseOpted;
    let spouseGender = this.tempservice.HeartCancer_InputArr['LA2_Gender'];
    let LA2_HeartSumAssured = this.tempservice.HeartCancer_InputArr[
      'LA2_HeartSumAssured'
    ];
    let LA2_CancerSumAssured = this.tempservice.HeartCancer_InputArr[
      'LA2_CancerSumAssured'
    ];

      if (this.tempservice.HeartCancer_InputArr['FamilyBenefit'] == 'Yes') {
      jointLifeFlag = 'Y';
      JointLifePolicyFor = 'self';
      spouseOpted = '1';
      FamilyBenefit = 'true';
    } else {
      jointLifeFlag = 'N';
      JointLifePolicyFor = 'false';
      spouseOpted = '0';
      FamilyBenefit = 'false';
      SpouseDob = '';
      spouseGender = '';
      LA2_HeartSumAssured = '';
      LA2_CancerSumAssured = '';
    }

    const premiumPayingFrequency = this.tempservice.HeartCancer_InputArr[
      'Frequency'
    ];

    const Primary_FullName = this.tempservice.userInput['name'];
    let Primary_FirstName = Primary_FullName.substr(
      0,
      Primary_FullName.indexOf(' ')
    );
    let Primary_LastName = Primary_FullName.substr(
      Primary_FullName.indexOf(' ') + 1
    );
    if (Primary_FirstName == '') {
      Primary_FirstName = Primary_LastName;
      Primary_LastName = '';
    }

    let premiumPayingTerm = this.tempservice.HeartCancer_InputArr['PolicyTerm'];
    let productId = 'T49';
    if (this.tempservice.HeartCancer_InputArr['PPT'] == 'Single Pay') {
      premiumPayingTerm = '1';
      productId = 'T48';
    }
    let isStaff;
    if (this.Glb_Staff == 'Yes') {
      isStaff = 1;
    } else {
      isStaff = 0;
    }

    let coverageOption;
    if (
      this.tempservice.HeartCancer_InputArr['LA1_HeartSumAssured'] != 0 &&
      this.tempservice.HeartCancer_InputArr['LA1_CancerSumAssured'] != 0
    ) {
      coverageOption = 'Heart and Cancer';
    } else if (
      this.tempservice.HeartCancer_InputArr['LA1_HeartSumAssured'] == 0 &&
      this.tempservice.HeartCancer_InputArr['LA1_CancerSumAssured'] != 0
    ) {
      coverageOption = 'Cancer';
    } else if (
      this.tempservice.HeartCancer_InputArr['LA1_HeartSumAssured'] != 0 &&
      this.tempservice.HeartCancer_InputArr['LA1_CancerSumAssured'] == 0
    ) {
      coverageOption = 'Heart';
    }
    if (this.tempservice.HeartCancer_InputArr['LoyaltyBenefit'] == 'Yes')
      LoyaltyBenefit = 'true';
    else LoyaltyBenefit = 'false';

    let TRAI_FLAG;
    if (this.tempservice.flag['InfoSubmitFlag']) TRAI_FLAG = 'YES';
    else TRAI_FLAG = 'NO';

    const tobacco =
      this.tempservice.HeartCancer_InputArr['LA1_Tobacco'] == 'Yes' ? 1 : 0;
    const tobaccoJL =
      this.tempservice.HeartCancer_InputArr['LA2_Tobacco'] == 'Yes' ? 1 : 0;
    let IsCalculateXRT = 'false';
    if (tobacco) IsCalculateXRT = 'true';
    else if (tobaccoJL) IsCalculateXRT = 'true';

    let UserDataJSON = {
      source: 'BOLPARTNER',
      sourceKey: 'BOLPARTNERKEY',
      advisorCode: '',
      dependentFlag: 'N',
      jointLifeFlag: jointLifeFlag,
      uidId: this.tempservice.HeartCancer_InputArr.UID,
      proposerInfos: {
        frstNm: Primary_FirstName,
        lstNm: Primary_LastName,
        mrtlSts: '696',
        dob: PropInfDob,
        gender: this.tempservice.HeartCancer_InputArr['LA1_Gender'],
        email: this.tempservice.userInput['email'],
        isStaff: isStaff,
        mobNo: this.tempservice.userInput['Mobile'],
        "comunctnAddress": {
          "pincode": this.tempservice.userInput['pincode'],
        },
        kycDoc: {
          idPrf: 'PAN',
          addPrf: 'Bank Letter',
          agePrf: 'PAN',
          itPrf: 'pancard',
          incomePrf: 'ITRETN',
          lddIdOthrDesc: '',
          lddIdNumber: '',
          lddIdExpiryDate: ''
        }
      },
      lifeAssrdInfos: {
        frstNm: '',
        lstNm: '',
        mrtlSts: '696',
        dob: SpouseDob,
        gender: this.tempservice.HeartCancer_InputArr['LA2_Gender'],
        isStaff: '0',
        mobNo: '',
        kycDoc: {
          idPrf: '',
          addPrf: '',
          agePrf: 'PAN',
          itPrf: ''
        }
      },
      productSelection: {
        premiumPayingFrequency: premiumPayingFrequency,
        policyTerm: this.tempservice.HeartCancer_InputArr['PolicyTerm'],
        premiumPayingTerm: premiumPayingTerm,
        salesChannel: '',
        productType: 'Health',
        productName: 'HeartCancer',
        productId: productId,
        premiumpaymentoption: this.tempservice.HeartCancer_InputArr['PPT'],
        storyCode: 'DN',
        tobacco: tobacco,
        tobaccoJL: tobaccoJL,
        DeathBenefitHeart: this.tempservice.HeartCancer_InputArr[
          'LA1_HeartSumAssured'
        ],
        DeathBenefitCancer: this.tempservice.HeartCancer_InputArr[
          'LA1_CancerSumAssured'
        ],
        DeathBenefitHeartJL: LA2_HeartSumAssured,
        DeathBenefitCancerJL: LA2_CancerSumAssured,
        IsCalculateXRT: IsCalculateXRT,
        Heart_XRT: String(tobacco),
        Cancer_XRT: String(tobacco),
        HeartJL_XRT: String(tobaccoJL),
        CancerJL_XRT: String(tobaccoJL),
        JointLifePolicyFor: JointLifePolicyFor,
        spouseOpted: spouseOpted,
        spouseDOB: SpouseDob,
        spouseGender: spouseGender,
        coverageOption: coverageOption,
        additionalBenefits: this.BenefitsString(),
        familyBenefit: FamilyBenefit,
        loyaltyBenefit: LoyaltyBenefit,
        traiFlag: TRAI_FLAG
      },
      "partnerRedirectUrl":window.location.href,
      "paymentData": {
        "pincode": this.tempservice.userInput['pincode'] != ("" || undefined) ? this.tempservice.userInput['pincode'] : "400097",
        "extraAtsInfo": {
          "fname": Primary_FirstName != "" ? Primary_FirstName : "abc",
          "lname": Primary_LastName != "" ? Primary_LastName : "xyz",
          "dob_str": PropInfDob,
          "gender": this.tempservice.HeartCancer_InputArr['LA1_Gender'],
          "sa": parseInt(this.tempservice.HeartCancer_InputArr['LA1_HeartSumAssured'])+parseInt(this.tempservice.HeartCancer_InputArr['LA1_CancerSumAssured'])
        }
      }
    };

    let EncryptedData = this.blowfishEncrypt.encryptData(JSON.stringify(UserDataJSON), '!t@a6@($%(', "encrypt");
    this.bolData.nativeElement.value = EncryptedData;
    let heartnCancerForm = this.heartCancerJsonForm.nativeElement;
      
      heartnCancerForm.action = "https://buy.iciciprulife.com/buy/PartnerIntegration.htm"; /*DigiDrive Prod*/
      
    heartnCancerForm.submit();
  }
}

  BenefitsString(): String {
    let str;

    if (
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] === 'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] ===
      'No' &&
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] === 'No'
    )
      str = 'Hospital Benefit';
    else if (
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] === 'No' &&
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] ===
      'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] === 'No'
    )
      str = 'Increasing Cover Benefit';
    else if (
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] === 'No' &&
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] ===
      'No' &&
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] === 'Yes'
    )
      str = 'Income Benefit';
    else if (
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] === 'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] ===
      'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] === 'No'
    )
      str = 'Hospital Benefit and Increasing Cover Benefit';
    else if (
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] === 'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] ===
      'No' &&
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] === 'Yes'
    )
      str = 'Hospital Benefit and Income Benefit';
    else if (
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] === 'No' &&
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] ===
      'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] === 'Yes'
    )
      str = 'Income Benefit and Increasing Cover Benefit';
    else if (
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] === 'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] ===
      'Yes' &&
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] === 'Yes'
    )
      str = 'Hospital Benefit and Income Benefit and Increasing Cover Benefit';
    else str = 'None';

    return str;
  }
}

