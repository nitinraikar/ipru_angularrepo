import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumAmountComponent } from './premium-amount.component';

describe('PremiumAmountComponent', () => {
  let component: PremiumAmountComponent;
  let fixture: ComponentFixture<PremiumAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PremiumAmountComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component)
    .toBeTruthy();
  });
});
