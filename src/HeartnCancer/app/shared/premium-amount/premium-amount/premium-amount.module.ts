import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PremiumAmountComponent } from '../premium-amount.component';
import { PopupModule } from 'src/Common/components/popup/popup/popup.module';
// import { PopupModule } from '../../popup/popup/popup.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PopupModule,
  ],
  declarations: [PremiumAmountComponent],
  exports: [PremiumAmountComponent]
})
export class PremiumAmountModule { }
