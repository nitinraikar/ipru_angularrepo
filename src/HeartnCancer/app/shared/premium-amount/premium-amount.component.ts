import { Component, OnInit, ViewChild } from '@angular/core';
import { PremiumCalculateService } from '../../main-forms/Premium/premium-calculate.service';
import { TempServiceService } from '../../service/temp-service.service';
import { GlobalServiceService } from '../../service/global-service.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';
import { HncEbiService } from 'src/Common/services/hnc-ebi.service';
import { WindowRefService } from 'src/Common/services/window-ref.service';

@Component({
  selector: 'app-premium-amount',
  templateUrl: './premium-amount.component.html',
  styleUrls: ['./premium-amount.component.css']
})
export class PremiumAmountComponent implements OnInit {
  age;
  policyterm;
  cancerWoTax;
  Cancertax;
  totolCancerValue;
  totalHeartValue;
  heartWoTax;
  heartTax;
  totalPremiumWithTax;
  totalTax;
  totalWoTax;
  showdropOption = false;
  firstPerson;
  secondPerson;
  totalCover;
  oldPremium;
  showBreakupFrequency;
  totalpremiumBreakup;
  discount;
  totalcoverBreakup;

  @ViewChild('inclusiveOfTaxes')
  inclusiveOfTaxes: PopupComponent;

  @ViewChild('totalbreakup') totalbreakup: PopupComponent;

  constructor(
    public tempservice: TempServiceService,
    private callEBI: HncEbiService,
    public globalService: GlobalServiceService,
    public WindowRef: WindowRefService,
    public premiumCalc: PremiumCalculateService
  ) {
    this.tempservice.invokeEvent.subscribe(value => {
      if(value === 'editTaxes'){
       this.showTaxes(); 
      }
    });
    this.age = 40;
  }

  ngOnInit(): void {
    this.premiumCalc.calculatePremium();
   }

  showdrop(): void {
    this.showdropOption = !this.showdropOption;
  }

  showTaxes(): void {
    this.inclusiveOfTaxes.showDialog();
    this.inclusiveOfTaxes.popuplarge();

    if (this.tempservice.flag['husbandWife']) {
      if (this.globalService.flag.cover.CancerCover === false) {
        this.cancerWoTax = 0;
        this.Cancertax = 0;
        this.totolCancerValue = this.cancerWoTax + this.Cancertax;
        this.heartWoTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Heart_1stPremium_WoTax'] +
          this.premiumCalc.premium['LA2_Heart_1stPremium_WoTax']);
        this.heartTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Heart_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Heart_1stPremium_WoTax'] +
          (this.premiumCalc.premium['LA2_Heart_1stPremium_WtTax'] -
            this.premiumCalc.premium['LA2_Heart_1stPremium_WoTax']));
        this.totalHeartValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.heartWoTax)) + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalWoTax = this.tempservice.addCommas(this.cancerWoTax + parseInt(this.tempservice.removeCommas(this.heartWoTax)));
        this.totalTax = this.tempservice.addCommas(this.Cancertax + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalPremiumWithTax = this.tempservice.addCommas(this.totolCancerValue + parseInt(this.tempservice.removeCommas(this.totalHeartValue)));
      } else if (this.globalService.flag.cover.HeartCover === false) {
        this.heartWoTax = 0;
        this.heartTax = 0;
        this.cancerWoTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WoTax'] +
          this.premiumCalc.premium['LA2_Cancer_1stPremium_WoTax']);
        this.Cancertax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WoTax'] +
          (this.premiumCalc.premium['LA2_Cancer_1stPremium_WtTax'] -
            this.premiumCalc.premium['LA2_Cancer_1stPremium_WoTax']));
        this.totolCancerValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + parseInt(this.tempservice.removeCommas(this.Cancertax)));
        this.totalHeartValue = this.heartWoTax + this.heartTax;
        this.totalWoTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + this.heartWoTax);
        this.totalTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.Cancertax)) + this.heartTax);
        this.totalPremiumWithTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.totolCancerValue)) + this.totalHeartValue);
      } else {
        this.cancerWoTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WoTax'] +
          this.premiumCalc.premium['LA2_Cancer_1stPremium_WoTax']);
        this.Cancertax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WoTax'] +
          (this.premiumCalc.premium['LA2_Cancer_1stPremium_WtTax'] -
            this.premiumCalc.premium['LA2_Cancer_1stPremium_WoTax']));
        this.totolCancerValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + parseInt(this.tempservice.removeCommas(this.Cancertax)));
        this.heartWoTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Heart_1stPremium_WoTax'] +
          this.premiumCalc.premium['LA2_Heart_1stPremium_WoTax']);
        this.heartTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Heart_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Heart_1stPremium_WoTax'] +
          (this.premiumCalc.premium['LA2_Heart_1stPremium_WtTax'] -
            this.premiumCalc.premium['LA2_Heart_1stPremium_WoTax']));
        this.totalHeartValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.heartWoTax)) + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalWoTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + parseInt(this.tempservice.removeCommas(this.heartWoTax)));
        this.totalTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.Cancertax)) + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalPremiumWithTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.totolCancerValue)) + parseInt(this.tempservice.removeCommas(this.totalHeartValue)));
      }
    } else {
      if (this.globalService.flag.cover.CancerCover === false) {
        this.cancerWoTax = 0;
        this.Cancertax = 0;
        this.totolCancerValue = this.cancerWoTax + this.Cancertax;
        this.heartWoTax = this.tempservice.addCommas(this.premiumCalc.premium[
          'LA1_Heart_1stPremium_WoTax'
        ]);
        this.heartTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Heart_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Heart_1stPremium_WoTax']);
        this.totalHeartValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.heartWoTax)) + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalWoTax = this.tempservice.addCommas(this.cancerWoTax + parseInt(this.tempservice.removeCommas(this.heartWoTax)));
        this.totalTax = this.tempservice.addCommas(this.Cancertax + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalPremiumWithTax = this.tempservice.addCommas((this.totolCancerValue) + parseInt(this.tempservice.removeCommas (this.totalHeartValue)));
      } else if (this.globalService.flag.cover.HeartCover === false) {
        this.heartWoTax = 0;
        this.heartTax = 0;
        this.cancerWoTax = this.tempservice.addCommas(this.premiumCalc.premium[
          'LA1_Cancer_1stPremium_WoTax'
        ]);
        this.Cancertax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WoTax']);
        this.totolCancerValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + parseInt(this.tempservice.removeCommas(this.Cancertax)));
        this.totalHeartValue = this.heartWoTax + this.heartTax;
        this.totalWoTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + this.heartWoTax);
        this.totalTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.Cancertax)) + this.heartTax);
        this.totalPremiumWithTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.totolCancerValue)) + this.totalHeartValue);
      } else {
        this.cancerWoTax = this.tempservice.addCommas(this.premiumCalc.premium[
          'LA1_Cancer_1stPremium_WoTax'
        ]);
        this.Cancertax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Cancer_1stPremium_WoTax']);
        this.totolCancerValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + parseInt(this.tempservice.removeCommas(this.Cancertax)));
        this.heartWoTax = this.tempservice.addCommas(this.premiumCalc.premium[
          'LA1_Heart_1stPremium_WoTax'
        ]);
        this.heartTax = this.tempservice.addCommas(
          this.premiumCalc.premium['LA1_Heart_1stPremium_WtTax'] -
          this.premiumCalc.premium['LA1_Heart_1stPremium_WoTax']);
        this.totalHeartValue = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.heartWoTax)) + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalWoTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.cancerWoTax)) + parseInt(this.tempservice.removeCommas(this.heartWoTax)));
        this.totalTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.Cancertax)) + parseInt(this.tempservice.removeCommas(this.heartTax)));
        this.totalPremiumWithTax = this.tempservice.addCommas(parseInt(this.tempservice.removeCommas(this.totolCancerValue)) + parseInt(this.tempservice.removeCommas(this.totalHeartValue)));
      }
    }
  }

  closeinclusiveOfTaxes(): void {
    this.inclusiveOfTaxes.closeDialog();
  }

  Change(evt, fldId): void {
    let value = evt.value.val;
    if (fldId === 'policyterm') {
      this.policyterm = value;
      this.tempservice.HeartCancer_InputArr['PolicyTerm'] = this.policyterm;
      this.tempservice.premium = this.callEBI.HeartCancerEBI(
        this.tempservice.HeartCancer_InputArr
      );
      this.tempservice.premiumAmount =
        Math.round(this.tempservice.premium.Base_FirstPremium) +
        Math.round(this.tempservice.premium.Base_SecondPremium);
    } else if (fldId === 'Frequency') {
      value = evt.value.name1;
      this.tempservice.HeartCancer_InputArr['Frequency'] = value;
      this.tempservice.premium = this.callEBI.HeartCancerEBI(
        this.tempservice.HeartCancer_InputArr
      );
      this.tempservice.premiumAmount =
        Math.round(this.tempservice.premium.Base_FirstPremium) +
        Math.round(this.tempservice.premium.Base_SecondPremium);
    }
  }
  openTotalBreakUp() {
    this.totalbreakup.showDialog();
    this.totalbreakup.changebreakup();
    this.firstPerson = this.tempservice.premium.LA1_first_premium;
    this.totalpremiumBreakup = 2 * this.tempservice.sumAssured;
    this.secondPerson = this.tempservice.premium.LA2_first_premium;
    this.totalCover = (this.tempservice.premium.LA1_first_premium) + (this.tempservice.premium.LA2_first_premium);
    this.oldPremium = this.tempservice.addCommas(Math.round(this.totalCover / 0.95));
    this.discount = this.tempservice.addCommas(Math.round(this.tempservice.removeCommas(this.oldPremium) - this.totalCover));


    this.totalcoverBreakup = this.tempservice.addCommas(this.tempservice.removeCommas(this.oldPremium) - this.tempservice.removeCommas(this.discount));
    if ((this.tempservice.userInput['Frequency']) == 'Yearly') {
      this.showBreakupFrequency = 'Yearly';
    } else {
      this.showBreakupFrequency = 'Monthly';
    }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();

     var splitdob = this.tempservice.userInput["dob"].split("/");
     var newdate = splitdob[1]+"/"+splitdob[0];
     try{
     this.tempservice.WindowRef.nativeWindow.dataLayer.push({
           'event' : this.tempservice.device+"_HnCCalculator_ViewTotalBreakup",
           'Data_Prefilled' : "No",
           'ProductCode' : "T48",
           'ProductName' : "HeartCancer",
           'pageName' : "protection : health-insurance-plans : heart-cancer-protect-calculator",
           'pagetype' : "top-health : heart-cancer-protect-calculator",
           'subsection1' : "health-insurance-plans",
           'subsection2' : "heart-cancer-protect-calculator",
           'subsection3' : "",
           'channel' : "protection",
           'Subchannel' : "health",
           'PageCategory' : "Calculator",
           'PageSubCategory' : "Calculator-Quote",
           'SectionName' : "Quote",
           'UserType': "Guest",
           'UserRole' : "BOL",
           'UserVisit' : "New",
           'Age' : this.tempservice.userInput["age"],
           'DOB' : newdate,
           'Gender' : this.tempservice.userInput["gender"],
           'Tobacco' : this.tempservice.userInput["tobacco"],
           'CancerCover' :  parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
           'HeartCover' : parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
           'totalCover' : parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
           'MonthlyPremium' :  this.tempservice.dataLayerArray.MonthlyPremium,
           'PaymentFrequency' : this.tempservice.HeartCancer_InputArr["Frequency"],
           'PolicyTerm' : parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Premium' : parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
           'YearlyPremium' : this.tempservice.dataLayerArray.YearlyPremium,
           'nypremium' : this.tempservice.dataLayerArray.nypremium,
           'PolicyType' : this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
           'PaymentTerm' :  parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Device' : this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
           'SpouseAdded' :  this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
           'UID' : parseInt(this.tempservice.HeartCancer_InputArr.UID)
     });
   }catch(e){}
   try{
     this.tempservice.datalayer['Behavior'] = "view Total Breakup";
     this.WindowRef.nativeWindow._satellite.track("Behavior");
   }catch(e){}
  }

  closeTotalBreakup() {
    this.totalbreakup.closeDialog();
  }
  totalGotIt() {
    this.totalbreakup.closeDialog();
  }
}
