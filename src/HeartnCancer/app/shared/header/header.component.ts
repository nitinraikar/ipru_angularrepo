import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { GlobalServiceService } from '../../service/global-service.service';
import { TempServiceService } from '../../service/temp-service.service';
// import { WindowRefService } from 'src/app/service/window-ref.service';
// import { PopupComponent } from '../popup/popup.component';
// import { WindowRefService } from '../../service/window-ref.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';
import { WindowRefService } from 'src/Common/services/window-ref.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('componentDiv')
  componentDiv: ElementRef;

 @ViewChild('callRequest') callRequest: PopupComponent;
  addCss = false;
  visibleSidebar2 = false;
  opened;
  opnscroll = false;
  showDownCalIcon = false;
  isSelected = false;
  showReturnHomeIcon = false;
  showQuoteScroll = true;
  leftDiv = true;
  showpanel;
  showmobileLogo = false;
  expand = false;

  defaultImage = 'HeartnCancer/assets/images/favicon.png';
  getCallImage = 'HeartnCancer/assets/images/get-call.png';
  offset = 100;

  constructor(
    private tempservice: TempServiceService,
    public globalService: GlobalServiceService,
    public WindowRef: WindowRefService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.tempservice.subject.subscribe(addCss => (this.addCss = addCss));
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    const element = window.pageYOffset;
    if (element > 500) {
      this.opnscroll = true;
      this.showDownCalIcon = true;
      this.isSelected = true;
      this.changeGetQuoteButtonColor(true);
      this.expand = true;
    } else if (element < 500) {
      this.opnscroll = false;
      this.showDownCalIcon = false;
      this.isSelected = false;
      this.expand = false;
    }
    
    if (element > 500) {
      this.showmobileLogo = true;
    } else {
      this.showmobileLogo = false;
    }
    
    if (element > 1500 && element < 2800) {
      this.showQuoteScroll = false;
      this.showDownCalIcon = false;
    } else {
      this.showQuoteScroll = true;
    }

    const bottomHeight = window.scrollY + window.innerHeight;
    if (bottomHeight > 5200) {
      this.showReturnHomeIcon = true;
    } else {
      this.showReturnHomeIcon = false;
    }

    if(bottomHeight > 4000){
      try{
         this.tempservice.datalayer['Scroll'] = '100';
        
         this.WindowRef.nativeWindow._satellite.track("Scroll");
      }catch(e){}
    }
  }

  showHeader(): void {
    this.visibleSidebar2 = true;
    this.leftDiv = true;
    this.showpanel = !this.showpanel;
  }

  toggle(): void {
    this.opened = true;
  }

  show(): void {
    this.globalService.flag['headerDisabled']['showPages'] = true;
    this.scrollFunction();
  }

  close(): void {
    this.showpanel = false;
    this.scrollFunction();
  }

	callRequestMethod(): void {
    this.showpanel = false;
    this.scrollFunction();
    this.callRequest.showDialog();
  }
  
  scrollFunction(): void {
    document.body.className = "";
  }

  changeGetQuoteButtonColor(add): void {
    this.addCss = add ? true : false;
  }

  Clk_Brochure() {
    window.open('https://www.iciciprulife.com/content/dam/icicipru/brochures/heart_and_cancer.pdf');
  }

	closecallRequest() {
    this.callRequest.closeDialog();
  }

  redirectHomePage() {
    window.location.href='https://www.iciciprulife.com';
  }

  checkPremiumIcon(){
     try{
       this.tempservice.datalayer['Behavior'] = "CHECK PREMIUM";
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }

  BenefitScroll() {
     try{
       this.tempservice.datalayer['Behavior'] = 'Benefits';
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }
  WorksScroll() {
     try{
       this.tempservice.datalayer['Behavior'] = 'How does the plan work?';
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }
  
  Faqs(){
     try{
       this.tempservice.datalayer['Behavior'] = 'Faqs';
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }

  Videos() {
     try{
       this.tempservice.datalayer['Behavior'] = 'Videos';
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }
  
  headerLinkLoad() {
    this.router.navigate([{ outlets: { protected: ['protected'],plan: ['plan'],video:['video'],fquestion:['f-question'],disclaimer:['f-disclaimer'] } }]); 
  }
}

