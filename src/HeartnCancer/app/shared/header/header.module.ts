import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderRoutingModule } from './header-routing.module';
import { HeaderComponent } from './header.component';
// import { PopupModule } from '../popup/popup/popup.module';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';
import { PopupModule } from 'src/Common/components/popup/popup/popup.module';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    HeaderRoutingModule,
    PopupModule,
    ScrollToModule.forRoot(),
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    }),
  ]
})
export class HeaderModule { }
