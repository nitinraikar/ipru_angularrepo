import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlusminusComponent } from '../plusminus.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [PlusminusComponent],
  exports:[PlusminusComponent]
})
export class PlusminusModule { }
