import { Component, Input, OnInit} from '@angular/core';
import { TempServiceService } from '../../service/temp-service.service';
import { GlobalServiceService } from '../../service/global-service.service';
import { PremiumCalculateService } from '../../main-forms/Premium/premium-calculate.service';

// import { TempServiceService } from 'src/app/service/temp-service.service';
// import { GlobalServiceService } from 'src/app/service/global-service.service';
// import { PremiumCalculateService } from 'src/app/main-forms/Premium/premium-calculate.service';

@Component({
  selector: 'app-plusminus',
  templateUrl: './plusminus.component.html',
  styleUrls: ['./plusminus.component.css']
})

export class PlusminusComponent implements OnInit {
  @Input()
  Type: string;
  
  @Input() sumAssured;
  counter = 0;

  constructor(
    public tempStorage: TempServiceService,
    public globalService: GlobalServiceService,
    public premiumCalc: PremiumCalculateService
  ) {
    this.tempStorage.subject.subscribe(() => {
      if (this.Type === 'heart')
        this.sumAssured = this.tempStorage.valueRoundup(
          this.tempStorage.HeartCancer_InputArr['LA1_HeartSumAssured']
        );
      else if (this.Type === 'cancer')
        this.sumAssured = this.tempStorage.valueRoundup(
          this.tempStorage.HeartCancer_InputArr['LA1_CancerSumAssured']
        );
    });
  }

  ngOnInit(): void { }

  incDec(objPrnt): void {
    let incDecVal = 500000;
    let ipVal;
    if (this.Type === 'heart') {
      ipVal = this.tempStorage.HeartCancer_InputArr['LA1_HeartSumAssured'];

      this.tempStorage.userInput['LA1_heartCover'] = this.sumAssured;
      this.tempStorage.userInput['LA2_heartCover'] = this.sumAssured;
    } else if (this.Type === 'cancer') {
      ipVal = this.tempStorage.HeartCancer_InputArr['LA1_CancerSumAssured'];
      this.tempStorage.userInput['LA1_cancerCover'] = this.sumAssured;
      this.tempStorage.userInput['LA2_cancerCover'] = this.sumAssured;
    }
    ipVal = parseInt(ipVal);
    if ('plus' === objPrnt) {
      
      if (ipVal < 100000) {
        incDecVal = 10000;
      } else if (ipVal < 1000000) {
        incDecVal = 100000;
      } else {
        incDecVal = 500000;
      }
      ipVal = parseInt(ipVal) + incDecVal;
    } else if (objPrnt === 'minus') {
      if (ipVal <= 100000) {
        incDecVal = 10000;
      } else if (ipVal <= 1000000) {
        incDecVal = 100000;
      } else {
        incDecVal = 500000;
      }
      ipVal = parseInt(ipVal) - incDecVal;
    }
    if (this.Type === 'heart') {
      this.minMaxValue(ipVal, 'heart');
    }
    if (this.Type === 'cancer') {
      this.minMaxValue(ipVal, 'cancer');
    }
    this.premiumCalc.calculatePremium();
    this.tempStorage.createHncCookie();
    if (this.Type === 'heart') {
      // this.tempStorage.OnchangeDefaultParam('heart cover')
    }
    if (this.Type === 'cancer') {
      // this.tempStorage.OnchangeDefaultParam('cancer cover')
    }
  }

  minMaxValue(ipVal, fieldID): void {
    if ('heart' === fieldID) {

      if (ipVal == '' || ipVal == undefined) {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_HeartSumAssured',
          this.tempStorage.HeartCancer_InputArr['LA1_HeartSumAssured']
        );
      }else if(ipVal > this.tempStorage.Boundary_Conditions['HSA_Max_LA1']) {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_HeartSumAssured',
          this.tempStorage.Boundary_Conditions['HSA_Max_LA1']
        );
      }
       else if (ipVal < this.tempStorage.constants['MIN_HeartCover']) {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_HeartSumAssured',
          this.tempStorage.constants['MIN_HeartCover']
        );
      } else if (ipVal > this.tempStorage.constants['MAX_HeartCover']) {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_HeartSumAssured',
          this.tempStorage.constants['MAX_HeartCover']
        );
      } else {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_HeartSumAssured',
          ipVal.toString()
        );
      }
      this.tempStorage.updateHeartCancerInputArrValue(
        'LA2_HeartSumAssured',
        this.tempStorage.HeartCancer_InputArr['LA1_HeartSumAssured']
      );
    } else if ('cancer' === fieldID) {
      if (ipVal == '' || ipVal == undefined) {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_CancerSumAssured',
          this.tempStorage.HeartCancer_InputArr['LA1_CancerSumAssured']
        );
      } else if (ipVal < this.tempStorage.constants['MIN_CancerCover']) {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_CancerSumAssured',
          this.tempStorage.constants['MIN_CancerCover']
        );
      } else if (ipVal > this.tempStorage.constants['MAX_CancerCover']) {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_CancerSumAssured',
          this.tempStorage.constants['MAX_CancerCover']
        );
      } else {
        this.tempStorage.updateHeartCancerInputArrValue(
          'LA1_CancerSumAssured',
          ipVal.toString()
        );
      }
      this.tempStorage.updateHeartCancerInputArrValue(
        'LA2_CancerSumAssured',
        this.tempStorage.HeartCancer_InputArr['LA1_CancerSumAssured']
      );
    }
    this.premiumCalc.calculatePremium();
    this.tempStorage.createHncCookie();
  }

  onFocusField(evt): void {
    const fieldValue = evt.target.value;
    if (this.Type === 'heart') {
      this.sumAssured = this.tempStorage.getValueFromStr(fieldValue);
    } else if (this.Type === 'cancer') {
      this.sumAssured = this.tempStorage.getValueFromStr(fieldValue);
    }
  }

  onBlurField(evt): void {
    if (this.Type === 'heart') {
      this.minMaxValue(evt.target.value, 'heart');
    } else if (this.Type === 'cancer') {
      this.minMaxValue(evt.target.value, 'cancer');
    }
    // this.tempStorage.OnchangeDefaultParam('heart cover')
  }

  onKeyUp(evt): void {
    let v = evt.target.value;
    if (this.Type === 'heart') {
      this.sumAssured = v.replace(/[^0-9]/g, '');
    } else if (this.Type === 'cancer') {
      this.sumAssured = v.replace(/[^0-9]/g, '');
    }
  }
}
