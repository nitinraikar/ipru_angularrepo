import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenefitscardComponent } from './benefitscard.component';

describe('BenefitscardComponent', () => {
  let component: BenefitscardComponent;
  let fixture: ComponentFixture<BenefitscardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BenefitscardComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenefitscardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component)
    .toBeTruthy();
  });
});
