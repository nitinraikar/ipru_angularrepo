import { Component, Input, OnInit } from '@angular/core';

import { TempServiceService } from '../../service/temp-service.service';
import { GlobalServiceService } from '../../service/global-service.service';
import { PremiumCalculateService } from '../../main-forms/Premium/premium-calculate.service';
// import { PremiumCalculateService } from 'src/app/main-forms/Premium/premium-calculate.service';

// declare const require: any;
// var sha1 = require('sha1');

@Component({
  selector: 'app-benefitscard',
  templateUrl: './benefitscard.component.html',
  styleUrls: ['./benefitscard.component.css']
})

export class BenefitscardComponent implements OnInit {
  
  @Input() Type: string;
  @Input() benefitAmount;
  @Input() showAdd;

  constructor(
    public tempservice: TempServiceService,
    public globalService: GlobalServiceService,
    public premiumCalc: PremiumCalculateService
  ) {
  }

  ngOnInit(): void { }

  changeAdd(): void {
    // var newBenefits = "";
    // var hospitalBenefitPremium = 0;
    // var increasingBenefitPremium = 0;
    // var incomeBenefitPremium = 0;
    if (this.Type === 'Hospital') {
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = 'Yes';
      this.tempservice.showHealthBenefit = false;     
    } else if (this.Type === 'IncreasingBenefit') {
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = 'Yes';
      this.tempservice.showIncreasingBenefit = false; 
    } else if (this.Type === 'IncomeBenefit') {
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = 'Yes';
      this.tempservice.showIncomeBenefit = false;
    }

    // if( this.tempservice.HeartCancer_InputArr['HospitalBenefit'] == 'Yes'){
    //   if(newBenefits!="")
    //     newBenefits += '_Hospital Cash Benefit';
    //   else
    //     newBenefits += 'Hospital Cash Benefit';
    //     hospitalBenefitPremium = this.premiumCalc.hospitalBenefitAmount;
    // }
    // if( this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] == 'Yes'){
    //   if(newBenefits!="")
    //   newBenefits += '_Increasing Cover benefit';
    // else
    //   newBenefits += 'Increasing Cover benefit';
    //   increasingBenefitPremium = this.premiumCalc.increasingBenefitAmount;
    // }
    // if( this.tempservice.HeartCancer_InputArr['IncomeBenefit'] == 'Yes'){
    //   if(newBenefits!="")
    //   newBenefits += '_Income Benefit';
    // else
    //   newBenefits += 'Income Benefit';
    //   incomeBenefitPremium = this.premiumCalc.incomeBenefitAmount;
    // }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
//     var splitdob = this.tempservice.userInput["dob"].split("/");
//     var newdate = splitdob[1]+"/"+splitdob[0];
//     try{
//     this.tempservice.WindowRef.nativeWindow.dataLayer.push({
//           'event' : this.tempservice.device + "_HnCCalculator_SelectRiderOption",
//           'Data_Prefilled' : "No",
//           'ProductCode' : "T48",
//           'ProductName' : "HeartCancer",
//           'pageName' : "protection : health-insurance-plans : heart-cancer-protect-calculator",
//           'pagetype' : "top-health : heart-cancer-protect-calculator",
//           'subsection1' : "health-insurance-plans",
//           'subsection2' : "heart-cancer-protect-calculator",
//           'subsection3' : "",
//           'channel' : "protection",
//           'Subchannel' : "health",
//           'PageCategory' : "Calculator",
//           'PageSubCategory' : "Calculator : Rider Section|Last Step",
//           'SectionName' : "Rider Section|Last Section",
//           'UserType': "Guest",
//           'UserRole' : "BOL",
//           'UserVisit' : "New",
//           'Age' : this.tempservice.userInput["age"],
//           'DOB' : newdate,
//           'Gender' : this.tempservice.userInput["gender"],
//           'Tobacco' : this.tempservice.userInput["tobacco"],
//           'CancerCover' :  parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
//           'HeartCover' : parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
//           'totalCover' : parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
//           'MonthlyPremium' :  this.tempservice.dataLayerArray.MonthlyPremium,
//           'PaymentFrequency' : this.tempservice.HeartCancer_InputArr["Frequency"],
//           'PolicyTerm' : parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
//           'Premium' : parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
//           'YearlyPremium' : this.tempservice.dataLayerArray.YearlyPremium,
//           'nypremium' : this.tempservice.dataLayerArray.nypremium,
//           'PolicyType' : this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
//           'PaymentTerm' :  parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
//           'Device' : this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
//           'NetworkUID1' :  sha1(this.tempservice.userInput["mobile"]),
//           'NetworkUID2' : sha1(this.tempservice.userInput["email"]),
//           'ICICIEmployee' : this.tempservice.HeartCancer_InputArr["Staff"],
//           'AnnualIncome' : this.tempservice.userInput["annualPackage"].replace(/\s/g, ""),
//           'Pcode' : this.tempservice.userInput["pincode"],
//           'OptionSelected' : newBenefits, //this.benefitsAdded.join(', ')
//           'HospitalBenefitPremium' : hospitalBenefitPremium,
//           'IncreasingBeneiftPremium' : increasingBenefitPremium,
//           'IncomeBenefitPremium' : incomeBenefitPremium,
//           'SpouseAdded' :  this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
//           'UID' : parseInt(this.tempservice.HeartCancer_InputArr.UID)
//     });
//   }catch(e){}
  }

  changeRemove(): void {
    this.tempservice.showHealthBenefit = true;
    // var newBenefits = "";
    var removedBenefits = "";
    // var hospitalBenefitPremium = 0;
    
    // var increasingBenefitPremium = 0;
    // var incomeBenefitPremium = 0;

    if (this.Type === 'Hospital') {
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = 'No';
      this.tempservice.showHealthBenefit = true;
    } else if (this.Type === 'IncreasingBenefit') {
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = 'No';
      this.tempservice.showIncreasingBenefit = true;
    } else if (this.Type === 'IncomeBenefit') {
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = 'No';
      this.tempservice.showIncomeBenefit = true;
    }
    
    if (this.tempservice.HeartCancer_InputArr['HospitalBenefit'] == 'No') {
      if(removedBenefits!="")
        removedBenefits += ',Hospital Cash Benefit';
      else
        removedBenefits += 'Hospital Cash Benefit';
    }
    if (this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] == 'No') {
      if(removedBenefits!="")
        removedBenefits += ',Hospital Cash Benefit_IncreasingIncomeBenefit';
      else
        removedBenefits += 'Hospital Cash Benefit_IncreasingIncomeBenefit';
    } 
    if (this.tempservice.HeartCancer_InputArr['IncomeBenefit'] == 'No') {
      if(removedBenefits!="")
        removedBenefits += ',Hospital Cash Benefit_IncomeBenefit';
      else
        removedBenefits += 'Hospital Cash Benefit_IncomeBenefit';
    }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
    // if( this.tempservice.HeartCancer_InputArr['HospitalBenefit'] == 'Yes'){
    //   if(newBenefits!="")
    //     newBenefits += '_Hospital Cash Benefit';
    //   else
    //     newBenefits += 'Hospital Cash Benefit';
    //     hospitalBenefitPremium = this.premiumCalc.hospitalBenefitAmount;
    // }
    // if( this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] == 'Yes'){
    //   if(newBenefits!="")
    //     newBenefits += '_Increasing Cover benefit';
    //   else
    //     newBenefits += 'Increasing Cover benefit';
    //     increasingBenefitPremium = this.premiumCalc.increasingBenefitAmount;
    // }
    // if( this.tempservice.HeartCancer_InputArr['IncomeBenefit'] == 'Yes'){
    //   if(newBenefits!="")
    //     newBenefits += '_Income Benefit';
    //   else
    //     newBenefits += 'Income Benefit';
    //     incomeBenefitPremium = this.premiumCalc.incomeBenefitAmount;
    // }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
//     var splitdob = this.tempservice.userInput["dob"].split("/");
//     var newdate = splitdob[1]+"/"+splitdob[0];
//     try{
//     this.tempservice.WindowRef.nativeWindow.dataLayer.push({
//           'event' : "Desktop_HnCCalculator_RemoveRiderOption",
//           'Data_Prefilled' : "No",
//           'ProductCode' : "T48",
//           'ProductName' : "HeartCancer",
//           'pageName' : "protection : health-insurance-plans : heart-cancer-protect-calculator",
//           'pagetype' : "top-health : heart-cancer-protect-calculator",
//           'subsection1' : "health-insurance-plans",
//           'subsection2' : "heart-cancer-protect-calculator",
//           'subsection3' : "",
//           'channel' : "protection",
//           'Subchannel' : "health",
//           'PageCategory' : "Calculator",
//           'PageSubCategory' : "Calculator : Rider Section|Last Step",
//           'SectionName' : "Rider Section|Last Section",
//           'UserType': "Guest",
//           'UserRole' : "BOL",
//           'UserVisit' : "New",
//           'Age' : this.tempservice.userInput["age"],
//           'DOB' : newdate,
//           'Gender' : this.tempservice.userInput["gender"],
//           'Tobacco' : this.tempservice.userInput["tobacco"],
//           'CancerCover' :  parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
//           'HeartCover' : parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
//           'totalCover' : parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
//           'MonthlyPremium' :  this.tempservice.dataLayerArray.MonthlyPremium,
//           'PaymentFrequency' : this.tempservice.HeartCancer_InputArr["Frequency"],
//           'PolicyTerm' : parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
//           'Premium' : parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
//           'YearlyPremium' : this.tempservice.dataLayerArray.YearlyPremium,
//           'nypremium' : this.tempservice.dataLayerArray.nypremium,
//           'PolicyType' : this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
//           'PaymentTerm' :  parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
//           'Device' : this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
//           'NetworkUID1' :  sha1(this.tempservice.userInput["mobile"]),
//           'NetworkUID2' : sha1(this.tempservice.userInput["email"]),
//           'ICICIEmployee' : this.tempservice.HeartCancer_InputArr["Staff"],
//           'AnnualIncome' : this.tempservice.userInput["annualPackage"].replace(/\s/g, ""),
//           'Pcode' : this.tempservice.userInput["pincode"],
//           'OptionSelected' : newBenefits, //this.benefitsAdded.join(', ')
//           'HospitalBenefitPremium' : hospitalBenefitPremium,
//           'IncreasingBeneiftPremium' : increasingBenefitPremium,
//           'IncomeBenefitPremium' : incomeBenefitPremium,
//           'OptionRemoved' : removedBenefits,
//           'SpouseAdded' :  this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
//           'UID' : parseInt(this.tempservice.HeartCancer_InputArr.UID)
//     });
//   }catch(e){}
  }

  showBenefitAmounts(): void {
    if ((this.tempservice.userInput['Frequency']) == 'Yearly' || (this.tempservice.HeartCancer_InputArr['Frequency']) == 'Yearly') {
      this.tempservice.flag['Inputfields']['showBreakupFrequency'] = 'Yearly';
    } else {
      this.tempservice.flag['Inputfields']['showBreakupFrequency'] = 'Monthly';
    }
  }
}
