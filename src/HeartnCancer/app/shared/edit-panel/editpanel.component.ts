import { Component, OnInit, ViewChild } from '@angular/core';
// import { PopupComponent } from '../popup/popup.component';
import { GlobalServiceService } from '../../service/global-service.service';
import { TempServiceService } from '../../service/temp-service.service';
// import { WindowRefService } from '../../service/window-ref.service';
import { PremiumCalculateService } from '../../main-forms/Premium/premium-calculate.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';
import { WindowRefService } from 'src/Common/services/window-ref.service';
// import { WindowRefService } from 'src/app/service/window-ref.service';
// import { GlobalServiceService } from 'src/app/service/global-service.service';
// import { TempServiceService } from 'src/app/service/temp-service.service';
// import { PopupComponent } from 'src/app/shared/popup/popup.component';
// import { PremiumCalculateService } from 'src/app/main-forms/Premium/premium-calculate.service';

@Component({
  selector: 'app-editpanel',
  templateUrl: './editpanel.component.html',
  styleUrls: ['./editpanel.component.css']
})
export class EditpanelComponent implements OnInit {
  showEditBar = false;
  showpanel;
  dobErrorMsg;
  pincodeErrorMsg: any = "";
  emailErrorMsg: any = "";
  mobileErrorMsg: any = "";
  nameErrorMsg: any = "";
  editshowHospBenefit: boolean = false;
  editshowIncrBenefit: boolean = false;
  editshowIncBenefit: boolean = false;
  disableCancerDiv: boolean = true;
  disableHeartDiv: boolean = true;
  Male: boolean;
  Female: boolean;
  showContent: string;

  @ViewChild('removecancer') removecancer: PopupComponent;
  @ViewChild('removeheart') removeheart: PopupComponent;

  
  constructor(public globalService: GlobalServiceService, public tempservice: TempServiceService,public WindowRef: WindowRefService,public premiumCalc: PremiumCalculateService) {

    this.showpanel = false;
    this.tempservice.flagData.subscribe(() => {
      this.Male = this.tempservice.flag['Male'];
      this.Female = this.tempservice.flag['Female'];
      if (this.tempservice.flag['Male']) {
        this.showContent = 'Wife';
      } else {
        this.showContent = 'Husband';
      }
      if (this.tempservice.flag['husbandWife']) {
        if (this.tempservice.userInput['storedMale'] == true) {
          this.Male = true;
          this.Female = false;
          this.showContent = 'Wife';
        } else {
          this.Male = false;
          this.Female = true;
          this.showContent = 'Husband';
        }
      }
    });

  }

  ngOnInit() {
    this.premiumCalc.calculatePremium();
    this.Male = this.tempservice.flag['Male'];
    this.Female = this.tempservice.flag['Female'];
    if (this.tempservice.flag['Male']) {
      this.showContent = 'Wife';
    } else {
      this.showContent = 'Husband';
    }
    if (this.tempservice.flag['husbandWife']) {
      if (this.tempservice.userInput['storedMale'] == true) {
        this.Male = true;
        this.Female = false;
        this.showContent = 'Wife';
      } else {
        this.Male = false;
        this.Female = true;
        this.showContent = 'Husband';
      }
    }
  }
  showEditTaxes() {
    this.tempservice.InclusiveOfTaxes();
  }

  updateSumAssuredVal(sumAssured) {
    console.log('Edit Comp ' + sumAssured);
  }

  show() {
    this.showEditBar = !this.showEditBar;
    this.showpanel = !this.showpanel;
    // try{
    //   this.tempservice.datalayer['Behavior'] = "Edit Panel Clicked(right end of screen)";
    //   this.WindowRef.nativeWindow._satellite.track("Behavior");
    // }catch(e){}
  }
  closeEdit() {
    // if (this.dobErrorMsg !="" || this.mobileErrorMsg != "" || this.emailErrorMsg != "" || this.pincodeErrorMsg != "" || this.nameErrorMsg != "") {
    //   console.log("Dont Close Outside Menu");
      
    //   this.showEditBar = true;
    //   // this.showpanel = true;
    // }
    // else {
    //   console.log("Closeedit outside Menu");
    //   this.showEditBar = false;
    //   // this.showpanel = true;
    // }
    this.showEditBar = false;
    this.showpanel = false;
  }
  // show() {
  //   if (this.dobErrorMsg != "" || this.mobileErrorMsg != "" || this.emailErrorMsg != "" || this.pincodeErrorMsg != "" || this.nameErrorMsg != "") {
  //     console.log("Dont Close Outside Menu");
  //     this.showEditBar = true;
  //     this.showpanel = true;
  //   }
  //   else {
  //     console.log("Closeedit outside Menu");
  //     this.showEditBar = false;
  //     this.showpanel = false;
  //   }
  // }
  closeEditCross() {
    this.showpanel = false;
  }

  openRemoveCancer() {
    this.removecancer.showDialog();
  }

  closeCancer() {
    this.removecancer.closeDialog();
  }

  cancerYes() {
    this.removecancer.closeDialog();
    this.globalService.flag.cover.CancerCover = false;
    this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'Heart';
    this.tempservice.flag['showHide']['hideRemoveHeartOption'] = false;
    this.tempservice.flag['showHide']['removeCancerCover'] = false;
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  openRemoveHeart() {
    this.removeheart.showDialog();
  }

  closeHeart() {
    this.removeheart.closeDialog();
  }

  heartYes() {
    this.removeheart.closeDialog();
    this.globalService.flag.cover.HeartCover = false;
    this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'Cancer';
    this.tempservice.flag['showHide']['hideRemoveCancerOption'] = false;
    this.tempservice.flag['showHide']['removeHeartCover'] = false;
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  onClick(fldId) {
    if ('personalInfo' === fldId) {
      this.globalService.flag.editScreen.personalInfo.mainDropDown = !this.globalService.flag.editScreen.personalInfo.mainDropDown;
      this.globalService.flag.editScreen.sec2active = false;
      this.globalService.flag.editScreen.sec3active = false;
      this.globalService.flag.editScreen.sec4active = false;
      this.globalService.flag.editScreen.sec5active = false;
    } else if ('addWifeCover' === fldId) {
      // this.globalService.flag.editScreen.personalInfo.addWifeCover = !this.globalService.flag.editScreen.personalInfo.addWifeCover;
      // this.globalService.flag.editScreen.sec2 = !this.globalService.flag.editScreen.sec2;
      // this.globalService.flag.editScreen.sec2active = !this.globalService.flag.editScreen.sec2active;
      this.globalService.flag.editScreen.personalInfo.removeWifeCover = !this.globalService.flag.editScreen.personalInfo.removeWifeCover;
      // this.globalService.flag.editScreen.personalInfo.mainDropDown = !this.globalService.flag.editScreen.personalInfo.mainDropDown;
      this.tempservice.flag['Inputfields']['editAddSpouse'] = true;
      this.tempservice.updateFlagData('Inputfields', this.tempservice.flag['Inputfields']);
      this.tempservice.HeartCancer_InputArr['LoyaltyBenefit'] = 'No';
      this.tempservice.HeartCancer_InputArr['FamilyBenefit'] = 'Yes';
	}  else if ('displayAddSpouse' === fldId) {
      this.globalService.flag.editScreen.personalInfo.addWifeCover = !this.globalService.flag.editScreen.personalInfo.addWifeCover;
      this.globalService.flag.editScreen.sec2 = !this.globalService.flag.editScreen.sec2;
      this.globalService.flag.editScreen.sec2active = !this.globalService.flag.editScreen.sec2active;
      // this.globalService.flag.editScreen.personalInfo.removeWifeCover = !this.globalService.flag.editScreen.personalInfo.removeWifeCover;
      this.globalService.flag.editScreen.personalInfo.mainDropDown = !this.globalService.flag.editScreen.personalInfo.mainDropDown;
      // this.tempservice.flag['Inputfields']['editAddSpouse'] = true;
      // this.tempservice.updateFlagData('Inputfields', this.tempservice.flag['Inputfields']);
      // this.tempservice.HeartCancer_InputArr['LoyaltyBenefit'] = 'No';
      // this.tempservice.HeartCancer_InputArr['FamilyBenefit'] = 'Yes';
    } else if ('removeWifeCover' === fldId) {
      this.globalService.flag.editScreen.sec2 = !this.globalService.flag.editScreen.sec2;
      this.globalService.flag.editScreen.sec2active = !this.globalService.flag.editScreen.sec2active;
      this.globalService.flag.editScreen.personalInfo.removeWifeCover = !this.globalService.flag.editScreen.personalInfo.removeWifeCover;
      this.tempservice.flag['showHide']['partnerCoverRemoveAdd'] = false;
      this.tempservice.flag['showHide']['showBreakUp'] = false;
      this.tempservice.flag['spouseTobaccoYes'] = false;
      this.tempservice.flag['spouseTobaccoNo'] = false;
      this.tempservice.flag['checkValidate']['spouseTobacco'] = false;

      this.tempservice.updateFlagData('showHide', this.tempservice.flag['showHide']);
      this.tempservice.updateFlagData('spouseTobaccoYes', false);
      this.tempservice.updateFlagData('spouseTobaccoNo', false);
      this.tempservice.updateFlagData('showHide', this.tempservice.flag['showHide']);
      this.tempservice.updateFlagData('husbandWife', false);
      this.tempservice.HeartCancer_InputArr['FamilyBenefit'] = 'No';
    } else if ('sec2' === fldId) {
      this.globalService.flag.editScreen.sec2active = !this.globalService.flag.editScreen.sec2active;
      this.globalService.flag.editScreen.sec3active = false;
      this.globalService.flag.editScreen.sec4active = false;
      this.globalService.flag.editScreen.sec5active = false;
      this.globalService.flag.editScreen.personalInfo.mainDropDown = false;
    } else if ('sec3' === fldId) {
      this.globalService.flag.editScreen.sec3active = !this.globalService.flag.editScreen.sec3active;
      this.globalService.flag.editScreen.sec2active = false;
      this.globalService.flag.editScreen.sec4active = false;
      this.globalService.flag.editScreen.sec5active = false;
      this.globalService.flag.editScreen.personalInfo.mainDropDown = false;
    } else if ('sec4' === fldId) {
      this.globalService.flag.editScreen.sec4active = !this.globalService.flag.editScreen.sec4active;
      this.globalService.flag.editScreen.sec3active = false;
      this.globalService.flag.editScreen.sec2active = false;
      this.globalService.flag.editScreen.sec5active = false;
      this.globalService.flag.editScreen.personalInfo.mainDropDown = false;
    } else if ('sec5' === fldId) {
      this.globalService.flag.editScreen.sec5active = !this.globalService.flag.editScreen.sec5active;
      this.globalService.flag.editScreen.sec4active = false;
      this.globalService.flag.editScreen.sec3active = false;
      this.globalService.flag.editScreen.sec2active = false;
      this.globalService.flag.editScreen.personalInfo.mainDropDown = false;
    } else if ('addHeartCover' === fldId) {
      if (this.globalService.flag.cover.HeartCover) {
        this.removeheart.showDialog();
        this.disableHeartDiv = false;
      } else {
        this.globalService.flag.cover.HeartCover = !this.globalService.flag.cover.HeartCover;
        this.disableHeartDiv = true;
        this.tempservice.updateHeartCancerInputArrValue(
          'LA1_HeartSumAssured',
          this.tempservice.constants['MIN_HeartCover']
        );
        this.tempservice.updateHeartCancerInputArrValue(
          'LA2_HeartSumAssured',
          this.tempservice.constants['MIN_HeartCover']
        );
        this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'CancerAndHeart';
        this.tempservice.flag['showHide']['hideRemoveCancerOption'] = true;
        this.tempservice.flag['showHide']['removeHeartCover'] = true;
      }

    } else if ('addCancerCover' === fldId) {
      if (this.globalService.flag.cover.CancerCover) {

        this.removecancer.showDialog();
        this.disableCancerDiv = false;
      }
      else {
        this.globalService.flag.cover.CancerCover = !this.globalService.flag.cover.CancerCover;
        this.disableCancerDiv = true;
        this.tempservice.updateHeartCancerInputArrValue(
          'LA1_CancerSumAssured',
          this.tempservice.constants['MIN_CancerCover']
        );
        this.tempservice.updateHeartCancerInputArrValue(
          'LA2_CancerSumAssured',
          this.tempservice.constants['MIN_CancerCover']
        );

        this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'CancerAndHeart';
        this.tempservice.flag['showHide']['hideRemoveHeartOption'] = true;
        this.tempservice.flag['showHide']['removeCancerCover'] = true;
      }
    }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  onKeyPress(evt, fieldID) {
    var fieldValue = evt.target.value;
    this.dobErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
  }

  showHospBenefit() {
    this.tempservice.showHealthBenefit  = !this.tempservice.showHealthBenefit ;
    if (!this.tempservice.showHealthBenefit) {
      this.tempservice.updateUserInputSubject('hospitalbenefit', 'Yes');
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = 'Yes';
    } else {
      this.tempservice.updateUserInputSubject('hospitalbenefit', 'No');
      this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = 'No';
    }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  showIncrBenefit() {
    this.tempservice.showIncreasingBenefit = !this.tempservice.showIncreasingBenefit;
    if (!this.tempservice.showIncreasingBenefit) {
      this.tempservice.updateUserInputSubject('increasingbenefit', 'Yes');
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = 'Yes';
    } else {
      this.tempservice.updateUserInputSubject('increasingbenefit', 'No');
      this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = 'No';
    }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  showIncBenefit() {
    this.tempservice.showIncomeBenefit = !this.tempservice.showIncomeBenefit;
    if (!this.tempservice.showIncomeBenefit) {
      this.tempservice.updateUserInputSubject('incomebenefit', 'Yes')
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = 'Yes';
    } else {
      this.tempservice.updateUserInputSubject('incomebenefit', 'No')
      this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = 'No';
    }
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  onKeyUp(event: any, fieldID: any): void {
    const fieldValue = event.target.value;
    if (fieldID === 'mobile-number') {
      let v = event.target.value;
      event.target.value = v.replace(/[^0-9]/g, '');
      this.mobileErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.mobileErrorMsg == "") {
        this.tempservice.flag['checkValidate']['mobileDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['mobileDetail'] = false;
      }
    } else if (fieldID === 'first-name') {
      this.nameErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.nameErrorMsg == "") {
        this.tempservice.flag['checkValidate']['nameDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['nameDetail'] = false;
      }
    } else if (fieldID === 'email') {
      this.emailErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.emailErrorMsg == "") {
        this.tempservice.flag['checkValidate']['emailDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['emailDetail'] = false;
      }
    } else if (fieldID === 'pincode') {
      this.pincodeErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.pincodeErrorMsg == "") {
        this.tempservice.flag['checkValidate']['pincodeDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['pincodeDetail'] = false;
      }
    }
  }
}
