import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditpanelComponent } from '../editpanel.component';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { PlusminusModule } from '../../plusminus/plusminus/plusminus.module';
import { PopupModule } from 'src/Common/components/popup/popup/popup.module';
// import { PopupModule } from '../../popup/popup/popup.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TextMaskModule,
    PlusminusModule,
    PopupModule
  ],
  declarations: [EditpanelComponent],
  exports: [EditpanelComponent]
})
export class EditpanelModule { }
