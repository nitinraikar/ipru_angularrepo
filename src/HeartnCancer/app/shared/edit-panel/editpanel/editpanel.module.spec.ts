import { EditpanelModule } from './editpanel.module';

describe('EditpanelModule', () => {
  let editpanelModule: EditpanelModule;

  beforeEach(() => {
    editpanelModule = new EditpanelModule();
  });

  it('should create an instance', () => {
    expect(editpanelModule).toBeTruthy();
  });
});
