import { Component } from '@angular/core';

@Component({
  selector: 'app-basic-details-card',
  templateUrl: './basic-details-card.component.html',
  styleUrls: ['./basic-details-card.component.css']
})
export class BasicDetailsCardComponent {
  showTickGender =  false;
  validateVibrate = false;

  showtick(): void {
    this.showTickGender = true;
  }

  dateRemoveTick(): void {
    this.showTickGender = false;
  }

  animate(flag: boolean): void {
    if (flag) {
      this.validateVibrate = true;
    } else {
      this.validateVibrate = false;
    }
  }
}
