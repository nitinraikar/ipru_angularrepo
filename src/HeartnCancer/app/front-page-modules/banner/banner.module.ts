import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

import { BannerComponent } from './banner.component';
import { BannerRoutingModule } from './banner-routing.module';
import { InViewportModule } from '@thisissoon/angular-inviewport';

@NgModule({
  declarations: [ BannerComponent ],
  imports: [
    CommonModule,
    ScrollToModule.forRoot(),
    BannerRoutingModule,
    InViewportModule
  ],
  exports: [ BannerComponent ]
})

export class BannerModule {
  
 }
