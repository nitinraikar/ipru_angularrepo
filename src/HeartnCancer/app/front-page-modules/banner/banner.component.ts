import { Component, OnInit } from '@angular/core';
import { TempServiceService } from '../../service/temp-service.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  show = false;

  constructor(public tempservice: TempServiceService) { }

  ngOnInit(): void { }

  action(inewViewport) {
    if (!this.show) {
      this.show = inewViewport
    }
  }
  showQuote(): void {
    try {
      this.tempservice.WindowRef.nativeWindow.dataLayer.push({
        'event': this.tempservice.device + "_HnCCalculator_CheckPremiumQuote_Banner",
        'Data_Prefilled': "No",
        'ProductCode': "T48",
        'ProductName': "HeartCancer",
        'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator",
        'pagetype': "top-health : heart-cancer-protect-calculator",
        'subsection1': "health-insurance-plans",
        'subsection2': "heart-cancer-protect-calculator",
        'subsection3': "",
        'channel': "protection",
        'Subchannel': "health",
        'PageCategory': "Calculator",
        'PageSubCategory': "Calculator Landing Page",
        'SectionName': "Banner",
        'UserType': "Guest",
        'UserRole': "BOL",
        'UserVisit': "New",
        'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
        'UID': parseInt(this.tempservice.HeartCancer_InputArr.UID)
      });
    } catch (e) { }
  }
}
