import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FDisclaimerComponent } from './f-disclaimer.component';

const routes: Routes = [
  {
    path: '',
    component: FDisclaimerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FDisclaimerRoutingModule { }
