import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-f-disclaimer',
  templateUrl: './f-disclaimer.component.html',
  styleUrls: ['./f-disclaimer.component.css']
})
export class FDisclaimerComponent implements OnInit {

  isExpanded = false;
  constructor() {}

  ngOnInit(): void {}

  toggle(): void {
    if(this.isExpanded) {
      this.isExpanded = false;
    } else {
      this.isExpanded = true;
    }
  }

  scroll(): void {
    setTimeout(() => {
      if (this.isExpanded)
        document.getElementById('bottom')
        .scrollIntoView();
    }, 150);
  }
}
