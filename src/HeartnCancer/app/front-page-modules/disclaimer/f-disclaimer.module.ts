import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FDisclaimerRoutingModule } from './f-disclaimer-routing.module';
import { FDisclaimerComponent } from './f-disclaimer.component';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [FDisclaimerComponent],
  imports: [
    CommonModule,
    FDisclaimerRoutingModule,
    MatExpansionModule
  ]
})
export class FDisclaimerModule { }
