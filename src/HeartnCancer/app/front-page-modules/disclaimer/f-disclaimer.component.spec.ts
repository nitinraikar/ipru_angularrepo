import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FDisclaimerComponent } from './f-disclaimer.component';

describe('FDisclaimerComponent', () => {
  let component: FDisclaimerComponent;
  let fixture: ComponentFixture<FDisclaimerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FDisclaimerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FDisclaimerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
