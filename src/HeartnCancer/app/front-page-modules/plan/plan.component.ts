import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { SwiperDirective } from 'ngx-swiper-wrapper';
import { Router } from '@angular/router';
import { TempServiceService } from '../../service/temp-service.service';

// import { TempServiceService } from 'src/app/service/temp-service.service';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {

  @ViewChild(SwiperDirective) directive = undefined;
  isMaleContent = false;
  isFemaleContent = true;
  femaleTab = true;
  maleTab = false;
  showBullets = [];

  defaultImage = 'HeartnCancer/assets/images/favicon.png';
  worksFemale1 = 'HeartnCancer/assets/images/works-female-01.png';
  worksFemale2 = 'HeartnCancer/assets/images/works-female-02.png';
  worksFemale3 = 'HeartnCancer/assets/images/works-female-03.png';
  worksFemale4 = 'HeartnCancer/assets/images/works-female-04.png';

  worksmale1 = 'HeartnCancer/assets/images/works-male-01.png';
  worksmale2 = 'HeartnCancer/assets/images/works-male-02.png';
  worksmale3 = 'HeartnCancer/assets/images/works-male-03.png';
  worksmale4 = 'HeartnCancer/assets/images/works-male-04.png';

  offset = 100;
  show = false;

  constructor(public zone: NgZone, public tempservice: TempServiceService, private router: Router) {}

  ngOnInit(): void {
    this.intializeBullets();
    console.log("PLan gets loaded");
  }

  setPagination(index): void {
    this.showBullets.forEach((indexValue, obj) => {
      const actualIndex = index;
      if (obj <= actualIndex && indexValue !== undefined)
        this.showBullets[obj] = true;
      else
        this.showBullets[obj] = false;
    });
  }

  next(index): void {
    this.directive.nextSlide(600, true);
    this.setPagination(index);
  }

  prev(index): void {
    this.directive.prevSlide(600, true);
    this.setPagination(index);
  }

  intializeBullets(): void {
    this.showBullets = [];
    this.showBullets.push(true);
    this.showBullets.push(false);
    this.showBullets.push(false);
    this.showBullets.push(false);
    if (this.directive) {
      this.directive.setIndex(0, 600, true);
    }
  }

  femaleContent(): void {
    this.intializeBullets();
    this.isFemaleContent = true;
    this.isMaleContent = false;
    this.femaleTab = true;
    this.maleTab = false;
  }

  maleContent(): void {
    this.intializeBullets();
    this.isFemaleContent = false;
    this.isMaleContent = true;
    this.femaleTab = false;
    this.maleTab = true;
  }

  action(event: any) {
    if(event) {
      this.router.navigate([{ outlets: { video:['video'],fquestion:['f-question'],disclaimer:['f-disclaimer'] } }], { skipLocationChange: true }); 
    }
    if (!this.show) {
      this.show = event
    }
  }

}
// protected: ['protected'],plan: ['plan'],video:['video'],fquestion:['f-question'],disclaimer:['f-disclaimer']