import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReasonRoutingModule } from './reason-routing.module';
import { ReasonComponent } from './reason.component';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';
import { InViewportModule } from '@thisissoon/angular-inviewport';

@NgModule({
  declarations: [ReasonComponent],
  imports: [
    CommonModule,
    ReasonRoutingModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    }),
    InViewportModule
  ]
})
export class ReasonModule { }
