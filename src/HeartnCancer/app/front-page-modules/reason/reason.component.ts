import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reason',
  templateUrl: './reason.component.html',
  styleUrls: ['./reason.component.css']
})

export class ReasonComponent implements OnInit {
  
	defaultImage = 'HeartnCancer/assets/images/favicon.png';
	reasonIcon1 = 'HeartnCancer/assets/images/reason-icon-01.png';
	longPolicyTerm = 'HeartnCancer/assets/images/Long-policy-term.png';
	addOnThree = 'HeartnCancer/assets/images/3-add-on.png';
	reasonIcon4 = 'HeartnCancer/assets/images/reason-icon-04.png';
	show = false;
	offset = 100;

  constructor(private router:Router) {}
  // private router:Router
	ngOnInit(): void {
    console.log("Reason gets loaded");
  }

  action(inewViewport) {
    console.log("dsfdsfdsf");
    if(inewViewport) {
      this.router.navigate([{ outlets: { protected: ['protected'] ,plan: ['plan'] } }], { skipLocationChange: true }); 
    }
    if(!this.show) {
      this.show = inewViewport
    }
  }
}
