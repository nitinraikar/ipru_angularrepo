import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SWIPER_CONFIG, SwiperConfigInterface, SwiperModule } from 'ngx-swiper-wrapper';
import { LazyLoadImageModule, intersectionObserverPreset } from 'ng-lazyload-image';

import { ProtectedRoutingModule } from './protected-routing.module';
import { ProtectedComponent } from './protected.component';

import { InViewportModule } from '@thisissoon/angular-inviewport';


if ((window.screen.width < 800)) {
  var DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 'auto',
    centeredSlides: true,
  };
} else {
  var DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 'auto',
  };
}

@NgModule({
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    SwiperModule,
    LazyLoadImageModule.forRoot({
      preset: intersectionObserverPreset
    }),
    InViewportModule
  ],
  declarations: [ProtectedComponent],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    },
  ]
})

export class ProtectedModule { }
