import { Component, OnInit, ViewChild } from '@angular/core';
import { SwiperDirective } from 'ngx-swiper-wrapper';

// import { TempServiceService } from 'src/app/service/temp-service.service';
import { Router } from '@angular/router';
import { TempServiceService } from '../../service/temp-service.service';

@Component({
  selector: 'app-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.css']
})

export class ProtectedComponent implements OnInit {

  @ViewChild(SwiperDirective)
  directive = undefined;

  public config: any;
  public index: any;
  public showhideVar: boolean = true;
  public show = false;

  defaultImage = 'HeartnCancer/assets/images/favicon.png';
  image = 'HeartnCancer/assets/images/Claim-settlement.png';
  futureIcon2 = 'HeartnCancer/assets/images/future-icon-02.png';
  futureIcon3 = 'HeartnCancer/assets/images/future-icon-03.png';
  protectFuture = 'HeartnCancer/assets/images/protected-future-bg.jpg';
  offset = 100;

  constructor(public tempservice:TempServiceService,public router:Router) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.showhideVar = false;
    },2000);
  }

  action(inewViewport) {
    console.log("inviewport",inewViewport);
    if(inewViewport) {
      console.log("Gets Loaded");
      this.router.navigate([{ outlets: { reason: ['reason'] } }], { skipLocationChange: true }); 
    }
    if(!this.show) {
      this.show = inewViewport
    }
  }
}
// reason: ['reason']