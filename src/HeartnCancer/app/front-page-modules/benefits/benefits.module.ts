import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenefitsRoutingModule } from './benefits-routing.module';
import { BenefitsComponent } from './benefits.component';
import { InViewportModule } from '@thisissoon/angular-inviewport';

@NgModule({
  declarations: [BenefitsComponent],
  imports: [
    CommonModule,
    BenefitsRoutingModule,
    InViewportModule
  ]
})
export class BenefitsModule { }
