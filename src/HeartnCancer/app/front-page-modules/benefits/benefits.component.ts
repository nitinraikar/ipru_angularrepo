import { Component, ElementRef, HostListener, OnInit  } from '@angular/core';

import { TempServiceService } from '../../service/temp-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.css']
})
export class BenefitsComponent implements OnInit {
  constructor(public el: ElementRef, private tempservice: TempServiceService,private router:Router) {}
  show = false;
  ngOnInit(): void {}

  @HostListener('window:scroll', [])
  checkScroll(): void {
    const componentPosition = this.el.nativeElement.offsetTop;
    const scrollPosition =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;

    if (scrollPosition >= componentPosition) {
      this.tempservice.subject.next(true);
    } else {
      this.tempservice.subject.next(false);
    }
  }

  action(inewViewport) { 
    if(inewViewport) {
      this.router.navigate([{ outlets: {reason: ['reason'] } }], { skipLocationChange: true }); 
    }
    if(!this.show) {
      this.show = inewViewport
    }
  }
}
// banner: ['banner']