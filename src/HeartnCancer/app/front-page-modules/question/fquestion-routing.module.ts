import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FquestionComponent } from './fquestion.component';

const routes: Routes = [
  {
    path: '',
    component: FquestionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FquestionRoutingModule { }
