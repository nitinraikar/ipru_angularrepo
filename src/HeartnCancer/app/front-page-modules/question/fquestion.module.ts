import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FquestionRoutingModule } from './fquestion-routing.module';
import { FquestionComponent } from './fquestion.component';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [FquestionComponent],
  imports: [
    CommonModule,
    FquestionRoutingModule,
    MatExpansionModule
  ]
})
export class FquestionModule { }
