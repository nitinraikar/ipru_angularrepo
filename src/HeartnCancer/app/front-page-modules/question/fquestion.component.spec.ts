import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FquestionComponent } from './fquestion.component';

describe('FquestionComponent', () => {
  let component: FquestionComponent;
  let fixture: ComponentFixture<FquestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FquestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
