import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fquestion',
  templateUrl: './fquestion.component.html',
  styleUrls: ['./fquestion.component.css']
})
export class FquestionComponent implements OnInit {

  visible = false;
  showMoreButton = true;
  constructor() {}

  ngOnInit(): void {}

  showMore(): void {
    this.visible = true;
	this.showMoreButton = false;
  }
  showLess(): void {
    this.visible = false;
    this.showMoreButton = true;
  }
}
