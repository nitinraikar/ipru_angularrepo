import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  datavariable;
  maintxt;
  class;
  counter;
  Read;
  youtubeurl;
  videoDataJson = {};
  videoContentList;
  mainVideoContent;
  arrow;
  showAnimation;
  readMeFadeInAndOut;
  readMeFadeInAndOutVideo;
  varierty = false;
  constructor(private sanitizer: DomSanitizer) {
    
    this.mainVideoContent = {
      headingTxt:
        '#HeartAndCancer - New Health Plan that pays claim amount on detection',
      displayTxt:
        'Heart Disease &amp; Cancer are two of the most common lifestyle-related illnesses today. They are impacting people close to each and every one of us. Once detected, the road to recovery is long and expensive – with tests, scans, treatments and surger',
      hiddenTxt:
        'At such a time, the new specialised health plan Heart/Cancer Protect can be a lifesaver. It is a health insurance plan that pays claim amount immediately on detection. This means you will have money in hand to get the right treatment, and can win against the illness – Agar taiyaari sahi ho, toh jeet pakki hai!    \nGet a cover of up to Rs. 25 lakhs for Heart Diseases and Rs. 50 lakhs for Cancer.    \nTo know more about ICICI Pru Heart/Cancer Protect, visit www.iciciprulife.com'
    };
    this.videoContentList = {
      videoContent1: true,
      videoContent1txt: 'Read more ',
      videoContent2: true,
      videoContent2txt: 'Read more ',
      videoContent3: true,
      videoContent3txt: 'Read more ',
      videoContent4: true,
      videoContent4txt: 'Read more ',
      videoContent5: true,
      videoContent5txt: 'Read more ',
      videoContent6: true,
      videoContent6txt: 'Read more ',
      videoContent7: true,
      videoContent7txt: 'Read more '
    };
    this.videoDataJson = {
      video1: {
        headingTxt:
          '#HeartAndCancer - New Health Plan that pays claim amount on detection',
        displayTxt:
          'Heart Disease &amp; Cancer are two of the most common lifestyle-related illnesses today. They are impacting people close to each and every one of us. Once detected, the road to recovery is long and expensive – with tests, scans, treatments and surger',
        hiddenTxt:
          'At such a time, the new specialised health plan Heart/Cancer Protect can be a lifesaver. It is a health insurance plan that pays claim amount immediately on detection. This means you will have money in hand to get the right treatment, and can win against the illness – Agar taiyaari sahi ho, toh jeet pakki hai!    \nGet a cover of up to Rs. 25 lakhs for Heart Diseases and Rs. 50 lakhs for Cancer.    \nTo know more about ICICI Pru Heart/Cancer Protect, visit www.iciciprulife.com',
        //src:'https://youtube.com/embed/AOkahnu2AxU?autoplay=0&amp;rel=0&amp;showinfo=0&amp;autohide=1'
        src: 'https://youtu.be/6zZETWsiFvM'
      },
      video2: {
        headingTxt:
          '#Listentobody - Catch the signs early. Protect yourself financially.',
        displayTxt: 'One life-saving truth that we all m',
        hiddenTxt:
          'iss, but Sumit understood. Watch the film to know more.\n\nVisit goo.gl/Anhwsg to register for a health check-up today.',
        src:
          'https://youtube.com/embed/f16WXvLhQIU?autoplay=0&rel=0&showinfo=0&autohide=1'
      },
      video3: {
        headingTxt: 'ICICI Pru Heart/Cancer Protect',
        displayTxt: 'When a critical illness like cancer',
        hiddenTxt:
          ' strikes, you expect the worst to happen. However, today you have the power to change this and protect yourself. Watch to know more. #YouProtectYouWin',
        src:
          'https://youtube.com/embed/HQfo_5vwR_E?autoplay=0&rel=0&showinfo=0&autohide=1'
      },
      video4: {
        headingTxt: 'ICICI Pru Heart/Cancer Protect',
        displayTxt: 'ICICI Pru Heart/Cancer Protect is a',
        hiddenTxt:
          ' health insurance plan which gives a lumpsum payout in the event of a cancer or heart disease.\n\nKnow More at https://goo.gl/PeLSPe',
        //src:'https://youtube.com/embed/h9mtx6NpJas?autoplay=0&rel=0&showinfo=0&autohide=1'
        src:'https://youtu.be/Qd-Rh_-zMMY'
      },
      video5: {
        headingTxt:
          'ICICI Pru Heart/Cancer Protect - Zero premiums after diagnosis',
        displayTxt: 'With ICICI Pru Heart/Cancer Protect',
        hiddenTxt:
          ', you do not have to pay any premiums after a minor diagnosis. The policy will continue for the rest of the term without you having to pay any premiums.',
        src:
          'https://youtube.com/embed/rmuXyfert9w?autoplay=0&rel=0&showinfo=0&autohide=1'
      },
      video6: {
        headingTxt: 'ICICI Pru Heart/Cancer Protect - Multiple claims',
        displayTxt: 'ICICI Pru Heart/Cancer Protect pays',
        hiddenTxt: ' out on multiple claims of cancer, protecting you fully.',
        src:
          'https://youtube.com/embed/NN-fGsk6Itg?autoplay=0&rel=0&showinfo=0&autohide=1'
      },
      video7: {
        headingTxt: 'ICICI Pru Heart/Cancer Protect - Cancel all worries',
        displayTxt: 'Stay worry free with ICICI Pru Hear',
        hiddenTxt:
          't/Cancer Protect, a health insurance plan that pays on diagnosis.',
        src:
          'https://youtube.com/embed/FxzWYlFly2Y?autoplay=0&rel=0&showinfo=0&autohide=1'
      }
    };
    this.arrow = true;
    this.youtubeurl = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://youtube.com/embed/AOkahnu2AxU?autoplay=0&amp;rel=0&amp;showinfo=0&amp;autohide=1'
    );
    this.maintxt = true;
    this.readMeFadeInAndOut = true;
    this.readMeFadeInAndOutVideo = {
      videoContent1: true,
      videoContent2: true,
      videoContent3: true,
      videoContent4: true,
      videoContent5: true,
      videoContent6: true,
      videoContent7: true
    };
    this.class = '';
    this.Read = 'Read more ';
    this.datavariable = true;
  }

  ngOnInit(): void {
    this.counter = 0;
  }

  Readmoretxt(fldID): void {
    if (fldID === 'maintxt') {
      this.maintxt = !this.maintxt;
      if (!this.maintxt) {
        this.Read = 'Read less ';
        this.arrow = false;
        this.showAnimation = true;
        this.readMeFadeInAndOut = false;
      } else {
        this.Read = 'Read more ';
        this.arrow = true;
        this.showAnimation = true;
        setTimeout(() => {
          this.readMeFadeInAndOut = true;
        }, 1000);
      }
    }
  }

  opac(): void {
    this.datavariable = true;
    setTimeout(() => {
      this.datavariable = false;
    }, 3000);
  }

  onClickEvt(fldID, evt): void {
    evt.stopPropagation();
    if (this.videoContentList.hasOwnProperty(fldID)) {
      this.videoContentList[fldID] = !this.videoContentList[fldID];
      if (!this.videoContentList[fldID]) {
        this.videoContentList[fldID + 'txt'] = 'Read less ';
        this.readMeFadeInAndOutVideo[fldID] = false;
      } else {
        this.videoContentList[fldID + 'txt'] = 'Read more ';
        setTimeout(() => {
          this.readMeFadeInAndOutVideo[fldID] = true;
        }, 1000);
      }
    } else if (this.videoDataJson.hasOwnProperty(fldID)) {
      this.mainVideoContent.headingTxt = this.videoDataJson[fldID][
        'headingTxt'
      ];
      this.mainVideoContent.displayTxt = this.videoDataJson[fldID][
        'displayTxt'
      ];
      this.mainVideoContent.hiddenTxt = this.videoDataJson[fldID]['hiddenTxt'];
      this.youtubeurl = this.sanitizer.bypassSecurityTrustResourceUrl(
        this.videoDataJson[fldID]['src']
      );
    }
  }

  nextprev(fldID): void {
    if (fldID === 'next' && this.class === '')
      this.class = 'class' + ++this.counter;
    else if (fldID === 'next') {
      if (this.counter < 3) this.class = 'class' + ++this.counter;
    }
    if (fldID === 'previous') {
      if (this.counter > 0) {
        this.class = 'class' + --this.counter;
        if (this.class === 'class0') {
          this.class = 'initial';
        }
      }
    }
  }

}
