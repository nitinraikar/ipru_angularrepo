import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { animate, style , transition, trigger } from '@angular/animations';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Headers } from '@angular/http';

// import { WindowRefService } from 'src/app/service/window-ref.service';
// import { StepsComponent } from 'src/app/shared/steps/steps.component';
// import { TempServiceService } from 'src/app/service/temp-service.service';
// import { GlobalServiceService } from 'src/app/service/global-service.service';
// import { EditpanelComponent } from 'src/app/shared/edit-panel/editpanel.component';
import { PremiumCalculateService } from '../../Premium/premium-calculate.service';
import { EditpanelComponent } from 'src/HeartnCancer/app/shared/edit-panel/editpanel.component';
import { StepsComponent } from 'src/HeartnCancer/app/shared/steps/steps.component';
import { TempServiceService } from 'src/HeartnCancer/app/service/temp-service.service';
import { GlobalServiceService } from 'src/HeartnCancer/app/service/global-service.service';
import { WindowRefService } from 'src/Common/services/window-ref.service';
// import { WindowRefService } from 'src/HeartnCancer/app/service/window-ref.service';

declare const require: any;
var sha1 = require('sha1');

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css'],
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translate3d(100%, 0, 0)'}),
        animate(400)
      ])
    ])
  ]
})
export class PersonalDetailsComponent implements OnInit {
  mobileErrorMsg: any = '';
  nameErrorMsg: any = '';
  emailErrorMsg: any = '';
  pincodeErrorMsg: any = '';
  Glb_keyword = "";
  Glb_matchtype = ""
  @ViewChild('showeditMenu') showeditMenu: EditpanelComponent;
  @ViewChild('showBullet') showBullet: StepsComponent;

  constructor(public http:HttpClient,private router: Router, public tempservice: TempServiceService, public globalServices:GlobalServiceService,public WindowRef: WindowRefService,public premiumCalc: PremiumCalculateService) { 
    this.router.events.subscribe(() => {
     document.body.className = "bodySticky";
    let isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent)
    if (isIEOrEdge == true) {
      setTimeout(() => {
        
        document.getElementById("personal-details").scrollIntoView({ behavior: "smooth", block: 'start', inline: 'nearest'});
      }, 5)
    } else {
      document.getElementById("personal-details").scrollIntoView(true);
    }
  });
  }

  ngOnInit(): void {
    this.showBullet.showBullets('PRD');
  }

  showEditMenu(): void {
    this.showeditMenu.show();
  }
  checkPremium(): void {
    if (this.tempservice.flag['checkValidate']['mobileDetail'] && this.tempservice.flag['checkValidate']['nameDetail'] && this.tempservice.flag['checkValidate']['emailDetail'] && this.tempservice.flag['checkValidate']['pincodeDetail']) {
      this.globalServices.flag.editScreen.sec4 = true;
      this.globalServices.flag.editScreen.sec5 = true;
      this.globalServices.flag.editScreen.sec3active = false;
      this.globalServices.flag.editScreen.personalInfo.mainDropDown = false;
      this.globalServices.flag.editScreen.sec2active = false;
      this.router.navigate(['/benefit']);
      
       var splitdob = this.tempservice.userInput["dob"].split("/");
       var newdate = splitdob[1]+"/"+splitdob[0];
       try{
         this.tempservice.WindowRef.nativeWindow.dataLayer.push({
               'event' : this.tempservice.device+"_HnCCalculator_LeadSubmitted",
               'Data_Prefilled' : "No",
               'ProductCode' : "T48",
               'ProductName' : "HeartCancer",
               'pageName' : "protection : health-insurance-plans : heart-cancer-protect-calculator",
               'pagetype' : "top-health : heart-cancer-protect-calculator",
               'subsection1' : "health-insurance-plans",
               'subsection2' : "heart-cancer-protect-calculator",
               'subsection3' : "",
               'channel' : "protection",
               'Subchannel' : "health",
               'PageCategory' : "Calculator",
               'PageSubCategory' : "Calculator-Lead",
               'SectionName' : "Lead Section",
               'UserType': "Guest",
               'UserRole' : "BOL",
               'UserVisit' : "New",
               'Age' : this.tempservice.userInput["age"],
               'DOB' : newdate,
               'Gender' : this.tempservice.userInput["gender"],
               'Tobacco' : this.tempservice.userInput["tobacco"],
               'CancerCover' :  parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
               'HeartCover' : parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
               'totalCover' : parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
               'MonthlyPremium' :  this.tempservice.dataLayerArray.MonthlyPremium,
               'PaymentFrequency' : this.tempservice.HeartCancer_InputArr["Frequency"],
               'PolicyTerm' : parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
               'Premium' : parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
               'YearlyPremium' : this.tempservice.dataLayerArray.YearlyPremium,
               'nypremium' : this.tempservice.dataLayerArray.nypremium,
               'PolicyType' : this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
               'PaymentTerm' :  parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
               'NetworkUID1' :  sha1(this.tempservice.userInput["mobile"]),
               'NetworkUID2' : sha1(this.tempservice.userInput["email"]),
               'ICICIEmployee' : this.tempservice.HeartCancer_InputArr["Staff"],
               'AnnualIncome' : this.tempservice.userInput["annualPackage"].replace(/\s/g, ""),
               'Pcode' : this.tempservice.userInput["pincode"],
               'LeadCount' : 1,
               'Device' : this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
               'SpouseAdded' :  this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
               'UID' : parseInt(this.tempservice.HeartCancer_InputArr.UID)
         });

       }catch(e){ }
       try{
         this.tempservice.datalayer = {
           'Section':"HnC Lead",
           'ProductCode':"T48",
           'Age': this.tempservice.userInput["age"],
           'Gender': this.tempservice.userInput["gender"],
           'Tobacco': this.tempservice.userInput["tobacco"],
           'TotalCover': this.tempservice.userInput["stringTotalCover"],
           'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
           'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'NetworkUID1' :  sha1(this.tempservice.userInput["mobile"]),
           'NetworkUID2' : sha1(this.tempservice.userInput["email"]),
           'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
           'SpouseAge': this.tempservice.userInput["Partnerage"],
           'SpouseTobacco': this.tempservice.HeartCancer_InputArr["LA2_Tobacco"],
           'AnnualIncome' : this.tempservice.userInput["annualPackage"].replace(/\s/g, ""),
           'Pincode' : this.tempservice.userInput["pincode"],
         }
         this.WindowRef.nativeWindow._satellite.track("HnC_Dec");
       }catch(e){}
      if(this.tempservice.SEM_WS_call_flag==0){
        this.tempservice.flag['Inputfields']['LeadFlag'] = true;
        this.fireLead();
      }

    }else{
      if(!this.tempservice.flag['checkValidate']['mobileDetail']){
        this.mobileErrorMsg = "Please Enter Mobile Number";
      }if(!this.tempservice.flag['checkValidate']['nameDetail']){
        this.nameErrorMsg = "Please Enter Your Name";
      }if(!this.tempservice.flag['checkValidate']['emailDetail']) {
        this.emailErrorMsg = "Please Enter Your Email";
      }if(!this.tempservice.flag['checkValidate']['pincodeDetail']) {
        this.pincodeErrorMsg = "Please Enter Pincode";
      }
    }
  }
  previousPage(): void {
    this.router.navigate(['/premium']);
  }

  onKeyUp(event: any, fieldID: any): void {
    const fieldValue = event.target.value;
    if (fieldID === 'mobile-number') {
      let v = event.target.value;
      event.target.value = v.replace(/[^0-9]/g, '');
      this.mobileErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.mobileErrorMsg == "") {
        this.tempservice.flag['checkValidate']['mobileDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['mobileDetail'] = false;
      }
    } else if (fieldID === 'first-name') {
      //let v = event.target.value;
      this.nameErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.nameErrorMsg == "") {
        this.tempservice.flag['checkValidate']['nameDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['nameDetail'] = false;
        this.nameErrorMsg = "Please Enter Valid Name";
      }
    } else if (fieldID === 'email') {
        this.emailErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
        if (this.emailErrorMsg == "") {
          this.tempservice.flag['checkValidate']['emailDetail'] = true;
        } else {
          this.tempservice.flag['checkValidate']['emailDetail'] = false;
        }
      }else if (fieldID === 'pincode'){
        this.pincodeErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
        if (this.pincodeErrorMsg == "") {
          this.tempservice.flag['checkValidate']['pincodeDetail'] = true;
        } else {
          this.tempservice.flag['checkValidate']['pincodeDetail'] = false;
        }
      }
      
    }
  changeCheked(): void {
    this.tempservice.flag['InfoSubmitFlag'] = !this.tempservice.flag['InfoSubmitFlag'];
    this.tempservice.flag['Inputfields']['TermsAndCond'] = this.tempservice.flag['InfoSubmitFlag'];
  }

  fireLead(): void{
      this.tempservice.SEM_WS_call_flag=1;
      var google_search_q="";
			//var google_search_q = ipru_getParameterByName('q', document.referrer);
			if(!google_search_q)
				google_search_q = '';
			
			if(this.tempservice.flag['Inputfields']['traiFlag'])
				var TRAI_FLAG='YES';
			else
				var TRAI_FLAG='NO';
			
			var EC_Status = this.tempservice.flag['Inputfields']['existingCustomeradded']?"Yes":"No";
				
			google_search_q = google_search_q.replace(/[\,\-\'\"\s]/g,'_');
			var product_specific = 'google_search_q-'+google_search_q + ',call-'+TRAI_FLAG+ ',ExistingCustomer-'+EC_Status;
			
			var datyArr = this.tempservice.userInput["dob"].split('/');
			var dob = datyArr[1] + '/' + datyArr[0] + '/' + datyArr[2];
			//var c2c_dob = datyArr[0] + '/' + datyArr[1] + '/' + datyArr[2];
				
			var tobacco = (this.tempservice.userInput["tobacco"] == "Yes")? 1 : 0 ;
			
			var objdata ={
				'LMS_uid': this.tempservice.HeartCancer_InputArr["UID"], 
				'LMS_product_code': 'T48', 
				'LMS_gender': this.tempservice.userInput["gender"], 
				'LMS_dob': dob, 
				'LMS_mobile': this.tempservice.userInput["mobile"], 
				'LMS_email': this.tempservice.userInput["email"], 
				'LMS_product_specific': product_specific, 
				'LMS_page_name': 'Website-HeartAndCancer-Calculator', 
				'keyword': this.Glb_keyword, 
				'matchtype': this.Glb_matchtype, 
				'LMS_TRAI_FLAG': TRAI_FLAG, 
				'LMS_firstName': this.tempservice.userInput["name"], 
				'LMS_lastName': "", 
				'LMS_tobacco': tobacco,
				'LMS_premium': this.tempservice.premiumAmount,
				'LMS_policypaymentfrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
				'LMS_policyTerm': this.tempservice.HeartCancer_InputArr["PolicyTerm"],
				'LMS_sumassuredPlus': this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"], 
				'LMS_sumassuredMinus': this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"], 
        'LMS_sumassured': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"])+parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
        'LMS_pageURL' : window.location.href.split("?")[0],
				'LMS_session_id' : "",
				'LMS_column_1' : "",
				'LMS_column_2' : "",
				'LMS_column_3' : "",
				'LMS_column_4' : "",
				'LMS_column_5' : "",
				'LMS_column_6' : ""
      };
      // $.ajax({
      //   url: "https://buy.iciciprulife.com/buy/leadWS.htm", 
      //   type:"POST",
      //   crossDomain: true,
      //   xhrFields: {
      //     withCredentials: true
      //   },
      //   data: $.param(objdata),
      //   success: function(res){ 
      //     console.log('Response=>' + res);
              // thisobj.otpApiCallFailed = false;
              // thisobj.otpGenerated = true;
              // thisobj.otpVerificationFailed = false;
              // //returnStatus = true;
              // var result = JSON.parse(res);
              // thisobj.clientId = result.clientId;
  
              // if(result.clientId=="No Client found for parematers passed." || (result.IPruException !== undefined && result.IPruException.status=="ERROR")){
              //   thisobj.otpVerificationFailed = true;
              //   //returnStatus = false ;
              // }else{
              //   thisobj.otpGenerated = true;
              //   thisobj.encodedmobile = result.mobileNo;
              //   //returnStatus = true;
              //   //return true;
              //   thisobj.Existingcustsucc();
                
              // }
      //   },
      //   error: function(error){
      //     console.log('Error Response=> ' + error);
      //     // thisobj.otpApiCallFailed = true;
      //     // thisobj.otpGenerated = false;
      //     // thisobj.otpVerificationFailed = false;
      //     // //returnStatus = false;
      //   },
      // });



			//var url = "https://www.iprusalesbeta.com/SellOnline_OTC/leadWS.htm"; /*DigiDrive UAT*/
      //var url = "https://www.iprusalesbeta.com/digiuat/leadWS.htm"; /*Temp DigiDrive UAT*/
      //var url = "https://beta.iciciprulife.com/DigiDrive/leadWS.htm"; /*DigiDrive PreProd*/
      var url = "https://buy.iciciprulife.com/buy/leadWS.htm"; /*Production*/

      //this.postIframe(url, "POST", objdata);
      var stringobj = JSON.stringify(objdata);
      var headers = new Headers();
      headers.append('Content-Type', 'application/json');
      // //1111headers.append('Content-Type', 'application/json');
      // let options = new RequestOptions({ headers: headers });

      this.http.post(url, stringobj).subscribe(
          res => {
            console.log('Response=>' + res);
            
          },
          (err: HttpErrorResponse) => {
              console.log('Error Response=> ' + err);
          }
      );
      this.fireLeadConversionPixels();
			// try{
      //           var req1 = {
      //                    "message":"comition",
      //                    "properties": {
      //                               "amount": "",
      //                               "gender": this.tempservice.userInput["gender"],
      //                               "cardType": "ICICI BANK",
      //                               "mobileNo": "9999999999",
      //                               "emailID": "abc@xyz.com",
      //                               "productCode": "T49",
      //                               "vendorUid": "",
      //                               "associationId": datyArr[2]+this.tempservice.userInput["mobile"].substr(this.tempservice.userInput["mobile"].length - 5),
      //                               "premium": "",
      //                               "message": window.location.hostname+window.location.pathname,
      //                               "deviceId": "19 Jun 1989",
      //                               "ipAddress": "" //ipAddress
      //                           }
			// 			};
						//dataLogging.sendCLog(req1);
      //}catch(e){}
      this.tempservice.fireConversion();
  }
  postIframe(target_url, method, params)
  {//Added for LMS calling ---- alternate way to implement cross domain ajax call by using hidden iframe -- KAUSHIK
    //Add iframe
    var iframe = document.createElement("iframe");
    document.body.appendChild(iframe);
    iframe.style.display = "none";
    
    //Give the frame a name
    var frame_name = "frame_name" + (new Date).getTime();
    iframe.contentWindow.name = frame_name;
  
    //build the form
    var form = document.createElement("form");
    form.target = frame_name;
    form.action = target_url;
    form.method = method;
    
    //loop through all parameters
    for (var key in params)
    {
      if (params.hasOwnProperty(key))
      {
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = key;
        input.value = params[key];
        form.appendChild(input);
      }
    }
  
    document.body.appendChild(form);
    form.submit();
  }
  
  fireLeadConversionPixels(): void{
    var gcc = new Image();
    var gcc1 = new Image();
    var rand_time = (new Date()).getTime();
    // var pixelurl = '';
    // var t;
    // var e = Glb_UID;
    // t = 'xxxxxxxx_xxxx_4xxx_yxxx_xxxxxxxxxxxx'.replace(/[xy]/g, function (e) {
    //   var t = 16 * Math.random() | 0,
    //   o = 'x' == e ? t : 3 & t | 8;
    //   return o.toString(16)
    // }),
    // t = t + '_' + (new Date).getTime();
    // var vCommId = t;
    gcc.src=window.location.protocol+'//www.googleadservices.com/pagead/conversion/964212897/?label=xe2DCLi55XAQofHiywM&amp;guid=ON&amp;script=0&rand_time='+rand_time;
    gcc1.src=window.location.protocol+'//www.googleadservices.com/pagead/conversion/850517359/?label=T_IjCPaByHMQ77rHlQM&amp;guid=ON&amp;script=0&rand_time='+rand_time;
  }
}