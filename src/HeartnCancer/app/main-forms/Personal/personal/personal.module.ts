import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { PersonalRoutingModule } from './personal-routing.module';
import { PersonalComponent } from './personal.component';
import { PersonalDetailsComponent } from '../personal-details/personal-details.component';
import { HttpClientModule } from '@angular/common/http';
import { StepsModule } from 'src/HeartnCancer/app/shared/steps/steps/steps.module';
import { EditpanelModule } from 'src/HeartnCancer/app/shared/edit-panel/editpanel/editpanel.module';
import { PlusminusModule } from 'src/HeartnCancer/app/shared/plusminus/plusminus/plusminus.module';
import { PopupModule } from 'src/Common/components/popup/popup/popup.module';

@NgModule({
  imports: [
    CommonModule,
    PersonalRoutingModule,
    PopupModule,
    StepsModule,
    FormsModule,
    EditpanelModule,
    PlusminusModule,
    HttpModule,
    HttpClientModule
  ],
  declarations: [
    PersonalComponent,
    PersonalDetailsComponent,
  ]
})
export class PersonalModule { }
