import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonalComponent } from './personal.component';
import { PersonalDetailsComponent } from '../personal-details/personal-details.component';

const routes: Routes = [
  {
    path: '',
    component: PersonalComponent,
    children: [
      { path: '', redirectTo: 'personal-details', pathMatch: 'full' },
      { path: 'personal-details', component: PersonalDetailsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalRoutingModule { }
