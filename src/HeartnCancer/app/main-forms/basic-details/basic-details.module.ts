import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicDetailsRoutingModule } from './basic-details-routing.module';
import { BasicDetailsComponent } from './basic-details.component';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { BasicDetailsCardComponent } from '../../shared/basic-details-card/basic-details-card.component';
import { StepsModule } from '../../shared/steps/steps/steps.module';

@NgModule({
  declarations: [
    BasicDetailsComponent,
    BasicDetailsCardComponent
  ],
  imports: [
    CommonModule,
    BasicDetailsRoutingModule,
    StepsModule,
    TextMaskModule,
    FormsModule,
  ]
})
export class BasicDetailsModule { }
