import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { TempServiceService } from '../../service/temp-service.service';
import { BasicDetailsCardComponent } from '../../shared/basic-details-card/basic-details-card.component';
import { GlobalServiceService } from '../../service/global-service.service';
import { StepsComponent } from '../../shared/steps/steps.component';
import { animate, style, transition, trigger } from '@angular/animations';
import { CookieService } from 'ngx-cookie-service';
import { BlowfishEncryptionService } from 'src/Common/services/blowfish-encryption.service';
import { WindowRefService } from 'src/Common/services/window-ref.service';


@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.css'],
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translate3d(-100%, 0, 0)' }),
        animate(400)
      ]),
    ])
  ]
})


export class BasicDetailsComponent implements OnInit, AfterViewInit {
  @ViewChild('tickGenderCard') tickGenderCard: BasicDetailsCardComponent;
  @ViewChild('tickDateCard') tickDateCard: BasicDetailsCardComponent;
  @ViewChild('tickTobaccoCard') tickTobaccoCard: BasicDetailsCardComponent;
  @ViewChild('showBullet') showBullet: StepsComponent;
  
  isvisible: boolean = true;
  isfemaleicon: boolean = true;
  istobacco: boolean = true;
  istobchange: boolean = true;
  showTooltip: boolean = false;
  showTickGender: boolean = false;
  showTickTobacco: boolean = false;
  dobErrorMsg: any = "";
  moveNextFlag: boolean;

  constructor(private router: Router, public tempservice: TempServiceService, public globalService: GlobalServiceService, public WindowRef: WindowRefService, public blowfishEncrypt: BlowfishEncryptionService, public cookieService: CookieService) {
  }

  ngOnInit() {

    this.showBullet.showBullets('BD');
    if (this.tempservice.premiumToLanding) {
      this.tickGenderCard.showtick();
      this.tickTobaccoCard.showtick();
      this.tickDateCard.showtick();
    }

    if (window.location.href.includes("?"))
      if (window.location.href.split("?")[1].match("Customise_HNC=true")) {
        var cookieData = this.cookieService.get('encryptedCookie_HNC');
        var cookieDataDecrypt = this.blowfishEncrypt.encryptData(cookieData, "!t@a6@($%(", 'decrypt');
        cookieDataDecrypt = cookieDataDecrypt.replace(/&/g, "\"\,\"");
        cookieDataDecrypt = "{" + cookieDataDecrypt.replace(/=/g, "\"\:\"") + "}";
        cookieDataDecrypt = cookieDataDecrypt.replace(/-/g, "—");
        var cookieDataJSON = JSON.parse(cookieDataDecrypt);
        this.tempservice.cookieStorage = cookieDataJSON;
        if (cookieDataJSON.Gender) {
          // this.tempservice.userInput['gender'] = cookieDataJSON.Gender;
          // this.tempservice.HeartCancer_InputArr.LA1_Gender = cookieDataJSON.Gender;
          // this.tempservice.flag['Inputfields'][cookieDataJSON.Gender] = true;
          if (cookieDataJSON.Gender == "Male") {
            // this.changeMaleIcon();
            document.getElementById('male').click();
          }
          else {
            // this.changeFemaleIcon();
            document.getElementById('female').click();
          }
        }
        if (cookieDataJSON.RiskA) {
          // this.tempservice.userInput['tobacco'] = cookieDataJSON.RiskA;
          // this.tempservice.HeartCancer_InputArr.LA1_Tobacco = cookieDataJSON.RiskA;
          // this.tempservice.flag['Inputfields'][cookieDataJSON.RiskA] = true;
          if (cookieDataJSON.RiskA == "Yes") {
            // this.changeYesIcon();
            document.getElementById('Yes').click();
          }
          else {
            // this.changeNoIcon();
            document.getElementById('No').click();
          }
        }
        if (cookieDataJSON.Cust_DOB) {
          this.tempservice.userInput['dob'] = cookieDataJSON.Cust_DOB;
          this.tempservice.HeartCancer_InputArr.LA1_DOB = cookieDataJSON.Cust_DOB;
          this.tempservice.flag['Inputfields'].dateofbirth = true;
          this.tickDateCard.showtick();
          this.tempservice.age = this.tempservice.getAge(cookieDataJSON.Cust_DOB);
          this.tempservice.ValidateHSA_LA1();
        }
        if (cookieDataJSON.Cust_DOB && cookieDataJSON.RiskA && cookieDataJSON.Gender && !this.tempservice.premiumToLanding) {
          // setTimeout(() => { this.tempservice.calculatePremium(); }, 2000);
          // document.getElementById('checkPremium').click();
          // this.checkPremium();
        }
        if (cookieDataJSON.Policy_Term) {
          this.tempservice.HeartCancer_InputArr.PolicyTerm = cookieDataJSON.Policy_Term;
          this.tempservice.setPolicyTermDropdownprimary = this.tempservice.setPolicyTermDropdown(this.tempservice.age);
          // this.tempservice.userInput['LA1_cancerCover'] = cookieDataJSON.Cancer_Cover;
        }
        if (cookieDataJSON.Heart_Cover) {
          // if(cookieDataJSON.Heart_Cover=="0"){
          //   this.tempservice.flag['Inputfields'].showHide.removeHeartCover = false;
          //   this.tempservice.flag['Inputfields'].showHide.hideRemoveHeartOption = false;
          // }
          this.tempservice.HeartCancer_InputArr.LA1_HeartSumAssured = cookieDataJSON.Heart_Cover;
          this.tempservice.userInput['LA1_heartCover'] = cookieDataJSON.Heart_Cover;
          // this.tempservice.calculatePremium();}
        }
        if (cookieDataJSON.Cancer_Cover) {
          // if(cookieDataJSON.Heart_Cover=="0"){
          //   this.tempservice.flag['Inputfields'].showHide.removeCancerCover = false;
          //   this.tempservice.flag['Inputfields'].showHide.hideRemoveHeartOption = false;
          // }
          this.tempservice.HeartCancer_InputArr.LA1_CancerSumAssured = cookieDataJSON.Cancer_Cover;
          this.tempservice.userInput['LA1_cancerCover'] = cookieDataJSON.Cancer_Cover;
          // this.tempservice.calculatePremium();}
        }
        if (cookieDataJSON.Payment_Frequency) {
          this.tempservice.HeartCancer_InputArr['Frequency'] = cookieDataJSON.Payment_Frequency;
          this.tempservice.userInput['Frequency'] = cookieDataJSON.Payment_Frequency;
        }
        if (cookieDataJSON.Cust_Spouse_DOB) {
          this.tempservice.userInput['spousedob'] = cookieDataJSON.Cust_Spouse_DOB;
        }
        if (cookieDataJSON.Spouse_RiskA) {
          this.tempservice.HeartCancer_InputArr.LA2_Tobacco = cookieDataJSON.Spouse_RiskA;
        }
        if (cookieDataJSON.Spouse_Heart_Cover) {
          this.tempservice.HeartCancer_InputArr.LA2_HeartSumAssured = cookieDataJSON.Spouse_Heart_Cover;
          this.tempservice.userInput['LA2_heartCover'] = cookieDataJSON.Spouse_Heart_Cover;
        }
        if (cookieDataJSON.Spouse_Heart_Cover) {
          this.tempservice.HeartCancer_InputArr.LA2_CancerSumAssured = cookieDataJSON.Spouse_Cancer_Cover;
          this.tempservice.userInput['LA2_cancerCover'] = cookieDataJSON.Spouse_Cancer_Cover;
        }

        if (cookieDataJSON.Cust_FirstName) {
          this.tempservice.userInput['name'] = cookieDataJSON.Cust_FirstName;
          this.tempservice.flag['checkValidate']['nameDetail'] = true;
          this.tempservice.flag["Inputfields"]["validName"] = true;
        }
        if (cookieDataJSON.MobileNo) {
          this.tempservice.userInput['mobile'] = cookieDataJSON.MobileNo;
          this.tempservice.flag['checkValidate']['mobileDetail'] = true;
          this.tempservice.flag["Inputfields"]["validMobNo"] = true;
        }
        if (cookieDataJSON.EmailId) {
          this.tempservice.userInput['email'] = cookieDataJSON.EmailId;
          this.tempservice.flag['checkValidate']['emailDetail'] = true;
          this.tempservice.flag["Inputfields"]["validEmail"] = true;
        }
        if (cookieDataJSON.Pincode) {
          this.tempservice.userInput['pincode'] = cookieDataJSON.Pincode;
          this.tempservice.flag["Inputfields"]["validPincode"] = true;
          this.tempservice.flag['checkValidate']['pincodeDetail'] = true;
        }
        if (cookieDataJSON.AnnualIncome) {
          this.tempservice.userInput['annualPackage'] = cookieDataJSON.AnnualIncome;
        }
        if (cookieDataJSON.Hospital_Cash_Benefit == "Yes") {
          // this.tempservice.addHospital();
          this.tempservice.HeartCancer_InputArr.HospitalBenefit = cookieDataJSON.Hospital_Cash_Benefit;
          // this.uid.Type='Hospital';
          // this.uid.changeAdd();}
          // this.tempservice.showHealthBenefit = false;
          // this.uid.Type = 'Hospital';
          // this.uid.changeAdd();    
        }
        if (cookieDataJSON.Income_Replacement_Cover == "Yes") {
          // this.tempservice.addIncomebenefits();
          // this.uid.Type='IncomeBenefit';
          // this.uid.changeAdd();
          // this.tempservice.showIncomeBenefit = false;
          // this.uid.Type = 'IncomeBenefit';
          // this.uid.changeAdd();
          this.tempservice.HeartCancer_InputArr.IncomeBenefit = cookieDataJSON.Income_Replacement_Cover;
        }
        if (cookieDataJSON.Increasing_Cover_Benefit == "Yes") {
          // this.tempservice.addIncreasings();
          // this.uid.Type='IncreasingBenefit';
          // this.uid.changeAdd();
          // this.tempservice.showIncreasingBenefit = false;
          // this.uid.Type = 'IncreasingBenefit';
          // this.uid.changeAdd();
          this.tempservice.HeartCancer_InputArr.IncreasingCoverBenefit = cookieDataJSON.Increasing_Cover_Benefit;
        }
      }
  }
  
  changeMaleIcon() {
    this.isvisible = false;
    this.isfemaleicon = true;
    this.tickGenderCard.showtick();
    this.restVibrateFlag();
  }

  changeFemaleIcon() {
    this.isfemaleicon = false;
    this.isvisible = true;
    this.tickGenderCard.showtick();
    this.restVibrateFlag();
  }

  changeYesIcon() {
    this.istobacco = false;
    this.istobchange = true;
    this.tickTobaccoCard.showtick();
    this.restVibrateFlag();
  }

  changeNoIcon() {
    this.istobchange = false;
    this.istobacco = true;
    this.tickTobaccoCard.showtick();
    this.restVibrateFlag();
  }

  toggle() {
    this.showTooltip = !this.showTooltip;
  }

  checkPremium() {
    this.moveNextFlag = true;
    if (!this.tickGenderCard.showTickGender) {
      this.tickGenderCard.animate(true);
      this.moveNextFlag = false
    }

    if (!this.tickDateCard.showTickGender) {
      this.tickDateCard.animate(true);
      this.moveNextFlag = false
    }

    if (!this.tickTobaccoCard.showTickGender) {
      this.tickTobaccoCard.animate(true);
      this.moveNextFlag = false
    }

    this.tempservice.flag['Inputfields']['QuoteProceedFlag'] = "Yes";
    var splitdob = this.tempservice.userInput["dob"].split("/");
    var newdate = splitdob[1] + "/" + splitdob[0];
    try {
      this.tempservice.WindowRef.nativeWindow.dataLayer.push({
        'event': this.tempservice.device + "_HnCCalculator_GetQuote",
        'Data_Prefilled': "No",
        'ProductCode': "T48",
        'ProductName': "HeartCancer",
        'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator",
        'pagetype': "top-health : heart-cancer-protect-calculator",
        'subsection1': "health-insurance-plans",
        'subsection2': "heart-cancer-protect-calculator",
        'subsection3': "",
        'channel': "protection",
        'Subchannel': "health",
        'PageCategory': "Calculator",
        'PageSubCategory': "Calculator Landing Page",
        'SectionName': "Calculator - PreQuoteScreen",
        'UserType': "Guest",
        'UserRole': "BOL",
        'UserVisit': "New",
        'Age': this.tempservice.userInput["age"],
        'DOB': newdate,
        'Gender': this.tempservice.userInput["gender"],
        'Tobacco': this.tempservice.userInput["tobacco"],
        'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
        'UID': parseInt(this.tempservice.HeartCancer_InputArr.UID)
      });
    } catch (e) { }
    try {
      this.tempservice.datalayer = {
        'Section': "HnC Checked Premium",
        'ProductCode': "T48",
        'Gender': this.tempservice.userInput["gender"],
        'Age': this.tempservice.userInput["age"],
        'Tobacco': this.tempservice.userInput["tobacco"],
        'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
        'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
        'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
        'TotalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
        'ProductName': "heart and cancer"
      }
      this.WindowRef.nativeWindow._satellite.track("HnC_Dec");
    } catch (e) { }
    if (this.moveNextFlag) {
      this.router.navigate(['/premium']);
      this.tempservice.premiumToLanding = false;
      this.globalService.flag.editScreen.personalInfo.mainDropDown = true;
    }
    setTimeout(() => { this.restVibrateFlag(); }, 1000);

  }

  onKeyPress(evt, fieldID) {
    const fieldValue = evt.target.value;
    this.dobErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
    if (this.dobErrorMsg == "") {
      this.tickDateCard.showtick();
      this.tempservice.ValidateHSA_LA1();
      this.tempservice.flag['Inputfields']['QuoteFlag'] = "Yes";
    }
    else {
      this.tickDateCard.dateRemoveTick();
      this.restVibrateFlag();
    }
  }

  onKeyDown(evt, fieldID) {
    const fieldValue = evt.target.value;
    this.dobErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
    if (this.dobErrorMsg == "") {
      this.tickDateCard.showtick();
    } else {
      this.tickDateCard.dateRemoveTick();
      this.restVibrateFlag();
    }
  }

  restVibrateFlag() {
    this.tickGenderCard.animate(false);
    this.tickDateCard.animate(false);
    this.tickTobaccoCard.animate(false);
  }

  ngAfterViewInit() {
    this.router.navigate([{ outlets: { plan :['plan'] } }], { skipLocationChange: true});
  }
}