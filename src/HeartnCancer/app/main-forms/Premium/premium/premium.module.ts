import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PremiumRoutingModule } from './premium-routing.module';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';

// import { PopupModule } from 'src/app/shared/popup/popup/popup.module';
// import { StepsModule } from 'src/app/shared/steps/steps/steps.module';
// import { EditpanelModule } from 'src/app/shared/edit-panel/editpanel/editpanel.module';
// import { PlusminusModule } from 'src/app/shared/plusminus/plusminus/plusminus.module';
// import { PremiumAmountModule } from 'src/app/shared/premium-amount/premium-amount/premium-amount.module';

import { PremiumComponent } from './premium.component';
import { PremiumPageComponent } from '../premium-page/premium-page.component';

import { PremiumCalculateService } from '../premium-calculate.service';
// import { HncEbiService } from 'src/app/service/hnc-ebi.service';
import { HttpClientModule } from '@angular/common/http';
// import { PopupModule } from 'src/HeartnCancer/app/shared/popup/popup/popup.module';
import { StepsModule } from 'src/HeartnCancer/app/shared/steps/steps/steps.module';
import { EditpanelModule } from 'src/HeartnCancer/app/shared/edit-panel/editpanel/editpanel.module';
import { PlusminusModule } from 'src/HeartnCancer/app/shared/plusminus/plusminus/plusminus.module';
import { PremiumAmountModule } from 'src/HeartnCancer/app/shared/premium-amount/premium-amount/premium-amount.module';
import { PopupModule } from 'src/Common/components/popup/popup/popup.module';
import { HncEbiService } from 'src/Common/services/hnc-ebi.service';
// import { HncEbiService } from 'src/HeartnCancer/app/service/hnc-ebi.service';

@NgModule({
  imports: [
    CommonModule,
    PremiumRoutingModule,
    FormsModule,
    PopupModule,
    StepsModule,
    TextMaskModule,
    EditpanelModule,
    PlusminusModule,
    PremiumAmountModule,
    HttpClientModule
  ],
  declarations: [
    PremiumComponent,
    PremiumPageComponent,
  ],
  providers:[PremiumCalculateService,HncEbiService,],
})

export class PremiumModule { }
