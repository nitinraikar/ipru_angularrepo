import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PremiumComponent } from './premium.component';
import { PremiumPageComponent } from '../premium-page/premium-page.component';


const routes: Routes = [
  {
    path: '',
    component: PremiumComponent,
    children: [
      { path: '', redirectTo: 'premium-page', pathMatch: 'full' },
      { path: 'premium-page', component: PremiumPageComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PremiumRoutingModule { }
