import { TestBed } from '@angular/core/testing';

import { PremiumCalculateService } from './premium-calculate.service';

describe('PremiumCalculateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PremiumCalculateService = TestBed.get(PremiumCalculateService);
    expect(service).toBeTruthy();
  });
});
