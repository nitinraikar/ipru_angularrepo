import { Injectable } from '@angular/core';
import { TempServiceService } from '../../service/temp-service.service';
import { HncEbiService } from 'src/Common/services/hnc-ebi.service';
// import { HncEbiService } from '../../service/hnc-ebi.service';
// import { HncEbiService } from 'src/app/service/hnc-ebi.service';
// import { TempServiceService } from 'src/app/service/temp-service.service';

@Injectable({
  providedIn: 'root'
})
export class PremiumCalculateService {

  premiumHeartCancerEbi;
  premium;
  premiumAmount;
  sumAssured;
  totalSumAssuredWithSpouse;
  
  public hospitalBenefitAmount;
  public increasingBenefitAmount;
  public incomeBenefitAmount;

  public newPremiumAmount;
  public discountAmount;
  public oldPremiumAmount;
  constructor(private CallEbi: HncEbiService,public tempservice: TempServiceService) { }

  calculatePremium(): void {
    if (this.tempservice.HeartCancer_InputArr['CoverageOption'] === 'Cancer') {
      this.tempservice.HeartCancer_InputArr['LA1_HeartSumAssured'] = '0';
      this.tempservice.HeartCancer_InputArr['LA2_HeartSumAssured'] = '0';
    }
    else if (this.tempservice.HeartCancer_InputArr['CoverageOption'] === 'Heart') {
      this.tempservice.HeartCancer_InputArr['LA1_CancerSumAssured'] = '0';
      this.tempservice.HeartCancer_InputArr['LA2_CancerSumAssured'] = '0';
    }

    var temp_freq = this.tempservice.HeartCancer_InputArr['Frequency'];
    // For yearly monthly premium
    //For nypremium
    var temp_dob = this.tempservice.HeartCancer_InputArr["LA1_DOB"];
    var temp_dob_split = temp_dob.split("/");
    var plusyr = parseInt(temp_dob_split[2]) + 1;
    var temp_plusone_dob = temp_dob_split[0] + "/" + temp_dob_split[1] + "/" + plusyr;
    if (this.CallEbi.calculateAge(temp_plusone_dob) >= 18) {
      this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_plusone_dob;
    }
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.tempservice.HeartCancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.tempservice.HeartCancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.nypremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_dob;

    
    //For Monthlly
    this.tempservice.HeartCancer_InputArr['Frequency'] = "Monthly";
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.tempservice.HeartCancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.tempservice.HeartCancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.MonthlyPremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]

    //For Yearly
    this.tempservice.HeartCancer_InputArr['Frequency'] = "Yearly";
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.tempservice.HeartCancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.tempservice.HeartCancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.YearlyPremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    // For yearly monthly premium

    this.tempservice.HeartCancer_InputArr['Frequency'] = temp_freq;

    // For Benefits
    let temp_hospitslbf = this.tempservice.HeartCancer_InputArr['HospitalBenefit'];
    let temp_incombf = this.tempservice.HeartCancer_InputArr['IncomeBenefit'];
    let temp_increasingbf = this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'];

    this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = "Yes";
    this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = "Yes";
    this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = "Yes";

    this.tempservice.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.tempservice.HeartCancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.tempservice.HeartCancer_InputArr,
      this.premiumHeartCancerEbi
    );

    this.hospitalBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
      this.premiumHeartCancerEbi.HospitalBenefit_ShowPremium +
      this.premiumHeartCancerEbi.Cancer_HospitalBenefit_ShowPremium +
      this.premiumHeartCancerEbi.LA2_HospitalBenefit_ShowPremium +
      this.premiumHeartCancerEbi.LA2_Cancer_HospitalBenefit_ShowPremium +
      this.premiumHeartCancerEbi.XRT_HospitalBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_HospitalBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_Cancer_HospitalBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_Cancer_HospitalBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_LA2_HospitalBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_LA2_HospitalBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_LA2_Cancer_HospitalBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_LA2_Cancer_HospitalBenefit_SecondPremium
    )
    );
    this.increasingBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
      this.premiumHeartCancerEbi.IncreasingCoverBenefit_ShowPremium +
      this.premiumHeartCancerEbi.Cancer_IncreasingCoverBenefit_ShowPremium +
      this.premiumHeartCancerEbi.LA2_IncreasingCoverBenefit_ShowPremium +
      this.premiumHeartCancerEbi.LA2_Cancer_IncreasingCoverBenefit_ShowPremium +
      this.premiumHeartCancerEbi.XRT_IncreasingCoverBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_IncreasingCoverBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_Cancer_IncreasingCoverBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_Cancer_IncreasingCoverBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_LA2_IncreasingCoverBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_LA2_IncreasingCoverBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncreasingCoverBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncreasingCoverBenefit_SecondPremium
    )
    );
    this.incomeBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
      this.premiumHeartCancerEbi.IncomeBenefit_ShowPremium +
      this.premiumHeartCancerEbi.Cancer_IncomeBenefit_ShowPremium +
      this.premiumHeartCancerEbi.LA2_IncomeBenefit_ShowPremium +
      this.premiumHeartCancerEbi.LA2_Cancer_IncomeBenefit_ShowPremium +
      this.premiumHeartCancerEbi.XRT_IncomeBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_IncomeBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_Cancer_IncomeBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_Cancer_IncomeBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_LA2_IncomeBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_LA2_IncomeBenefit_SecondPremium +
      this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncomeBenefit_FirstPremium +
      this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncomeBenefit_SecondPremium
    )
    );
    this.tempservice.HospitalCalculated = this.hospitalBenefitAmount;
    this.tempservice.IncreasingCalculated = this.increasingBenefitAmount;
    this.tempservice.IncomeBenefitCalculated = this.incomeBenefitAmount; 
    
    this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = temp_hospitslbf;
    this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = temp_incombf;
    this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = temp_increasingbf;
    //For benefits
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.tempservice.HeartCancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.tempservice.HeartCancer_InputArr,
      this.premiumHeartCancerEbi
    );
    this.premiumAmount = this.tempservice.addCommas(
      this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    );

    this.sumAssured = this.tempservice.valueRoundup(
      parseInt(this.tempservice.HeartCancer_InputArr.LA1_HeartSumAssured) +
      parseInt(this.tempservice.HeartCancer_InputArr.LA1_CancerSumAssured)
    );

    this.totalSumAssuredWithSpouse = this.tempservice.valueRoundup(
      parseInt(this.tempservice.HeartCancer_InputArr.LA1_HeartSumAssured) +
      parseInt(this.tempservice.HeartCancer_InputArr.LA1_CancerSumAssured) +
      parseInt(this.tempservice.HeartCancer_InputArr.LA2_HeartSumAssured) +
      parseInt(this.tempservice.HeartCancer_InputArr.LA2_CancerSumAssured)
    );

    this.newPremiumAmount = Math.round(this.premium["LA1_first_premium"]);
    this.oldPremiumAmount = Math.round(this.newPremiumAmount / 0.95);
    this.discountAmount = Math.round(this.oldPremiumAmount - this.newPremiumAmount);
  }


  onChangeDropdown(evt, fieldID) {
    let fieldValue = evt.target.value;
    this.tempservice.userInput["Frequency"] = fieldValue;
    if ("policyterm" == fieldID) {
      this.tempservice.HeartCancer_InputArr["PolicyTerm"] = fieldValue;
      this.tempservice.subject.next(this.tempservice.HeartCancer_InputArr);
      this.tempservice.userInputSubject.next(this.tempservice.userInput);
    } else if ("Frequency" == fieldID) {
      this.tempservice.HeartCancer_InputArr["Frequency"] = fieldValue;
      this.tempservice.subject.next(this.tempservice.HeartCancer_InputArr);
      this.tempservice.userInputSubject.next(this.tempservice.userInput);
    } else if ("annualPackage" == fieldID) {
      this.tempservice.userInput["annualPackage"] = fieldValue;
    }
    // if ("policyterm" == fieldID) {
    //   this.tempservice.OnchangeDefaultParam('policy term')
    // } else if ("Frequency" == fieldID) {
    //   this.tempservice.OnchangeDefaultParam('premium frequency')
    // }
    this.calculatePremium();
    this.tempservice.createHncCookie();
  }

    calculateNextPremium (){
  if (this.tempservice.HeartCancer_InputArr['CoverageOption'] === 'Cancer') {
    this.tempservice.HeartCancer_InputArr['LA1_HeartSumAssured'] = '0';
    this.tempservice.HeartCancer_InputArr['LA2_HeartSumAssured'] = '0';
  }
  else if (this.tempservice.HeartCancer_InputArr['CoverageOption'] === 'Heart') {
    this.tempservice.HeartCancer_InputArr['LA1_CancerSumAssured'] = '0';
    this.tempservice.HeartCancer_InputArr['LA2_CancerSumAssured'] = '0';
  }

  var temp_freq = this.tempservice.HeartCancer_InputArr['Frequency'];
  // For yearly monthly premium
  //For nypremium
  var temp_dob = this.tempservice.HeartCancer_InputArr["LA1_DOB"];
  var temp_dob_split = temp_dob.split("/");
  var plusyr = parseInt(temp_dob_split[2]) - 1;
  var temp_plusone_dob = temp_dob_split[0] + "/" + temp_dob_split[1] + "/" + plusyr;
  if (this.CallEbi.calculateAge(temp_plusone_dob) >= 18) {
    this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_plusone_dob;
  }
  this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
    this.tempservice.HeartCancer_InputArr
  );
  this.tempservice.nextPremium = this.CallEbi.calculateEBI(
    this.tempservice.HeartCancer_InputArr,
    this.premiumHeartCancerEbi
  );
  // this.tempservice.dataLayerArray.nypremium = this.tempservice.nextPremium["LA1_first_premium"] + this.tempservice.nextPremium["LA2_first_premium"]
  this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_plusone_dob;
  // this.HeartCancer_InputArr['LA1_DOB'] = temp_dob;

  //For Monthlly
  this.tempservice.HeartCancer_InputArr['Frequency'] = "Monthly";
  this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
    this.tempservice.HeartCancer_InputArr
  );
  this.tempservice.nextPremium = this.CallEbi.calculateEBI(
    this.tempservice.HeartCancer_InputArr,
    this.premiumHeartCancerEbi
  );
  // this.tempservice.dataLayerArray.MonthlyPremium = this.tempservice.nextPremium["LA1_first_premium"] + this.tempservice.nextPremium["LA2_first_premium"]

  //For Yearly
  this.tempservice.HeartCancer_InputArr['Frequency'] = "Yearly";
  this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
    this.tempservice.HeartCancer_InputArr
  );
  this.tempservice.nextPremium = this.CallEbi.calculateEBI(
    this.tempservice.HeartCancer_InputArr,
    this.premiumHeartCancerEbi
  );
  // this.tempservice.dataLayerArray.YearlyPremium = this.tempservice.nextPremium["LA1_first_premium"] + this.tempservice.nextPremium["LA2_first_premium"]
  // For yearly monthly nextPremium

  this.tempservice.HeartCancer_InputArr['Frequency'] = temp_freq;

  // For Benefits
  let temp_hospitslbf = this.tempservice.HeartCancer_InputArr['HospitalBenefit'];
  let temp_incombf = this.tempservice.HeartCancer_InputArr['IncomeBenefit'];
  let temp_increasingbf = this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'];

  this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = "Yes";
  this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = "Yes";
  this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = "Yes";

  this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
    this.tempservice.HeartCancer_InputArr
  );
  this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_plusone_dob;
  this.tempservice.nextPremium = this.CallEbi.calculateEBI(
    this.tempservice.HeartCancer_InputArr,
    this.premiumHeartCancerEbi
  );

  this.hospitalBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
    this.premiumHeartCancerEbi.HospitalBenefit_ShowPremium +
    this.premiumHeartCancerEbi.Cancer_HospitalBenefit_ShowPremium +
    this.premiumHeartCancerEbi.LA2_HospitalBenefit_ShowPremium +
    this.premiumHeartCancerEbi.LA2_Cancer_HospitalBenefit_ShowPremium +
    this.premiumHeartCancerEbi.XRT_HospitalBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_HospitalBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_Cancer_HospitalBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_Cancer_HospitalBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_LA2_HospitalBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_LA2_HospitalBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_LA2_Cancer_HospitalBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_LA2_Cancer_HospitalBenefit_SecondPremium
  )
  );
  this.increasingBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
    this.premiumHeartCancerEbi.IncreasingCoverBenefit_ShowPremium +
    this.premiumHeartCancerEbi.Cancer_IncreasingCoverBenefit_ShowPremium +
    this.premiumHeartCancerEbi.LA2_IncreasingCoverBenefit_ShowPremium +
    this.premiumHeartCancerEbi.LA2_Cancer_IncreasingCoverBenefit_ShowPremium +
    this.premiumHeartCancerEbi.XRT_IncreasingCoverBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_IncreasingCoverBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_Cancer_IncreasingCoverBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_Cancer_IncreasingCoverBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_LA2_IncreasingCoverBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_LA2_IncreasingCoverBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncreasingCoverBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncreasingCoverBenefit_SecondPremium
  )
  );
  this.incomeBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
    this.premiumHeartCancerEbi.IncomeBenefit_ShowPremium +
    this.premiumHeartCancerEbi.Cancer_IncomeBenefit_ShowPremium +
    this.premiumHeartCancerEbi.LA2_IncomeBenefit_ShowPremium +
    this.premiumHeartCancerEbi.LA2_Cancer_IncomeBenefit_ShowPremium +
    this.premiumHeartCancerEbi.XRT_IncomeBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_IncomeBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_Cancer_IncomeBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_Cancer_IncomeBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_LA2_IncomeBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_LA2_IncomeBenefit_SecondPremium +
    this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncomeBenefit_FirstPremium +
    this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncomeBenefit_SecondPremium
  )
  );

  this.tempservice.HeartCancer_InputArr['HospitalBenefit'] = temp_hospitslbf;
  this.tempservice.HeartCancer_InputArr['IncomeBenefit'] = temp_incombf;
  this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] = temp_increasingbf;
  //For benefits
  this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_plusone_dob;
  this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
    this.tempservice.HeartCancer_InputArr
  );
  this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_plusone_dob;
  this.tempservice.nextPremium = this.CallEbi.calculateEBI(
    this.tempservice.HeartCancer_InputArr,
    this.premiumHeartCancerEbi
  );
  this.tempservice.nextPremiumAmount = this.tempservice.addCommas(
    this.tempservice.nextPremium["LA1_first_premium"] + this.tempservice.nextPremium["LA2_first_premium"]
  );

  this.sumAssured = this.tempservice.valueRoundup(
    parseInt(this.tempservice.HeartCancer_InputArr.LA1_HeartSumAssured) +
    parseInt(this.tempservice.HeartCancer_InputArr.LA1_CancerSumAssured)
  );

  this.tempservice.sumAssuredPL = parseInt(this.tempservice.HeartCancer_InputArr.LA1_HeartSumAssured) + parseInt(this.tempservice.HeartCancer_InputArr.LA1_CancerSumAssured)
  
  this.totalSumAssuredWithSpouse = this.tempservice.valueRoundup(
    parseInt(this.tempservice.HeartCancer_InputArr.LA1_HeartSumAssured) +
    parseInt(this.tempservice.HeartCancer_InputArr.LA1_CancerSumAssured) +
    parseInt(this.tempservice.HeartCancer_InputArr.LA2_HeartSumAssured) +
    parseInt(this.tempservice.HeartCancer_InputArr.LA2_CancerSumAssured)
  );

  this.tempservice.sumAssuredJL = parseInt(this.tempservice.HeartCancer_InputArr.LA2_HeartSumAssured) +  parseInt(this.tempservice.HeartCancer_InputArr.LA2_CancerSumAssured)
  this.tempservice.HeartCancer_InputArr['LA1_DOB'] = temp_dob;
  // this.newPremiumAmount = Math.round(this.nextPremium["LA1_first_premium"]);
  // this.oldPremiumAmount = Math.round(this.newPremiumAmount / 0.95);
  // this.discountAmount = Math.round(this.oldPremiumAmount - this.newPremiumAmount);
  }
}
