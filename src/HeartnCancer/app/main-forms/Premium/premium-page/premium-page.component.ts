import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, transition, animate, style } from '@angular/animations';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

// import { StepsComponent } from 'src/app/shared/steps/steps.component';
// import { PopupComponent } from 'src/app/shared/popup/popup.component';
// import { EditpanelComponent } from 'src/app/shared/edit-panel/editpanel.component';

// import { WindowRefService } from 'src/app/service/window-ref.service';
// import { TempServiceService } from 'src/app/service/temp-service.service';
// import { GlobalServiceService } from 'src/app/service/global-service.service';
import { PremiumCalculateService } from '../premium-calculate.service';
// import { PopupComponent } from 'src/HeartnCancer/app/shared/popup/popup.component';
import { EditpanelComponent } from 'src/HeartnCancer/app/shared/edit-panel/editpanel.component';
import { StepsComponent } from 'src/HeartnCancer/app/shared/steps/steps.component';
import { GlobalServiceService } from 'src/HeartnCancer/app/service/global-service.service';
import { TempServiceService } from 'src/HeartnCancer/app/service/temp-service.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';
import { WindowRefService } from 'src/Common/services/window-ref.service';
// import { WindowRefService } from 'src/HeartnCancer/app/service/window-ref.service';
// import { PopupComponent } from 'HeartnCancer/src/app/shared/popup/popup.component';
// import { EditpanelComponent } from 'HeartnCancer/src/app/shared/edit-panel/editpanel.component';
// import { StepsComponent } from 'HeartnCancer/src/app/shared/steps/steps.component';
// import { GlobalServiceService } from 'HeartnCancer/src/app/service/global-service.service';
// import { WindowRefService } from 'HeartnCancer/src/app/service/window-ref.service';
// import { TempServiceService } from 'HeartnCancer/src/app/service/temp-service.service';

declare const require: any;
var sha1 = require('sha1');

@Component({
  selector: 'app-premium-page',
  templateUrl: './premium-page.component.html',
  styleUrls: ['./premium-page.component.css'],
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translate3d(100%, 0, 0)' }),
        animate(400)
      ]),
      transition(':leave', [
        style({ transform: 'translate3d(-100%, 0, 0)' }),
        animate(400)
      ])
    ])
  ]
})

export class PremiumPageComponent implements OnInit {

  otpRegenerated = false;
  otpGenerated = false;
  otpVerificationFailed = false;
  // employeeVerificationFailed = false;
  // employeeVerificationSuccess = false;
  clientId;
  otpAttemptFailed = 0;
  otpApiCallFailed = false;

  showno = false;
  showyes = false;
  dobErrorMsg: any = "";
  mobileErrorMsg: any = "";
  emailErrorMsg: any = "";
  spouseTobaccoErrorMsg: any = "";
  Male: any = '';
  Female: any = '';
  showContent: string = '';
  spouseage: any = '';
  showmale: boolean = false;
  showFemale: boolean = false;
  hideRemoveHeartOption: boolean = true;
  hideRemoveCancerOption: boolean = true;
  disableDIV: boolean = true;
  disableCancerDiv: boolean = true;
  firstPerson;
  secondPerson;
  totalCover;
  oldPremium;
  showBreakupFrequency;
  totalpremiumBreakup;
  discount;
  totalcoverBreakup;
  animationState = "enter";
  otpExistingcust = false;
  otpExistingcustval;
  otpErrorMsg;
  encodedmobile;
  
  @ViewChild('spousecoverdiscount') spousecoverdiscount: PopupComponent;
  @ViewChild('existingcustomer') existingcustomer: PopupComponent;
  @ViewChild('spousedetails') spousedetails: PopupComponent;
  @ViewChild('existingcustdetails') existingcustdetails: PopupComponent;
  @ViewChild('existingCustomerSelected') existingCustomerSelected: PopupComponent;
  @ViewChild('spouseSelected') spouseSelected: PopupComponent;
  @ViewChild('otp') otp: PopupComponent;
  @ViewChild('congratulations') congratulations: PopupComponent;
  @ViewChild('removecancer') removecancer: PopupComponent;
  @ViewChild('removeheart') removeheart: PopupComponent;
  @ViewChild('showeditMenu') showeditMenu: EditpanelComponent;
  @ViewChild('showBullet') showBullet: StepsComponent;
  @ViewChild('mobileNo') mobileNo: ElementRef;
  @ViewChild('email') email: ElementRef;

  // @ViewChild('groupemployee') groupemployee: PopupComponent;

  // @ViewChild('ntid') ntid: ElementRef;  

  // @ViewChild('password') password: ElementRef;

  constructor(private http:HttpClient,private router: Router,public premiumCalc: PremiumCalculateService, public globalServices: GlobalServiceService, public tempservice: TempServiceService, public WindowRef: WindowRefService) {
    this.tempservice.premiumToLanding = true;
    this.tempservice.flagData.subscribe(() => {
      this.updateValues();
    });
     this.router.events.subscribe(() => {
        document.body.className = "bodySticky";
        let isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent)
        if (isIEOrEdge == true) { 
           setTimeout(() => {
            document.getElementById("premium-details").scrollIntoView();
           },5)
          
        } else {
          document.getElementById("premium-details").scrollIntoView(true);
        }
      });
  }
  
  ngOnInit() {
    this.updateValues();  
     var splitdob = this.tempservice.userInput["dob"].split("/");
     var newdate = splitdob[1] + "/" + splitdob[0];
     try {
       this.tempservice.WindowRef.nativeWindow.dataLayer.push({
         'event': "Desktop_HnCCalculator_QuoteSeen",
         'Data_Prefilled': "No",
         'ProductCode': "T48",
         'ProductName': "HeartCancer",
         'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
         'pagetype': "top-health : heart-cancer-protect-calculator",
         'subsection1': "health-insurance-plans",
         'subsection2': "heart-cancer-protect-calculator",
         'subsection3': "health-insurance-plans-desktop",
         'channel': "protection",
         'Subchannel': "health",
         'PageCategory': "Calculator",
         'PageSubCategory': "Calculator-Quote",
         'SectionName': "Quote",
         'UserType': "Guest",
         'UserRole': "BOL",
         'UserVisit': "New",
         'Age': this.tempservice.userInput["age"],
         'DOB': newdate,
         'Gender': this.tempservice.userInput["gender"],
         'Tobacco': this.tempservice.userInput["tobacco"],
         'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
         'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
         'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
         'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
         'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
         'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
         'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
         'nypremium': this.tempservice.dataLayerArray.nypremium,
         'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
         'PaymentTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
         'UID': this.tempservice.HeartCancer_InputArr.UID
       });
     } catch (e) { }
  }

  updateValues() {
    this.showBullet.showBullets('PD');
    this.Male = this.tempservice.flag['Male'];
    this.Female = this.tempservice.flag['Female'];
    if (this.tempservice.flag['Male']) {
      this.showContent = 'Wife';
    } else {
      this.showContent = 'Husband';
    }
    if (this.tempservice.flag['husbandWife']) {
      this.tempservice.flag['showHide']['notExistingCus'] = true;
      this.tempservice.flag['showHide']['ExistingCus'] = false;
      if (this.tempservice.userInput['storedMale'] == true) {
        this.Male = true;
        this.Female = false;
        this.showContent = 'Wife';
      } else {
        this.Male = false;
        this.Female = true;
        this.showContent = 'Husband';
      }
    }
  }

  showEditMenu() {
    this.showeditMenu.show();
  }
  yesNoEvent() {
     var splitdob = this.tempservice.userInput["dob"].split("/");
     var newdate = splitdob[1] + "/" + splitdob[0];
     try {
       this.tempservice.WindowRef.nativeWindow.dataLayer.push({
         'event': "Desktop_HnCCalculator_AddSpouse_PopupOk",
         'Data_Prefilled': "No",
         'ProductCode': "T48",
         'ProductName': "HeartCancer",
         'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
         'pagetype': "top-health : heart-cancer-protect-calculator",
         'subsection1': "health-insurance-plans",
         'subsection2': "heart-cancer-protect-calculator",
         'subsection3': "health-insurance-plans-desktop",
         'channel': "protection",
         'Subchannel': "health",
         'PageCategory': "Calculator",
         'PageSubCategory': "Calculator-Quote",
         'SectionName': "Quote - Spouse Cover Discount",
         'UserType': "Guest",
         'UserRole': "BOL",
         'UserVisit': "New",
         'Age': this.tempservice.userInput["age"],
         'DOB': newdate,
         'Gender': this.tempservice.userInput["gender"],
         'Tobacco': this.tempservice.userInput["tobacco"],
         'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
         'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
         'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
         'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
         'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
         'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
         'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
         'nypremium': this.tempservice.dataLayerArray.nypremium,
         'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
         'PaymentTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
         'UID': this.tempservice.HeartCancer_InputArr.UID
       });
     } catch (e) { }
  }
  updateSumAssuredVal(sumAssured) {
    this.tempservice.sumAssured = sumAssured;
  }
  getcover() {
    this.tempservice.flag['Inputfields']['Select_Plan_Flag'] = "Yes";
     var splitdob = this.tempservice.userInput["dob"].split("/");
     var newdate = splitdob[1] + "/" + splitdob[0];
     try {
       this.tempservice.WindowRef.nativeWindow.dataLayer.push({
         'event': "Desktop_HnCCalculator_GetLifeCover",
         'Data_Prefilled': "No",
         'ProductCode': "T48",
         'ProductName': "HeartCancer",
         'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
         'pagetype': "top-health : heart-cancer-protect-calculator",
         'subsection1': "health-insurance-plans",
         'subsection2': "heart-cancer-protect-calculator",
         'subsection3': "health-insurance-plans-desktop",
         'channel': "protection",
         'Subchannel': "health",
         'PageCategory': "Calculator",
         'PageSubCategory': "Calculator-Quote",
         'SectionName': "Quote",
         'UserType': "Guest",
         'UserRole': "BOL",
         'UserVisit': "New",
         'Age': this.tempservice.userInput["age"],
         'DOB': newdate,
         'Gender': this.tempservice.userInput["gender"],
         'Tobacco': this.tempservice.userInput["tobacco"],
         'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
         'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
         'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
         'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
         'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
         'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
         'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
         'nypremium': this.tempservice.dataLayerArray.nypremium,
         'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
         'PaymentTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
         'SpouseAdded': this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
         'UID': this.tempservice.HeartCancer_InputArr.UID
       });
     } catch (e) { }
     try{
       this.tempservice.datalayer={
         'Section':"HnC Quote",
         'ProductCode':"T48",
         'Gender': this.tempservice.userInput["gender"],
         'Age': this.tempservice.userInput["age"],
         'Tobacco': this.tempservice.userInput["tobacco"],
         'TotalCover': this.tempservice.userInput["stringTotalCover"],
         'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
         'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
         'PrimaryCover': 		parseInt(this.tempservice.HeartCancer_InputArr['LA1_HeartSumAssured'])+parseInt(this.tempservice.HeartCancer_InputArr['LA1_CancerSumAssured']), 
         'SpouseCover': parseInt(this.tempservice.HeartCancer_InputArr['LA2_HeartSumAssured'])+parseInt(this.tempservice.HeartCancer_InputArr['LA2_CancerSumAssured']),
         'SpouseAge': this.tempservice.userInput["Partnerage"],
         'SpouseTobacco': this.tempservice.HeartCancer_InputArr["LA2_Tobacco"]
       }
       this.WindowRef.nativeWindow._satellite.track("HnC_Dec");
     }
     catch(e) {}
    this.globalServices.flag.editScreen.sec3 = true;
    this.globalServices.flag.editScreen.personalInfo.mainDropDown = false;
    this.globalServices.flag.editScreen.sec2active = false;
    this.router.navigate(['/personal']);
  }

  // spouse pop-up
  showDialogSpouse() {
     var splitdob = this.tempservice.userInput["dob"].split("/");
     var newdate = splitdob[1] + "/" + splitdob[0];
     this.tempservice.WindowRef.nativeWindow.dataLayer.push({
       'event': "Desktop_HnCCalculator_AddSpouse",
       'Data_Prefilled': "No",
       'ProductCode': "T48",
       'ProductName': "HeartCancer",
       'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
       'pagetype': "top-health : heart-cancer-protect-calculator",
       'subsection1': "health-insurance-plans",
       'subsection2': "heart-cancer-protect-calculator",
       'subsection3': "health-insurance-plans-desktop",
       'channel': "protection",
       'Subchannel': "health",
       'PageCategory': "Calculator",
       'PageSubCategory': "Calculator-Quote",
       'SectionName': "Quote - Spouse Cover Discount",
       'UserType': "Guest",
       'UserRole': "BOL",
       'UserVisit': "New",
       'Age': this.tempservice.userInput["age"],
       'DOB': newdate,
       'Gender': this.tempservice.userInput["gender"],
       'Tobacco': this.tempservice.userInput["tobacco"],
       'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
       'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
       'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
       'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
       'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
       'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
       'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
       'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
       'nypremium': this.tempservice.dataLayerArray.nypremium,
       'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
       'PaymentTerm': this.tempservice.HeartCancer_InputArr["PolicyTerm"],
       'Device': "Desktop",
       'UID': this.tempservice.HeartCancer_InputArr.UID
     });
     try{
       this.tempservice.datalayer['Behavior'] = "Add Spouse";
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
    if (!this.tempservice.flag['existingCustomeradded']) {
      this.spousecoverdiscount.showDialog();
    }
    else if (this.tempservice.flag['existingCustomeradded'] == true) {
      this.existingCustomerSelected.showDialog();
    }
  }
  closeDialogSpouse() {
    this.spousecoverdiscount.closeDialog();
     try{
       this.tempservice.datalayer['Behavior'] = "Add Spouse - No"
    
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }
  openSpouseDetails() {
    // to close background popup
    this.tempservice.flag['showHide']['ExistingCus'] = false;
    this.tempservice.flag['showHide']['notExistingCus'] = true;
    this.tempservice.flag['existingCustomeradded'] = false;
    this.spousecoverdiscount.closeDialog();
    this.spousedetails.showDialog();
     try{
       this.tempservice.datalayer['Behavior'] = "Add Spouse - Yes"

       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }

  // spouse details pop-up
  backToSpouse() {
    this.spousedetails.closeDialog();
    this.spousecoverdiscount.showDialog();
  }
  showmeUpdatedQuote() {
    if (this.tempservice.flag['checkValidate']['spousedob'] == true && this.tempservice.flag['checkValidate']['spouseTobacco']) {
      this.spousedetails.closeDialog();
      this.tempservice.flag['showHide']['showBreakUp'] = true;
      this.tempservice.flag['showHide']['partnerCoverRemoveAdd'] = true;
      if (this.tempservice.flag['Male'] == true) {
        this.tempservice.HeartCancer_InputArr['LA2_Gender'] = 'Female';
      } else if (this.tempservice.flag['Female'] == true) {
        this.tempservice.HeartCancer_InputArr['LA2_Gender'] = 'Male';
      }
      this.tempservice.userInput['storedMale'] = this.tempservice.flag['Male'];
      this.tempservice.userInput['storedFemale'] = this.tempservice.flag['Female'];
      this.tempservice.updateFlagData('husbandWife', true);
      this.globalServices.flag.editScreen.personalInfo.mainDropDown = false;
      this.globalServices.flag.editScreen.sec2active = true;
      this.globalServices.flag.editScreen.sec2 = true;
      this.tempservice.HeartCancer_InputArr['FamilyBenefit'] = 'Yes';
      this.globalServices.flag.editScreen.personalInfo.removeWifeCover = !this.globalServices.flag.editScreen.personalInfo.removeWifeCover;
      this.premiumCalc.calculatePremium();
      this.tempservice.createHncCookie();
       var splitdob = this.tempservice.userInput["dob"].split("/");
       var newdate = splitdob[1] + "/" + splitdob[0];
       try {
         this.tempservice.WindowRef.nativeWindow.dataLayer.push({
           'event': "Desktop_HnCCalculator_SpouseAdded",
           'Data_Prefilled': "No",
           'ProductCode': "T48",
           'ProductName': "HeartCancer",
           'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
           'pagetype': "top-health : heart-cancer-protect-calculator",
           'subsection1': "health-insurance-plans",
           'subsection2': "heart-cancer-protect-calculator",
           'subsection3': "health-insurance-plans-desktop",
           'channel': "protection",
           'Subchannel': "health",
           'PageCategory': "Calculator",
           'PageSubCategory': "Calculator-Quote",
           'SectionName': "Quote - Spouse Cover Discount",
           'UserType': "Guest",
           'UserRole': "BOL",
           'UserVisit': "New",
           'Age': this.tempservice.userInput["age"],
           'DOB': newdate,
           'Gender': this.tempservice.userInput["gender"],
           'Tobacco': this.tempservice.userInput["tobacco"],
           'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
           'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
           'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
           'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
           'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
           'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
           'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
           'nypremium': this.tempservice.dataLayerArray.nypremium,
           'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
           'PaymentTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
           'SpouseAdded': this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
           'UID': this.tempservice.HeartCancer_InputArr.UID
         });
       } catch (e) { }
       try{
         this.tempservice.datalayer = {
           'Section':"HnC Spouse",
           'ProductCode':"T48",
           'Gender': this.tempservice.userInput["gender"],
           'Age': this.tempservice.userInput["age"],
           'Tobacco': this.tempservice.userInput["tobacco"],
           'TotalCover': this.tempservice.userInput["stringTotalCover"],
           'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
           'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
           'PrimaryCover': this.tempservice.userInput["stringPrimaryCover"], 
           'SpouseCover': this.tempservice.userInput["stringSpouseCover"],
           'SpouseAge': this.tempservice.userInput["Partnerage"],
           'SpouseTobacco': this.tempservice.HeartCancer_InputArr["LA2_Tobacco"]
         }
         this.WindowRef.nativeWindow._satellite.track("HnC_Dec");
       }catch(e) {
       }
    } else {
      if (this.tempservice.flag['checkValidate']['spousedob'] == false) {
        this.dobErrorMsg = "Please Enter Valid Date of Birth";
      }
      if (this.tempservice.flag['checkValidate']['spouseTobacco'] == false) {
        // this.spouseTobaccoErrorMsg = "Please Select Tobacco option";
        this.tempservice.flag['Inputfields']['spouseTobaccoErrorMsg'] = "Please Select Tobacco option";
      }
    }
  }

  removeSpouse() {
    this.tempservice.flag['showHide']['showBreakUp'] = false;
    this.tempservice.flag['showHide']['partnerCoverRemoveAdd'] = false;
    this.tempservice.flag['spouseTobaccoYes'] = false;
    this.tempservice.flag['spouseTobaccoNo'] = false;
    this.tempservice.flag['checkValidate']['spouseTobacco'] = false;
    this.tempservice.updateFlagData('showHide', this.tempservice.flag['showHide']);
    this.tempservice.updateFlagData('Male', this.Male);
    this.tempservice.updateFlagData('Female', this.Female);
    this.tempservice.updateFlagData('husbandWife', false);
    this.tempservice.updateFlagData('spouseTobaccoYes', false);
    this.tempservice.updateFlagData('spouseTobaccoNo', false);

    this.tempservice.HeartCancer_InputArr['FamilyBenefit'] = 'No';
    this.globalServices.flag.editScreen.personalInfo.removeWifeCover = false;
    this.globalServices.flag.editScreen.sec2 = false;
    this.globalServices.flag.editScreen.personalInfo.removeWifeCover = true;
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }
  closeSpouseDetails() {
    this.spousedetails.closeDialog();
  }

  // existing customer pop-up
  showDialogExistingCustomer() {
    if (!this.tempservice.flag['showHide']['partnerCoverRemoveAdd']) {
      this.existingcustomer.showDialog();
    } else {
      this.spouseSelected.showDialog();
    }
     var splitdob = this.tempservice.userInput["dob"].split("/");
     var newdate = splitdob[1] + "/" + splitdob[0];
     try {
       this.tempservice.WindowRef.nativeWindow.dataLayer.push({
         'event': "Desktop_HnCCalculator_ExistingCustomer",
         'Data_Prefilled': "No",
         'ProductCode': "T48",
         'ProductName': "HeartCancer",
         'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
         'pagetype': "top-health : heart-cancer-protect-calculator",
         'subsection1': "health-insurance-plans",
         'subsection2': "heart-cancer-protect-calculator",
         'subsection3': "health-insurance-plans-desktop",
         'channel': "protection",
         'Subchannel': "health",
         'PageCategory': "Calculator",
         'PageSubCategory': "Calculator-Quote",
         'SectionName': "Quote",
         'UserType': "Guest",
         'UserRole': "BOL",
         'UserVisit': "New",
         'Age': this.tempservice.userInput["age"],
         'DOB': newdate,
         'Gender': this.tempservice.userInput["gender"],
         'Tobacco': this.tempservice.userInput["tobacco"],
         'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
         'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
         'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
         'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
         'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
         'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
         'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
         'nypremium': this.tempservice.dataLayerArray.nypremium,
         'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
         'PaymentTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
         'SpouseAdded': this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
         'UID': this.tempservice.HeartCancer_InputArr.UID
       });
     } catch (e) { }
     try{
       this.tempservice.datalayer['Behavior'] = "Click here if you are an existing ICICI Pru Customer";
    
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}    
  }

  closeDialogExistingCustomer() {
    this.existingcustomer.closeDialog();
  }

  openExistingCustDetails() {
    this.existingcustomer.closeDialog();
    this.existingcustdetails.showDialog();
     try{
       this.tempservice.datalayer['Behavior'] = "Existing Customer -Yes";
    
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}
  }

  // existing customer details pop-up  //
  gotoOTP() {
    console.log("getotp");
    if (this.tempservice.flag['checkValidate']['existingMobile']) {
       var splitdob = this.tempservice.userInput["dob"].split("/");
       var newdate = splitdob[1] + "/" + splitdob[0];
      //  var isexistingCust = "";
      //  if (this.tempservice.flag['existingCustomeradded'] == true) {
      //    isexistingCust = "Yes";
      //  } else {
      //    isexistingCust = "No";
      //  }
      //  var MoNumber = this.mobileNo.nativeElement.value;
       try {
         this.tempservice.WindowRef.nativeWindow.dataLayer.push({
           'event': "Desktop_HnCCalculator_ExistingCustomerConfirm",
           'Data_Prefilled': "No",
           'ProductCode': "T48",
           'ProductName': "HeartCancer",
           'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
           'pagetype': "top-health : heart-cancer-protect-calculator",
           'subsection1': "health-insurance-plans",
           'subsection2': "heart-cancer-protect-calculator",
           'subsection3': "health-insurance-plans-desktop",
           'channel': "protection",
           'Subchannel': "health",
           'PageCategory': "Calculator",
           'PageSubCategory': "Calculator-Quote",
           'SectionName': "Quote - Existing Customer",
           'UserType': "Guest",
           'UserRole': "BOL",
           'UserVisit': "New",
           'Age': this.tempservice.userInput["age"],
           'DOB': newdate,
           'Gender': this.tempservice.userInput["gender"],
           'Tobacco': this.tempservice.userInput["tobacco"],
           'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
           'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
           'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
           'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
           'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
           'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
           'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
           'nypremium': this.tempservice.dataLayerArray.nypremium,
           'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
           'PaymentTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
           'SpouseAdded': this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
           'NetworkUID1': sha1(this.mobileNo.nativeElement.value),
           'ExistingCustomer': "Yes",
           'UID': this.tempservice.HeartCancer_InputArr.UID
         });
       } catch (e) { }
    }
    if (this.tempservice.flag['checkValidate']['existingMobile']) {
      this.generateOTP();
       if(this.generateOTP()){
         this.existingcustdetails.closeDialog();  
         this.otp.showDialog();
       }

    } else {
      if (!this.tempservice.flag['checkValidate']['existingMobile']) {
        this.mobileErrorMsg = "Please Enter Mobile Number";
      }
       if(!this.tempservice.flag['checkValidate']['existingEmail']){
         this.emailErrorMsg = "Please Enter your Email";
       }
    }

  }
  closePopUp() {
    this.existingcustdetails.closeDialog();
  }

  // otp

  regenerateotp() {
    this.otpRegenerated = true;
  }

  gotoCongratulations() {
    if (this.otpExistingcust) {
      this.VerifyOTP();
       try{
         this.tempservice.datalayer = {
           'Section':"HnC Existing Customer verified",
           'ProductCode':"T48",
           'Gender': this.tempservice.userInput["gender"],
           'Age': this.tempservice.userInput["age"],
           'Tobacco': this.tempservice.userInput["tobacco"],
           'TotalCover': this.tempservice.userInput["stringTotalCover"],
           'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
           'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
           'PrimaryCover': this.tempservice.userInput["stringPrimaryCover"], 
           'SpouseCover': this.tempservice.userInput["stringSpouseCover"],
           'SpouseAge': this.tempservice.userInput["Partnerage"],
           'SpouseTobacco': this.tempservice.HeartCancer_InputArr["LA2_Tobacco"]
         }
         this.WindowRef.nativeWindow._satellite.track("HnC_Dec");        
       }catch(e){}
    } else {
      this.otpErrorMsg = "Please Enter OTP";
    }
  }
  closeOTP() {
    this.otp.closeDialog();
  }

  // congratulations
  removeExisting() {
    this.congratulations.closeDialog();
    this.tempservice.flag['existingCustomeradded'] = false;
    this.tempservice.flag['showHide']['ExistingCus'] = false;
    this.tempservice.flag['showHide']['notExistingCus'] = true;
    this.tempservice.HeartCancer_InputArr['LoyaltyBenefit'] = 'No';
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }
  done() {
    this.congratulations.closeDialog();
    this.tempservice.flag['existingCustomeradded'] = true;
    this.tempservice.flag['showHide']['ExistingCus'] = true;
    this.tempservice.flag['showHide']['notExistingCus'] = false;
  }
  showExistingSuccess() {
    this.congratulations.showDialog();
  }
  // remove cancer
  openRemoveCancer() {
    this.removecancer.showDialog();
  }
  closeCancer() {
    this.removecancer.closeDialog();
    
  }
  cancerYes() {
    this.removecancer.closeDialog();
    this.tempservice.flag['showHide']['removeCancerCover'] = false;
    this.tempservice.flag['showHide']['hideRemoveHeartOption'] = false;
    this.disableCancerDiv = false;
    this.globalServices.flag.cover.CancerCover = false;
    this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'Heart';
    this.tempservice.flag['Inputfields']['ComboFlag'] = "Yes";
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }
  addCancer() {
    this.tempservice.flag['showHide']['removeCancerCover'] = true;
    this.tempservice.flag['showHide']['hideRemoveHeartOption'] = true;
    this.disableCancerDiv = true;
    this.globalServices.flag.cover.CancerCover = true;
    this.tempservice.flag['Inputfields']['ComboFlag'] = "No";
    this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'CancerAndHeart';
    this.tempservice.updateHeartCancerInputArrValue(
      'LA1_CancerSumAssured',
      this.tempservice.constants['MIN_CancerCover']
    );
    this.tempservice.updateHeartCancerInputArrValue(
      'LA2_CancerSumAssured',
      this.tempservice.constants['MIN_CancerCover']
    );
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  // remove heart
  openRemoveHeart() {
    this.removeheart.showDialog();
  }
  closeHeart() {
    this.removeheart.closeDialog(); 
  }
  heartYes() {
    this.removeheart.closeDialog();
    this.tempservice.flag['Inputfields']['ComboFlag'] = "Yes";
    this.tempservice.flag['showHide']['removeHeartCover'] = false;
    this.tempservice.flag['showHide']['hideRemoveCancerOption'] = false;
    this.disableDIV = false;
    this.globalServices.flag.cover.HeartCover = false;
    this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'Cancer';
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }
  addHeart() {
    this.tempservice.flag['showHide']['removeHeartCover'] = true;
    this.tempservice.flag['showHide']['hideRemoveCancerOption'] = true;
    this.disableDIV = true;
    this.globalServices.flag.cover.HeartCover = true;
    this.tempservice.updateHeartCancerInputArrValue(
      'LA1_HeartSumAssured',
      this.tempservice.constants['MIN_HeartCover']
    );
    this.tempservice.updateHeartCancerInputArrValue(
      'LA2_HeartSumAssured',
      this.tempservice.constants['MIN_HeartCover']
    );
    
    this.tempservice.HeartCancer_InputArr['CoverageOption'] = 'CancerAndHeart';
    this.tempservice.flag['Inputfields']['ComboFlag'] = "No";
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  yesradio() {
    this.showyes = true;
    this.showno = false;
  }
  noradio() {
    this.showyes = false;
    this.showno = true;
  }
  
  onKeyPress(evt, fieldID) {
    var fieldValue = evt.target.value;
    this.dobErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
    if (this.dobErrorMsg == "") {
      this.tempservice.flag['checkValidate']['spousedob'] = true;
    } else {
      this.tempservice.flag['checkValidate']['spousedob'] = false;
    }
  }

  onKeyUp(event, fieldID) {
    var fieldValue = event.target.value;
    if ("mobileNo" == fieldID) {
      var v = event.target.value;
      event.target.value = v.replace(/[^0-9]/g, '');
      this.mobileErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.mobileErrorMsg == "") {
        this.tempservice.flag['checkValidate']['existingMobile'] = true;
      } else {
        this.tempservice.flag['checkValidate']['existingMobile'] = false;
      }
    } else if ("email" == fieldID) {
      this.emailErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.mobileErrorMsg == "") {
        this.tempservice.flag['checkValidate']['existingEmail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['existingEmail'] = false;
      }
    } else if ("otp" == fieldID) {
      this.otpErrorMsg = "";
      var v = event.target.value;
      event.target.value = v.replace(/[^0-9]/g, '');
      if (event.target.value.length == 6) {
        event.target.blur();
        this.otpExistingcustval = event.target.value;
        this.otpExistingcust = true;
      } else {
        this.otpErrorMsg = "Please Enter Valid OTP";
      }
    }
  }

  previousPage() {
    this.tempservice.premiumToLanding = true;
    this.router.navigate(['/landing-page']);
  }

  closeexistingCustomerSelected() {
    this.existingCustomerSelected.closeDialog();
     try{
       this.tempservice.datalayer['Behavior'] = "Existing Customer - No";
       this.WindowRef.nativeWindow._satellite.track("Behavior");
     }catch(e){}

  }

  openSpouseInfo() {
    this.tempservice.flag['existingCustomeradded'] = false;
    this.tempservice.HeartCancer_InputArr['LoyaltyBenefit'] = 'No';
    this.existingCustomerSelected.closeDialog();
    this.spousedetails.showDialog();
    this.tempservice.flag['showHide']['ExistingCus'] = false;
    this.tempservice.flag['showHide']['notExistingCus'] = true;
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
  }

  closespouseSelected() {
    this.spouseSelected.closeDialog();
  }

  existingCusPopUp() {
    this.spouseSelected.closeDialog();
    this.otpApiCallFailed = false;
    this.otpGenerated = false;
    this.otpVerificationFailed = false;
    this.existingcustdetails.showDialog();
    this.removeSpouse();
  }

  generateOTP() {
    let returnStatus = false;
    let splitDate = this.tempservice.HeartCancer_InputArr["LA1_DOB"].split("/");
    let temp_Current_dob = splitDate[0] + "-";

    if (splitDate[1] == "01")
      temp_Current_dob += "JAN-";
    else if (splitDate[1] == "02")
      temp_Current_dob += "FEB-";
    else if (splitDate[1] == "03")
      temp_Current_dob += "MAR-";
    else if (splitDate[1] == "04")
      temp_Current_dob += "APR-";
    else if (splitDate[1] == "05")
      temp_Current_dob += "MAY-";
    else if (splitDate[1] == "06")
      temp_Current_dob += "JUN-";
    else if (splitDate[1] == "07")
      temp_Current_dob += "JUL-";
    else if (splitDate[1] == "08")
      temp_Current_dob += "AUG-";
    else if (splitDate[1] == "09")
      temp_Current_dob += "SEP-";
    else if (splitDate[1] == "10")
      temp_Current_dob += "OCT-";
    else if (splitDate[1] == "11")
      temp_Current_dob += "NOV-";
    else if (splitDate[1] == "12")
      temp_Current_dob += "DEC-";
    temp_Current_dob += splitDate[2];

    let headers1 = new HttpHeaders();
    headers1.set('Content-Type', 'application/x-www-form-urlencoded');
    //let options = {responseType: "json"};
    let paramBean = {}
    paramBean["mobile"] = this.mobileNo.nativeElement.value;
    // paramBean["personalEmailId"] = this.email.nativeElement.value.toUpperCase();
    paramBean["isExistingCustomer"] = true;
    paramBean["dob"] = temp_Current_dob;
    paramBean = JSON.stringify(paramBean);
    let parameters = {};
    parameters["paramBean"] = paramBean;
    parameters["transactionName"] = "generateOtp";
    // let thisobj = this;
    // $.ajax({
    //   url: "https://buy.iciciprulife.com/buy/bolCustomerOtp.htm",
    //   type: "POST",
    //   crossDomain: true,
    //   xhrFields: {
    //     withCredentials: true
    //   },
    //   data: $.param(parameters),
    //   success: function (res) {
    //     console.log('Response=>' + res);
    //     thisobj.otpApiCallFailed = false;
    //     thisobj.otpGenerated = true;
    //     thisobj.otpVerificationFailed = false;
    //     //returnStatus = true;
    //     var result = JSON.parse(res);
    //     thisobj.clientId = result.clientId;

    //     if (result.clientId == "No Client found for parematers passed." || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
    //       thisobj.otpVerificationFailed = true;
    //       //returnStatus = false ;
    //     } else {
    //       thisobj.otpGenerated = true;
    //       thisobj.encodedmobile = result.mobileNo;
    //       //returnStatus = true;
    //       //return true;
    //       thisobj.Existingcustsucc();

    //     }
    //   },
    //   error: function (error) {
    //     console.log('Error Response=> ' + error);
    //     thisobj.otpApiCallFailed = true;
    //     thisobj.otpGenerated = false;
    //     thisobj.otpVerificationFailed = false;
    //     //returnStatus = false;
    //   },
    // });
    this.http.post('https://buy.iciciprulife.com/buy/bolCustomerOtp.htm', parameters, {headers: headers1, responseType: "text"}).subscribe(
          res => {
            console.log('Response=>' + res);
            this.otpApiCallFailed = false;
            this.otpGenerated = true;
            this.otpVerificationFailed = false;
            returnStatus = true;
            var result = JSON.parse(res['_body']);
            this.clientId = result.clientId;

            if(result.clientId=="No Client found for parematers passed." || (result.IPruException !== undefined && result.IPruException.status=="ERROR")){
              this.otpVerificationFailed = true;
              returnStatus = false ;
            }else{
              this.otpGenerated = true;
              returnStatus = true;
              this.Existingcustsucc();
            }
          },
          (err: HttpErrorResponse) => {
              console.log('Error Response=> ' + err);
                this.otpApiCallFailed = true;
                this.otpGenerated = false;
                this.otpVerificationFailed = false;
                returnStatus = false;
          }
      );
    return returnStatus;
  }
  Existingcustsucc() {
    this.existingcustdetails.closeDialog();
    this.otp.showDialog();
  }
  VerifyOTP() {
    var paramBean = {};
    paramBean["otpNumber"] = this.otpExistingcustval;
    paramBean["custId"] = this.clientId;    //From generateOtp request
    paramBean = JSON.stringify(paramBean);
    var parameters = {};
    parameters["paramBean"] = paramBean;
    parameters["transactionName"] = "validateOtp";
    // let thisobj = this;
    // $.ajax({
    //   url: "https://buy.iciciprulife.com/buy/bolCustomerOtp.htm",
    //   type: 'POST',
    //   crossDomain: true,
    //   xhrFields: {
    //     withCredentials: true
    //   },
    //   data: $.param(parameters),
    //   success: function (result) {
    //     result = JSON.parse(result);
    //     if (result.OtpVerificationStaus == "Invalid OTP" || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
    //       /*$('.UserMsg').text('Uh Oh. The OTP you entered is invalid. Please check and re-enter');
    //       $('.StatusUser').show();*/
    //       //$('.OTPMsg').text('Uh Oh. The OTP you entered is invalid. Please check and re-enter');
    //       this.otpErrorMsg = "Uh Oh. The OTP you entered is invalid. Please check and re-enter";
    //       //thisobj.Existingcust();
    //     } else if (result.OtpVerificationStaus == "Valid OTP") {
    //       // $('.UserMsg').text('Your details have been verified. Thank You.');
    //       this.otpErrorMsg = "";
    //       thisobj.Existingcust();
    //     }
    //     //$('.popup_loader').hide();
    //   }
    // })
    this.http.post('https://buy.iciciprulife.com/buy/bolCustomerOtp.htm', parameters).subscribe(
          resp => {
            console.log('Response=>' + resp);    
            var result = JSON.parse(resp['_body']);
            this.clientId = result.clientId;
            if (result.OtpVerificationStaus == "Invalid OTP" || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
              this.otpErrorMsg = "Uh Oh. The OTP you entered is invalid. Please check and re-enter";
            }
            else if (result.OtpVerificationStaus == "Valid OTP") {
                     this.otpErrorMsg = "";
                     this.Existingcust();
                   }
          },
      );
  }
  Existingcust() {
    this.otp.closeDialog();
    this.tempservice.HeartCancer_InputArr['LoyaltyBenefit'] = 'Yes';
    this.premiumCalc.calculatePremium();
    this.tempservice.createHncCookie();
    this.congratulations.showDialog();
  }
  serializeData(data) {
    // If this is not an object, defer to native stringification.
    if (typeof data != "object") {
      return ((data == null) ? "" : data.toString());
    }
    var buffer = [];
    // Serialize each key in the object.
    for (var name in data) {
      if (!data.hasOwnProperty(name)) {
        continue;
      }
      var value = data[name];
      buffer.push(
        encodeURIComponent(name) + "=" + encodeURIComponent((value == null) ? "" : value)
      );
    }
    // Serialize the buffer and clean it up for transportation.
    var source = buffer.join("&").replace(/%20/g, "+");
    return (source);
  }
  //Group Employee 
  
  //Group employee popup
//   showDialogGroupEmployee() {
//     if (!this.tempservice.flag['showHide']['groupEmployeeDialog']) {
//       this.groupemployee.showDialog();
//     } 
//     else if ((!this.tempservice.flag['showHide']['groupEmployeeDialog'])){
//       this.spouseSelected.showDialog();
//     }
//     var splitdob = this.tempservice.userInput["dob"].split("/");
//     var newdate = splitdob[1] + "/" + splitdob[0];
//     try {
//       this.tempservice.WindowRef.nativeWindow.dataLayer.push({
//         'event': this.tempservice.device+"_HnCCalculator_ExistingCustomer",
//         'Data_Prefilled': "No",
//         'ProductCode': "T48",
//         'ProductName': "HeartCancer",
//         'pageName': "protection : health-insurance-plans : heart-cancer-protect-calculator:"+this.tempservice.device,
//         'pagetype': "top-health : heart-cancer-protect-calculator",
//         'subsection1': "health-insurance-plans",
//         'subsection2': "heart-cancer-protect-calculator",
//         'subsection3': "health-insurance-plans-"+this.tempservice.device,
//         'channel': "protection",
//         'Subchannel': "health",
//         'PageCategory': "Calculator",
//         'PageSubCategory': "Calculator-Quote",
//         'SectionName': "Quote",
//         'UserType': "Guest",
//         'UserRole': "BOL",
//         'UserVisit': "New",
//         'Age': this.tempservice.userInput["age"],
//         'DOB': newdate,
//         'Gender': this.tempservice.userInput["gender"],
//         'Tobacco': this.tempservice.userInput["tobacco"],
//         'CancerCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
//         'HeartCover': parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
//         'totalCover': parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
//         'MonthlyPremium': this.tempservice.dataLayerArray.MonthlyPremium,
//         'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
//         'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
//         'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
//         'YearlyPremium': this.tempservice.dataLayerArray.YearlyPremium,
//         'nypremium': this.tempservice.dataLayerArray.nypremium,
//         'PolicyType': this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
//         'PaymentTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
//         'Device': this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
//         'SpouseAdded': this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
//         'UID': parseInt(this.tempservice.HeartCancer_InputArr.UID)
//       });
//     } catch (e) { }
//     try{
//       this.tempservice.datalayer['Behavior'] = "Click here if you are an existing ICICI Pru Customer";
      
//       this.WindowRef.nativeWindow._satellite.track("Behavior");
//     }catch(e){}    
//   }

//   groupEmployeesucc() {
//     this.groupemployee.closeDialog();
//   }

//   employeeVerify() {
   
//     // var headers = new Headers();
//     // headers.append('Content-Type', 'application/x-www-form-urlencoded');
//     // let options = new RequestOptions({ headers: headers });
//     let paramBean = {}
//     paramBean["ntid"] = this.ntid.nativeElement.value;
//     // paramBean["personalEmailId"] = this.email.nativeElement.value.toUpperCase();
//     paramBean["password"] = this.password.nativeElement.value;
//     paramBean["groupEmpFlag"] = "Yes";
//     // paramBean = JSON.stringify(paramBean);
//     // let parameters = {};
//     // parameters["paramBean"] = paramBean;
//     // parameters["transactionName"] = "generateOtp";
//     let thisobj = this;
//     $.ajax({
//       url: "https://buy.iciciprulife.com/buy/bolCustomerOtp.htm",
//       type: "POST",
//       crossDomain: true,
//       xhrFields: {
//         withCredentials: true
//       },
//       data: $.param(paramBean),
//       success: function (res) {
//         console.log('Response=>' + res);
       
//         //returnStatus = true;
//         var result = JSON.parse(res);
//         thisobj.clientId = result.clientId;
//         this.employeeVerificationFailed = true;

//         if (result.clientId == "No Client found for parematers passed." || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
//           thisobj.otpVerificationFailed = true;
//           //returnStatus = false ;
//         } else {
//           // thisobj.otpGenerated = true;
//           // thisobj.encodedmobile = result.mobileNo;
//           //returnStatus = true;
//           //return true;
//           thisobj.groupEmployeesucc();

//         }
//       },
//       error: function (error) {
//         console.log('Error Response=> ' + error);
//         this.employeeVerificationFailed = true;
//         // thisobj.otpApiCallFailed = true;
//         // thisobj.otpGenerated = false;
//         // thisobj.otpVerificationFailed = false;
//         //returnStatus = false;
//       },
//     });
//   }
}