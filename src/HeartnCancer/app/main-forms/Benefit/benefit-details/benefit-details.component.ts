import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { animate, style, transition, trigger } from '@angular/animations';
import { PremiumCalculateService } from '../../Premium/premium-calculate.service';
import { EditpanelComponent } from 'src/HeartnCancer/app/shared/edit-panel/editpanel.component';
import { StepsComponent } from 'src/HeartnCancer/app/shared/steps/steps.component';
import { BenefitscardComponent } from 'src/HeartnCancer/app/shared/benefits-card/benefitscard.component';
import { TempServiceService } from 'src/HeartnCancer/app/service/temp-service.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';
import { WindowRefService } from 'src/Common/services/window-ref.service';

declare const require: any;
var sha1 = require('sha1');

@Component({
  selector: 'app-benefit-details',
  templateUrl: './benefit-details.component.html',
  styleUrls: ['./benefit-details.component.css'],
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translate3d(100%, 0, 0)' }),
        animate(400)
      ])
    ])
  ]
})

export class BenefitDetailsComponent implements OnInit {
  @ViewChild('hospital')
  hospital: PopupComponent;

  @ViewChild('increasingcover')
  increasingcover: PopupComponent;

  @ViewChild('incomebenfit')
  incomebenfit: PopupComponent;

  @ViewChild('showeditMenu')
  showeditMenu: EditpanelComponent;

  @ViewChild('showBullet')
  showBullet: StepsComponent;

  @ViewChild('heartCancerJsonForm') heartCancerJsonForm: ElementRef;

  @ViewChild('HospitalCalculated') HospitalCalculated:BenefitscardComponent;
  constructor(
    private router: Router,
    public tempservice: TempServiceService,
    public WindowRef: WindowRefService,
    public premiumCalc: PremiumCalculateService
  ) {
    this.router.events.subscribe(() => {
        document.body.className = "bodySticky";
    let isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent)
    if (isIEOrEdge == true) {
      setTimeout(() => {
        document.getElementById("benefit-details").scrollIntoView({ behavior: "smooth"});
      }, 50)
    } else {
      document.getElementById("benefit-details").scrollIntoView(true);
    }
  });
  }

  ngOnInit(): void {
    this.showBullet.showBullets('BND');
  }
  showEditMenu(): void {
    this.showeditMenu.show();
  }
  
  // hospital cash
  openHosital(): void {
    this.hospital.showDialog();
  }
  closeHospital(): void {
    // this.tempservice.addHospital();
    this.hospital.closeDialog();
  }

  // increasing
  openIncreasing(): void {
    this.increasingcover.showDialog();
  }
  closeIncreasing(): void {
    //  this.tempservice.addIncreasings();
    this.increasingcover.closeDialog();
  }

  //  incomebenfit
  openIncomebenfit(): void {
    this.incomebenfit.showDialog();
  }
  closeIncomebenfit(): void {
    //  this.tempservice.addIncomebenefits();
    this.incomebenfit.closeDialog();
  }
  previousPage(): void {
    this.router.navigate(['/personal']);
  }
  BuyNow(): void {
    var newBenefits = "";
    var RiderPremium = 0;
    var hospitalAmount = 0;
    var increasingAmount = 0;
    var incomeAmount = 0;
    this.tempservice.flag['Inputfields']['ProceedAppFlag'] = "Yes";
    if( this.tempservice.HeartCancer_InputArr['HospitalBenefit'] == 'Yes'){
      if(newBenefits!="")
        newBenefits += '_Hospital Cash Benefit';
      else
        newBenefits += 'Hospital Cash Benefit';
        hospitalAmount = this.premiumCalc.hospitalBenefitAmount;
    }
    if( this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'] == 'Yes'){
      if(newBenefits!="")
        newBenefits += '_Increasing Cover benefit';
      else
        newBenefits += 'Increasing Cover benefit';
        increasingAmount = this.premiumCalc.increasingBenefitAmount
    }
    if( this.tempservice.HeartCancer_InputArr['IncomeBenefit'] == 'Yes'){
      if(newBenefits!="")
        newBenefits += '_Income Benefit';
      else
        newBenefits += 'Income Benefit';
        incomeAmount = this.premiumCalc.incomeBenefitAmount
    }

    RiderPremium = hospitalAmount+increasingAmount+incomeAmount;
    var splitdob = this.tempservice.userInput["dob"].split("/");
    var newdate = splitdob[1]+"/"+splitdob[0];
     try{
       this.tempservice.WindowRef.nativeWindow.dataLayer.push({
           'event' : this.tempservice.device+"_HnCCalcuator_ProceedToApplication",
           'Data_Prefilled' : "No",
           'ProductCode' : "T48",
           'ProductName' : "HeartCancer",
           'pageName' : "protection : health-insurance-plans : heart-cancer-protect-calculator:desktop",
           'pagetype' : "top-health : heart-cancer-protect-calculator",
           'subsection1' : "health-insurance-plans",
           'subsection2' : "heart-cancer-protect-calculator",
           'subsection3' : "",
           'channel' : "protection",
           'Subchannel' : "health",
           'PageCategory' : "Calculator",
           'PageSubCategory' : "Calculator : Rider Section|Last Step",
           'SectionName' : "Rider Section|Last Section",
           'UserType': "Guest",
           'UserRole' : "BOL",
           'UserVisit' : "New",
           'Age' : this.tempservice.userInput["age"],
           'DOB' : newdate,
           'Gender' : this.tempservice.userInput["gender"],
           'Tobacco' : this.tempservice.userInput["tobacco"],
           'CancerCover' :  parseInt(this.tempservice.HeartCancer_InputArr["LA1_CancerSumAssured"]),
           'HeartCover' : parseInt(this.tempservice.HeartCancer_InputArr["LA1_HeartSumAssured"]),
           'totalCover' : parseInt(this.tempservice.getValueFromStr(this.tempservice.sumAssured)),
           'MonthlyPremium' :  this.tempservice.dataLayerArray.MonthlyPremium,
           'PaymentFrequency' : this.tempservice.HeartCancer_InputArr["Frequency"],
           'PolicyTerm' : parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Premium' : parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
           'YearlyPremium' : this.tempservice.dataLayerArray.YearlyPremium,
           'nypremium' : this.tempservice.dataLayerArray.nypremium,
           'PolicyType' : this.tempservice.HeartCancer_InputArr["PPT"] == "Regular Pay" ? "Regular" : "",
           'PaymentTerm' :  parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
           'Device' : this.tempservice.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
           'NetworkUID1' :  sha1(this.tempservice.userInput["mobile"]),
           'NetworkUID2' : sha1(this.tempservice.userInput["email"]),
           'ICICIEmployee' : this.tempservice.HeartCancer_InputArr["Staff"],
           'AnnualIncome' : this.tempservice.userInput["annualPackage"].replace(/\s/g, ""),
           'Pcode' : this.tempservice.userInput["pincode"],
           'OptionSelected' : newBenefits, //this.benefitsAdded.join(', ')
           'RiderPremium' : RiderPremium ,
           'HospitalCashBenefit' : this.tempservice.HeartCancer_InputArr['HospitalBenefit'],
           'IncreasingCoverBeneift' : this.tempservice.HeartCancer_InputArr['IncreasingCoverBenefit'],
           'IncomeBenefit' : this.tempservice.HeartCancer_InputArr['IncomeBenefit'],
           'SpouseAdded' :  this.tempservice.HeartCancer_InputArr["FamilyBenefit"],
           'UID' : parseInt(this.tempservice.HeartCancer_InputArr.UID)
       });
     }catch(e){}
     try{
       this.tempservice.datalayer = {
         'Section':"HnC Proceed to App",
         'ProductCode':"T48",
         'Age': this.tempservice.userInput["age"],
         'Gender': this.tempservice.userInput["gender"],
         'Tobacco': this.tempservice.userInput["tobacco"],
         'TotalCover': this.tempservice.userInput["stringTotalCover"],
         'Premium': parseInt(this.tempservice.removeCommas(this.tempservice.premiumAmount)),
         'PolicyTerm': parseInt(this.tempservice.HeartCancer_InputArr["PolicyTerm"]),
         'NetworkUID1' :  sha1(this.tempservice.userInput["mobile"]),
         'NetworkUID2' : sha1(this.tempservice.userInput["email"]),
         'PaymentFrequency': this.tempservice.HeartCancer_InputArr["Frequency"],
         'SpouseAge': this.tempservice.userInput["Partnerage"],
         'SpouseTobacco': this.tempservice.HeartCancer_InputArr["LA2_Tobacco"],
         'Benefits': '',
         'AnnualIncome' : this.tempservice.userInput["annualPackage"].replace(/\s/g, ""),
       }
       this.WindowRef.nativeWindow._satellite.track("HnC_Dec");
     }catch(e){}
    this.router.navigate(['/final-loader']);
  }
}
