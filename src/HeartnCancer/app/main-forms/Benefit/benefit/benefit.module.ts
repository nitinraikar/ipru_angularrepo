import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { BenefitRoutingModule } from './benefit-routing.module';
import { BenefitComponent } from './benefit.component';
import { BenefitDetailsComponent } from '../benefit-details/benefit-details.component';
import { BenefitscardComponent } from 'src/HeartnCancer/app/shared/benefits-card/benefitscard.component';
import { PremiumAmountModule } from 'src/HeartnCancer/app/shared/premium-amount/premium-amount/premium-amount.module';
import { PlusminusModule } from 'src/HeartnCancer/app/shared/plusminus/plusminus/plusminus.module';
import { EditpanelModule } from 'src/HeartnCancer/app/shared/edit-panel/editpanel/editpanel.module';
import { StepsModule } from 'src/HeartnCancer/app/shared/steps/steps/steps.module';
import { PopupModule } from 'src/Common/components/popup/popup/popup.module';

@NgModule({
  imports: [
    CommonModule,
    BenefitRoutingModule,
    PopupModule,
    StepsModule,
    FormsModule,
    EditpanelModule,
    PlusminusModule,
    PremiumAmountModule,
    HttpModule
  ],
  declarations: [
    BenefitComponent,
    BenefitDetailsComponent,
    BenefitscardComponent
  ]
})
export class BenefitModule { }
