import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BenefitComponent } from './benefit.component';
import { BenefitDetailsComponent } from '../benefit-details/benefit-details.component';

const routes: Routes = [
  {
    path: '',
    component: BenefitComponent,
    children: [
      { path: '', redirectTo: 'benefit-details', pathMatch: 'full' },
      { path: 'benefit-details', component: BenefitDetailsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BenefitRoutingModule { }
