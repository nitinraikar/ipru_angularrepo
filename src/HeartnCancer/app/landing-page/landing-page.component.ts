import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { TempServiceService } from './../service/temp-service.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})

export class LandingPageComponent implements OnInit,AfterViewInit {

  uidArray = ["1160", "1024", "1170", "1413", "2399"];
  
  constructor(private TempService: TempServiceService, private router: Router) { }

  ngOnInit() {
    
    this.router.navigate([{ outlets: {  header: ['header'] } }], { skipLocationChange: true});
    
    if (this.TempService.WindowRef.nativeWindow.innerWidth<=768) {
      this.TempService.flag['Inputfields'].isDevisetypeMobile=true;
    } else {
      this.TempService.flag['Inputfields'].isDevisetypeMobile=false;
  }
}
  ngAfterViewInit() {  
      let uidBannerLocal = 'false';
      let uidLocal = this.TempService.HeartCancer_InputArr['UID'];
      if (this.uidArray.indexOf(uidLocal) > -1) {
        uidBannerLocal = 'true';
      }
      if (this.TempService.premiumToLanding) {
        document.getElementById('basicDetails-scroll').scrollIntoView(true);
        this.TempService.premiumToLanding = false;
      } 
      else {
        sessionStorage.setItem('Glb_UID', uidLocal);
        sessionStorage.setItem('Glb_UID_Banner', uidBannerLocal);
        this.redirectToId();
      }
       try {
         this.TempService.WindowRef.nativeWindow.dataLayer.push({
             'event' : this.TempService.device+"_HnCCalculator_Landed",
             'ProductCode' : "T48",
             'ProductName' : "HeartCancer",
             'pageName' : "protection : health-insurance-plans : heart-cancer-protect-calculator",
             'pagetype' : "top-health : heart-cancer-protect-calculator",
             'subsection1' : "health-insurance-plans",
             'subsection2' : "heart-cancer-protect-calculator",
             'subsection3' : "",
             'channel' : "protection",
             'Subchannel' : "health",
             'PageCategory' : "Calculator",
             'PageSubCategory' :"Calculator Landing Page",
             'SectionName' : "Calculator - PreQuoteScreen",
             'UserType': "Guest",
             'UserRole' : "BOL",
             'UserVisit' : "New",
             'Device' : this.TempService.flag['Inputfields'].isDevisetypeMobile == true ? "Mobile" : "Desktop",
             'UID' : parseInt(this.TempService.HeartCancer_InputArr.UID)
           });
         }catch(e){}
         try{
           this.TempService.datalayer = {
             'pagetype':"bottom-health : heart-cancer-protect-calculator",
             'pageName':"protection : health-insurance-plans : heart-cancer-protect-calculator",
             'channel':"protection",
             'subsection1':"health-insurance-plans",
             'subsection2':"heart-cancer-protect-calculator",
             'subsection3':"health-insurance-plans-desktop",
             'ProductCode':"T48",
             'ProductName':"heart and cancer"
           }
         }catch(e){}
    }

  redirectToId() {
    setTimeout(() => {
      let redirectId = 'basicDetails-scroll';
      const uidLocalBanner = sessionStorage.getItem('Glb_UID_Banner');
      if (uidLocalBanner === 'true') {
        this.router.navigate([{ outlets: { header:['header'],banner: ['banner'],benefits: ['benefits'] } }], { skipLocationChange: true});
        
        redirectId = 'banner-scroll';
      }
      if (!document.getElementById(redirectId)) {
        this.redirectToId();
      } else {
        document.getElementById(redirectId).scrollIntoView(true);
        this.router.navigate([{ outlets: { header:['header'] } }], { skipLocationChange: true});
      }
    });
  }
}
// reason: ['reason'] ,plan: ['plan'], benefits: ['benefits'],