import { Injectable, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { HncEbiService } from 'src/Common/services/hnc-ebi.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';
import { CommonService } from 'src/Common/services/common.service';

@Injectable({
  providedIn: 'root'
})
export class TempServiceService {
  dateMask: any;
  premiumHeartCancerEbi;
  setPolicyTermDropdownprimary = [];
  dataLayerArray: any;
  datalayer: object;
  premium;
  premiumAmount = 67;
  sumAssured;
  DOB;
  age;
  constants;
  SEM_WS_call_flag=0;
  flag = {};
  userInput = {};
  Cancer_InputArr: any;
  mobileStructure;
  desktopStructure;
  showHideMobile;
  showHideMobilePersonal;
  IEBrowser = false;
  checkCustomiseHNC; 
  
  public newPremiumAmount;
  public discountAmount;
  public oldPremiumAmount;

  invokeEvent: Subject<any> = new Subject();
  invokeEvent1: Subject<any> = new Subject();
  invokeEvent2: Subject<any> = new Subject();
  @ViewChild('existingcustdetails') existingcustdetails: PopupComponent; 
  
  constructor(private CallEbi: HncEbiService,public commonService: CommonService) {
    this.flag = {
      Inputfields: {
        Male: true,
        Female: false,
        gender: true,
        TobaccoYes: false,
        TobaccoNo: true,
        tobacco: true,
        dateofbirth: false,
        validMobNo: false,
        validName: false,
        validEmail: false,
        validPincode: false,
        traiFlag:true,
        TermsAndCond:true,
        existingCustomeradded: false
      },
      checkValidate: {
        pincodeDetail:false,
        emailDetail:false,
        nameDetail:false,
        mobileDetail:false,
        existingMobile: false
      },
      showHide: {
        personalPage: false,
        disableBtn:true,
        quoteSection:true,
        IEBasicDetailsPage:true,
        IEPersonalDetailsPage:false,
        ExistingCus: false,
        notExistingCus: true,
      },
      InfoSubmitFlag: true
    }

    this.Cancer_InputArr = {
      LA1_DOB: "01/01/1990",
      LA2_DOB: "01/01/1985",
      LA1_Gender: "Male",
      LA2_Gender: "Female",
      LA1_HeartSumAssured: "0",
      LA2_HeartSumAssured: "0",
      LA1_CancerSumAssured: "1000000",
      LA2_CancerSumAssured: "0",
      PolicyTerm: "20",
      Frequency: "Monthly",
      PPT: "Regular Pay",
      CoverageOption: "Cancer",
      SalesChannel: "Online",
      Staff: "No",
      LoyaltyBenefit: "No",
      FamilyBenefit: "No",
      HospitalBenefit: "No",
      IncreasingCoverBenefit: "No",
      IncomeBenefit: "No",
      LA1_Tobacco: "No",
      LA2_Tobacco: "No",
      UID: "480"
    };
    this.userInput = {
      gender: "",
      tobacco: "",
      dob: "",
      name: "",
      mobile: "",
      email: "",
      pincode: "",
      age: "",
      LA1_cancerCover:""
    };
    this.constants = {
      ZERO_Cover: 0,
      MIN_CancerCover: 200000,
      MAX_CancerCover: 5000000
    };
    this.dateMask = [
      /[0-9]/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      /\d/,
      /\d/
    ];
  }

  onChangeRadio(evt, fieldID) {
    let fieldValue = evt.target.value;
    if ("gender" == fieldID) {
      if ("Male" == fieldValue) {
        this.userInput["gender"] = fieldValue;
        this.Cancer_InputArr["LA1_Gender"] = fieldValue;
        console.log("Gender", this.Cancer_InputArr["LA1_Gender"]);
      } else {
        this.userInput["gender"] = fieldValue;
        this.Cancer_InputArr["LA1_Gender"] = fieldValue;
        console.log("Gender", this.Cancer_InputArr["LA1_Gender"]);
      }
    }
    if ("tobacco" == fieldID) {
      if ("Yes" == fieldValue) {
        this.userInput["tobacco"] = fieldValue;
        this.Cancer_InputArr["LA1_Tobacco"] = fieldValue;
        console.log("tobacco", this.Cancer_InputArr["LA1_Tobacco"]);
      } else {
        this.userInput["tobacco"] = fieldValue;
        this.Cancer_InputArr["LA1_Tobacco"] = fieldValue;
      }
    }
    this.calculatePremium();  
  }

  Fieldvalidation(fieldName, fieldValue) {
    if ("dobDate" == fieldName) {
      let exp = /(\d{2})\/(\d{2})\/(\d{4})/;
      if (exp.test(fieldValue)) {
        if (this.commonService.getAge(fieldValue) >= 18 && this.commonService.getAge(fieldValue) <= 65) {
          this.DOB = fieldValue;
          this.age = this.commonService.getAge(fieldValue);
          this.setPolicyTermDropdownprimary = this.setPolicyTermDropdown(
            this.age
          );
          this.userInput["age"] = this.commonService.getAge(fieldValue);
          this.Cancer_InputArr["LA1_DOB"] = fieldValue;
          this.flag["Inputfields"]["dateofbirth"] = true;
          if (this.flag["Inputfields"]["dateofbirth"] &&
            (this.flag["Inputfields"]["Male"] || this.flag["Inputfields"]["Female"]) &&
            (this.flag["Inputfields"]["TobaccoYes"] || this.flag["Inputfields"]["TobaccoNo"])
          ) {
            this.flag['showHide']['disableBtn'] = false;
          }
          this.calculatePremium();  
          return "";
        } else {
          this.flag["Inputfields"]["dateofbirth"] = false;
          if (this.flag["Inputfields"]["dateofbirth"] &&
            (this.flag["Inputfields"]["Male"] || this.flag["Inputfields"]["Female"]) &&
            (this.flag["Inputfields"]["TobaccoYes"] || this.flag["Inputfields"]["TobaccoNo"])
          ) {
            this.flag['showHide']['disableBtn'] = true;
          }
          return "This plan is available for age 18 to 65";
        }
      }else {
        return "This plan is available for age 18 to 65";
      }
    }else if ("first-name" == fieldName) {
      let letterRegx = /^(?=.{2,20}$)(([a-zA-Z ])\2?(?!\2))+$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validName"] = false;
        return "Enter your name";
      }else if(letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validName"] = true; 
        return "";
      }
    }else if ("mobile-number" === fieldName || fieldName === "mobileNo") {
      let letterRegx = /^[6789]\d{9}$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validMobNo"] = false;
        return "Enter valid mobile number";
      }else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validMobNo"] = true;
        this.userInput['mobile'] = fieldValue; 
        return "";
      }
    }else if ("email" == fieldName) {
      let letterRegx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validEmail"] = false;
        return "Enter valid email id";
      } else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validEmail"] = true;
        return "";
      }
    }else if ("pincode" == fieldName) {
      let letterRegx = /^[1-9][0-9]{5}$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validPincode"] = false;
        return "Enter a valid pin code";
      }else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validPincode"] = true;
        return "";
      }
    }
  }

  onChangeDropdown(evt, fieldID) {
    let fieldValue = evt.target.value;
    this.userInput["Frequency"] = fieldValue;
    if ("policyterm" == fieldID) {
      this.Cancer_InputArr["PolicyTerm"] = fieldValue;
    } else if ("Frequency" == fieldID) {
      this.Cancer_InputArr["Frequency"] = fieldValue;
    } else if ("annualPackage" == fieldID) {
      this.userInput["annualPackage"] = fieldValue;
    }
    this.calculatePremium();  
  }

  setPolicyTermDropdown(age) {
    let policyterm = [];
    if (75 - age >= 40) {
      for (let i = 5; i <= 40; i++) {
        policyterm.push(i);
      }
    } else {
      for (let i = 5; i <= 75 - age; i++) {
        policyterm.push(i);
      }
      if (75 - age <= 20) {
        this.Cancer_InputArr["PolicyTerm"] = (75 - age).toString();
        // this.subject.next(this.HeartCancer_InputArr);
      }
    }
    return policyterm;
  }

  calculatePremium(): void {
    if(this.Cancer_InputArr['CoverageOption'] === 'Cancer'){
      this.Cancer_InputArr['LA1_HeartSumAssured'] = '0';
      this.Cancer_InputArr['LA2_HeartSumAssured'] = '0';
    }
    else if(this.Cancer_InputArr['CoverageOption'] === 'Heart'){
      this.Cancer_InputArr['LA1_CancerSumAssured'] = '0';
      this.Cancer_InputArr['LA2_CancerSumAssured'] = '0';
    }

    var temp_freq = this.Cancer_InputArr['Frequency'];
    // For yearly monthly premium
    //For nypremium
    var temp_dob = this.Cancer_InputArr["LA1_DOB"] ;
    var temp_dob_split = temp_dob.split("/");
    var plusyr = parseInt(temp_dob_split[2])+1;
    var temp_plusone_dob = temp_dob_split[0]+"/"+temp_dob_split[1]+"/"+plusyr;
    if(this.CallEbi.calculateAge(temp_plusone_dob) >=18){
      this.Cancer_InputArr['LA1_DOB'] = temp_plusone_dob;
    }
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.nypremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    this.Cancer_InputArr['LA1_DOB'] = temp_dob;

    //For Monthlly
    this.Cancer_InputArr['Frequency'] = "Monthly";
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.MonthlyPremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]

    //For Yearly
    this.Cancer_InputArr['Frequency'] = "Yearly";
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.YearlyPremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    // For yearly monthly premium

    this.Cancer_InputArr['Frequency'] = temp_freq;

    // For Benefits
    // let temp_hospitslbf = this.HeartCancer_InputArr['HospitalBenefit'];
    // let temp_incombf = this.HeartCancer_InputArr['IncomeBenefit'];
    // let temp_increasingbf = this.HeartCancer_InputArr['IncreasingCoverBenefit'];
    
    // this.HeartCancer_InputArr['HospitalBenefit'] = "Yes";
    // this.HeartCancer_InputArr['IncomeBenefit'] = "Yes";
    // this.HeartCancer_InputArr['IncreasingCoverBenefit'] = "Yes";
    
    // this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
    //   this.HeartCancer_InputArr
    // );
    // this.premium = this.CallEbi.calculateEBI(
    //   this.HeartCancer_InputArr,
    //   this.premiumHeartCancerEbi
    // );

    // this.hospitalBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
    //   this.premiumHeartCancerEbi.HospitalBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.Cancer_HospitalBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.LA2_HospitalBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.LA2_Cancer_HospitalBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.XRT_HospitalBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_HospitalBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_Cancer_HospitalBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_Cancer_HospitalBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_HospitalBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_HospitalBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_Cancer_HospitalBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_Cancer_HospitalBenefit_SecondPremium)
    // );
    // this.increasingBenefitAmount = this.CallEbi.CalculateBenefitTax(Math.round(
    //   this.premiumHeartCancerEbi.IncreasingCoverBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.Cancer_IncreasingCoverBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.LA2_IncreasingCoverBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.LA2_Cancer_IncreasingCoverBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.XRT_IncreasingCoverBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_IncreasingCoverBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_Cancer_IncreasingCoverBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_Cancer_IncreasingCoverBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_IncreasingCoverBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_IncreasingCoverBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncreasingCoverBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncreasingCoverBenefit_SecondPremium)
    // );
    // this.incomeBenefitAmount =this.CallEbi.CalculateBenefitTax( Math.round(
    //   this.premiumHeartCancerEbi.IncomeBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.Cancer_IncomeBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.LA2_IncomeBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.LA2_Cancer_IncomeBenefit_ShowPremium +
    //     this.premiumHeartCancerEbi.XRT_IncomeBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_IncomeBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_Cancer_IncomeBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_Cancer_IncomeBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_IncomeBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_IncomeBenefit_SecondPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncomeBenefit_FirstPremium +
    //     this.premiumHeartCancerEbi.XRT_LA2_Cancer_IncomeBenefit_SecondPremium)
    //     );
    
    // this.HeartCancer_InputArr['HospitalBenefit'] = temp_hospitslbf;
    // this.HeartCancer_InputArr['IncomeBenefit'] = temp_incombf;
    // this.HeartCancer_InputArr['IncreasingCoverBenefit'] = temp_increasingbf;
    //For benefits
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    this.premiumAmount = this.commonService.addCommas(
      this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    );
    
    this.sumAssured = this.commonService.valueRoundup(
      parseInt(this.Cancer_InputArr.LA1_HeartSumAssured) +
        parseInt(this.Cancer_InputArr.LA1_CancerSumAssured)
    );

    // this.totalSumAssuredWithSpouse = this.valueRoundup(
    //   parseInt(this.HeartCancer_InputArr.LA1_HeartSumAssured) +
    //     parseInt(this.HeartCancer_InputArr.LA1_CancerSumAssured) +
    //     parseInt(this.HeartCancer_InputArr.LA2_HeartSumAssured) +
    //     parseInt(this.HeartCancer_InputArr.LA2_CancerSumAssured)
    // );

    this.newPremiumAmount = Math.round(this.premium["LA1_first_premium"]);
    this.oldPremiumAmount = Math.round(this.newPremiumAmount / 0.95);
    this.discountAmount = Math.round(this.oldPremiumAmount - this.newPremiumAmount);
  }

  toggle() {
    this.invokeEvent.next('editTaxes');
  }
  showOTP() {
    console.log("inside tempservice");
    this.invokeEvent1.next('editTaxes1')
  }

  showCongrats() {
    console.log("inside ...............");
    this.invokeEvent2.next('editTaxes2')
  }
}
