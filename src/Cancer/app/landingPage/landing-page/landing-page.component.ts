import { Component, OnInit } from '@angular/core';
import { TempServiceService } from '../../Service/temp-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {
  cancerCover: any;
  constructor(public tempservice: TempServiceService, private route: ActivatedRoute) { }

  ngOnInit() {
    // if (window.location.href.includes("?"))
    // if (window.location.href.split("?")[1].match("Customise_HNC=true")) 
    // if (this.tempservice.checkCustomiseHNC){
    this.route.queryParamMap.subscribe(params => {
      console.log("params:-", params)
      this.cancerCover = params['params'].CancerCover;
      if (this.cancerCover) {
        this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] = this.cancerCover;
        this.tempservice.userInput['LA1_cancerCover'] = this.cancerCover;
      }
    })
    // }
    try {
      this.tempservice.datalayer = {
        'pagetype': "bottom-health : heart-cancer-protect-calculator",
        'pageName': "protection : health-insurance-plans : cancer-cover-details-calculator",
        'channel': "protection",
        'subsection1': "health-insurance-plans",
        'subsection2': "cancer-cover-details-calculator",
        'ProductCode': "T48",
        'ProductName': "heart and cancer"
      }
    } catch (e) { }
  }
}
