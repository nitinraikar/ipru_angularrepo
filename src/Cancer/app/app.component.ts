import { Component } from '@angular/core';
import { TempServiceService } from './Service/temp-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'iciciCancer';

  constructor(public tempservice: TempServiceService) {}

  ngOnInit() { 
    window.onbeforeunload = function () {
      window.scrollTo(0, 0);
    }

    if (this.ipru_getParameterByName("UID", "") != undefined && this.ipru_getParameterByName("UID", "") != "") {
      this.tempservice.Cancer_InputArr['UID'] = this.ipru_getParameterByName("UID", "");
    } else {
      this.tempservice.Cancer_InputArr['UID'] = "480";
    }

    // if(this.ipru_getParameterByName("Customise_HNC=true", "") != undefined && this.ipru_getParameterByName("Customise_HNC=true", "") != "") {
    //   this.tempservice.checkCustomiseHNC = true;
    // }else {
    //   this.tempservice.checkCustomiseHNC = false;
    // }
  }

  ipru_getParameterByName(name, query): any {
    if (!query)
      query = window.location.search;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(query);
    if (results == null)
      return "";
    else
      return decodeURIComponent(results[1].replace(/\+/g, " "));
  }
}