import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { MatExpansionModule } from '@angular/material/expansion';
import { HttpClientModule } from '@angular/common/http';

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { TextMaskModule } from 'angular2-text-mask';
import { FlipModule } from 'ngx-flip';
import { DialogModule } from 'primeng/dialog';

import { AppComponent } from './app.component';
import { LandingPageComponent } from './landingPage/landing-page/landing-page.component';
import { HeaderComponent } from './FrontPage_Modules/header/header.component';
import { BannerComponent } from './FrontPage_Modules/banner/banner.component';
import { ScoreCardComponent } from './FrontPage_Modules/score-card/score-card.component';
import { PlusminusComponent } from './Shared/plusminus/plusminus.component';
import { BasicDetailsComponent } from './MainForms/basic-details/basic-details.component';
import { PersonalDetailsComponent } from './MainForms/personal-details/personal-details.component';

import { BenefitsComponent } from 'src/Common/components/benefits/benefits.component';
import { DependOnUsComponent } from 'src/Common/components/depend-on-us/depend-on-us.component';
import { PayoutOptionsComponent } from 'src/Common/components/payout-options/payout-options.component';
import { DisclaimerComponent } from 'src/Common/components/disclaimer/disclaimer.component';

import { TempServiceService } from './Service/temp-service.service';
import { HncEbiService } from 'src/Common/services/hnc-ebi.service';
import { BlowfishEncryptionService } from 'src/Common/services/blowfish-encryption.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    HeaderComponent,
    BenefitsComponent,
    BannerComponent,
    DependOnUsComponent,
    ScoreCardComponent,
    PayoutOptionsComponent,
    DisclaimerComponent,
    PlusminusComponent,
    BasicDetailsComponent,
    PersonalDetailsComponent,
    PopupComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    FormsModule,
    TextMaskModule,
    HttpClientModule,
    FlipModule,
    DialogModule,
    ScrollToModule.forRoot(),
    RouterModule.forRoot([
      { path: '', redirectTo: 'landing-page', pathMatch: 'full' },
      { path: 'landing-page', component: LandingPageComponent }
    ],{ useHash: true }) 
  ],
  providers: [
    TempServiceService,
    // CancerEbiService,
    HncEbiService,
    BlowfishEncryptionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
