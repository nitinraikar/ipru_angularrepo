import { Component, OnInit, ViewChild } from '@angular/core';
import { TempServiceService } from '../../Service/temp-service.service';
import { BannerComponent } from '../../FrontPage_Modules/banner/banner.component';
import { ActivatedRoute } from '@angular/router';
import { WindowRefService } from 'src/Common/services/window-ref.service';
import { CommonService } from 'src/Common/services/common.service';

@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.css']
})
export class BasicDetailsComponent implements OnInit {
  dobErrorMsg;
  cancerCover;
  gender: any;
  // paymentFrequency;
  // policyTerm;
  smoker;

  @ViewChild('banner') banner: BannerComponent;
  constructor(public tempservice: TempServiceService,public WindowRef: WindowRefService, private route:ActivatedRoute, public commonService: CommonService) { }

  ngOnInit() {
    // if (window.location.href.includes("/?"))
      // if (window.location.href.split("/?")[1].match("Customise_HNC=true")){ 
      // if (this.tempservice.checkCustomiseHNC){
    this.route.queryParamMap.subscribe(params => {
      console.log("params:-",  params)
      this.cancerCover = params['params'].CancerCover;
        // if(this.cancerCover) {
        //   console.log("sdsaffdsfd");
        //   this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] = this.cancerCover;
        //   this.tempservice.userInput['LA1_cancerCover'] = this.cancerCover;
        // }
        this.gender = params['params'].Gender;
        if(this.gender){
          if (this.gender == 'Male'){
            this.tempservice.flag['Inputfields']['Male'] = true;
            this.tempservice.flag['Inputfields']['Female'] = false;
            document.getElementById('male').click();
          }else {
            this.tempservice.flag['Inputfields']['Female'] = true;
            this.tempservice.flag['Inputfields']['Male'] = false;
            document.getElementById('female').click();
          }
        }
        this.smoker = params['params'].Smoker;
        if(this.smoker){
          if (this.smoker == 'Yes') {
            this.tempservice.flag['Inputfields']['TobaccoYes'] = true;
            this.tempservice.flag['Inputfields']['TobaccoNo'] = false;
            document.getElementById('Yes').click();
          }else {
            this.tempservice.flag['Inputfields']['TobaccoYes'] = false;
            this.tempservice.flag['Inputfields']['TobaccoNo'] = true;
            document.getElementById('No').click();
          }
        }
    });
  // }
}
  // function to move into personalPage
  nextPage() {
    this.tempservice.flag['showHide']['personalPage'] = true;
    this.tempservice.flag['showHide']['IEBasicDetailsPage'] = false;
    this.tempservice.flag['showHide']['IEPersonalDetailsPage'] = true;
    try{
        this.tempservice.datalayer = {
          'Section':"HnC Quote",
          'ProductCode':"T48",
          'Gender': this.tempservice.userInput["gender"],
          'Age': this.tempservice.userInput["age"],
          'Tobacco': this.tempservice.userInput["tobacco"],
          'Premium': parseInt(this.commonService.removeCommas(this.tempservice.premiumAmount)),
          'PaymentFrequency': this.tempservice.Cancer_InputArr["Frequency"],
          'PolicyTerm': parseInt(this.tempservice.Cancer_InputArr["PolicyTerm"]),
          'TotalCover':parseInt(this.commonService.getValueFromStr(this.tempservice.sumAssured)),
        }
        this.WindowRef.nativeWindow._satellite.track("HnC_Dec_1");
      }catch(e){}
  }

  backToQuote() {
    this.tempservice.showHideMobile = false;
    this.tempservice.flag['showHide']['quoteSection'] = true;
  }

  // function to check Dob
  onKeyPress(evt, fieldID) {
    const fieldValue = evt.target.value;
    this.dobErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
    if (this.dobErrorMsg == "") {
      this.tempservice.flag['showHide']['disableBtn'] = false;
    } else {
      this.tempservice.flag['showHide']['disableBtn'] = true;
    }
  }

  showDialogExistingCustomer() {
    if (this.dobErrorMsg == "") {
      this.tempservice.toggle();
      }else {
        this.dobErrorMsg = "Please Enter Date of Birth";
      }
  }
  showDialogExistingCongrats() {
    this.tempservice.showCongrats();
  }
}
