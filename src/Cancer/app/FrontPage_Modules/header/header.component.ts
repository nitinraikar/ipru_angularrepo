import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { TempServiceService } from '../../Service/temp-service.service';
import { WindowRefService } from 'src/Common/services/window-ref.service';
import { PopupComponent } from 'src/Common/components/popup/popup.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  addCss = false;
  visibleSidebar2 = false;
  opened;
  opnscroll = false;
  showDownCalIcon = false;
  isSelected = false;
  showReturnHomeIcon = false;
  showQuoteScroll = true;
  leftDiv = true;
  showpanel;
  expand = false;

  @ViewChild('callRequest') callRequest: PopupComponent;
  constructor(public WindowRef: WindowRefService,
    private tempservice: TempServiceService
    ) { }

  ngOnInit() {
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    const element = window.pageYOffset;
    if (element > 500) {
      this.opnscroll = true;
      this.showDownCalIcon = true;
      this.isSelected = true;
      this.changeGetQuoteButtonColor(true);
      this.expand = true;
    } else if (element < 500) {
      this.opnscroll = false;
      this.showDownCalIcon = false;
      this.isSelected = false;
      this.expand = false;
    }
    
    const bottomHeight = window.scrollY + window.innerHeight;
    if (bottomHeight > 5200) {
      this.showReturnHomeIcon = true;
    } else {
      this.showReturnHomeIcon = false;
    }
    try{
      this.tempservice.datalayer['Scroll'] = '100';      
      this.WindowRef.nativeWindow._satellite.track("Scroll");
    }catch(e){}
  }

  showHeader(): void {
    this.visibleSidebar2 = true;
    this.leftDiv = true;
    this.showpanel = !this.showpanel;
  }

  toggle(): void {
    this.opened = true;
  }

  show(): void {
    this.scrollFunction();
    try {
      this.tempservice.datalayer['Behavior'] = "BENEFITS";
      this.WindowRef.nativeWindow._satellite.track("Behavior");
    }catch(e){}
  }

  assurance() {
    try{
      this.tempservice.datalayer['Behavior'] = "ASSURANCE";
      this.WindowRef.nativeWindow._satellite.track("Behavior");
    }catch(e){}
  }

  close(): void {
    this.showpanel = false;
    this.scrollFunction();
  }

	callRequestMethod(): void {
    this.showpanel = false;
    this.scrollFunction();
    this.callRequest.showDialog();

    try{
      this.tempservice.datalayer['Behavior'] = "NEED HELP";
      this.WindowRef.nativeWindow._satellite.track("Behavior");
    }catch(e){}
    
  }
  scrollFunction(): void {
    document.body.className = "";
  }

  changeGetQuoteButtonColor(add): void {
    this.addCss = add ? true : false;
  }

  Clk_Brochure() {
    //window.open('./../../content/dam/icicipru/brochures/heart_and_cancer.pdf');
    window.open('https://www.iciciprulife.com/content/dam/icicipru/brochures/heart_and_cancer.pdf');
    try{
      this.tempservice.datalayer['Behavior'] = "DOWNLOADS";
      this.WindowRef.nativeWindow._satellite.track("Behavior");
    }catch(e){}
  }
  tracker(){
    window.open("https://buy.iciciprulife.com/buy/apptrackerhomeNew.htm");
    try{
      this.tempservice.datalayer['Behavior'] = "TRACK APPLICATION";
      this.WindowRef.nativeWindow._satellite.track("Behavior");
    }catch(e){}
  }
  
	closecallRequest() {
     this.callRequest.closeDialog();
  }

  checkPremium() {
    try{
      this.tempservice.datalayer['Behavior'] = "CHECK PREMIUM";
      this.WindowRef.nativeWindow._satellite.track("Behavior");
    }catch(e){}
  }
}
