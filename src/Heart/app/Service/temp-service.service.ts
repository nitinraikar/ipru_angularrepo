import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { HncEbiService } from 'src/Common/services/hnc-ebi.service';
import { CommonService } from 'src/Common/services/common.service';

@Injectable({
  providedIn: 'root'
})
export class TempServiceService {
  dateMask: any;
  premiumHeartCancerEbi;
  setPolicyTermDropdownprimary = [];
  dataLayerArray: any;
  premium;
  premiumAmount;
  sumAssured;
  DOB;
  age;
  constants;
  Boundary_Conditions;
  SEM_WS_call_flag=0;
  flag = {};
  userInput = {};
  Cancer_InputArr: any;
  mobileStructure;
  desktopStructure;
  showHideMobile;
  showHideMobilePersonal;
  IEBrowser = false;

  public newPremiumAmount;
  public discountAmount;
  public oldPremiumAmount;
  public subject = new Subject<any>();

  invokeEvent: Subject<any> = new Subject();
  invokeEvent1: Subject<any> = new Subject();
  invokeEvent2: Subject<any> = new Subject();
  constructor(private CallEbi: HncEbiService,public commonService: CommonService) {
    this.flag = {
      showHide: {
        personalPage: false,
        disableBtn:true,
        quoteSection:true,
        IEBasicDetailsPage:true,
        IEPersonalDetailsPage:false,
        ExistingCus: false,
        notExistingCus: true
      },
      Inputfields: {
        Male: true,
        Female: false,
        gender: true,
        TobaccoYes: false,
        TobaccoNo: true,
        tobacco: true,
        dateofbirth: false,
        validMobNo: false,
        validName: false,
        validEmail: false,
        validPincode: false,
        traiFlag:true,
        TermsAndCond:true
      },
      checkValidate: {
        pincodeDetail:false,
        emailDetail:false,
        nameDetail:false,
        mobileDetail:false,
        existingMobile: false
      },
      InfoSubmitFlag: true
    }

    this.Cancer_InputArr = {
      LA1_DOB: "01/01/1990",
      LA2_DOB: "01/01/1985",
      LA1_Gender: "Male",
      LA2_Gender: "Female",
      LA1_HeartSumAssured: "1000000",
      LA2_HeartSumAssured: "0",
      LA1_CancerSumAssured: "0",
      LA2_CancerSumAssured: "0",
      PolicyTerm: "20",
      Frequency: "Monthly",
      PPT: "Regular Pay",
      CoverageOption: "Heart",
      SalesChannel: "Online",
      Staff: "No",
      LoyaltyBenefit: "No",
      FamilyBenefit: "No",
      HospitalBenefit: "No",
      IncreasingCoverBenefit: "No",
      IncomeBenefit: "No",
      LA1_Tobacco: "No",
      LA2_Tobacco: "No",
      UID: "480"
    };
    this.userInput = {
      gender: "Male",
      tobacco: "No",
      dob: "",
      name: "",
      mobile: "",
      email: "",
      pincode: "",
      age: "",
      LA1_cancerCover:""
    };
    this.constants = {
      ZERO_Cover: 0,
      MIN_CancerCover: 200000,
      MAX_CancerCover: 5000000,
      MIN_HeartCover: 200000,
      MAX_HeartCover: 2500000,
    };

    this.Boundary_Conditions = {
      HSA_Max_LA1: "",
    };
    this.dateMask = [
      /[0-9]/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      /\d/,
      /\d/
    ];
  }

  onChangeRadio(evt, fieldID) {
    let fieldValue = evt.target.value;
    if ("gender" == fieldID) {
      if ("Male" == fieldValue) {
        this.userInput["gender"] = fieldValue;
        this.Cancer_InputArr["LA1_Gender"] = fieldValue;
        console.log("Gender", this.Cancer_InputArr["LA1_Gender"]);
        console.log("CheckGender", this.userInput['gender']);
      } else {
        this.userInput["gender"] = fieldValue;
        this.Cancer_InputArr["LA1_Gender"] = fieldValue;
        console.log("Gender", this.Cancer_InputArr["LA1_Gender"]);
      }
    }
    if ("tobacco" == fieldID) {
      if ("Yes" == fieldValue) {
        this.userInput["tobacco"] = fieldValue;
        this.Cancer_InputArr["LA1_Tobacco"] = fieldValue;
        console.log("tobacco", this.Cancer_InputArr["LA1_Tobacco"]);
        console.log("ChecTobacco", this.userInput['tobacco']);
      } else {
        this.userInput["tobacco"] = fieldValue;
        this.Cancer_InputArr["LA1_Tobacco"] = fieldValue;
      }
    }
    this.calculatePremium();  
  }


  Fieldvalidation(fieldName, fieldValue) {
    if ("dobDate" == fieldName) {
      let exp = /(\d{2})\/(\d{2})\/(\d{4})/;
      if (exp.test(fieldValue)) {
        if (this.commonService.getAge(fieldValue) >= 18 && this.commonService.getAge(fieldValue) <= 65) {
          this.DOB = fieldValue;
          this.age = this.commonService.getAge(fieldValue);
          this.setPolicyTermDropdownprimary = this.setPolicyTermDropdown(
            this.age
          );
          console.log(">>>age", this.age);
          this.userInput["age"] = this.commonService.getAge(fieldValue);
          console.log("userInputage>>>>",this.userInput['age']);
          this.Cancer_InputArr["LA1_DOB"] = fieldValue;
          this.userInput['age'] = fieldValue;
          console.log("userInputage>>>>",this.userInput['age']);
          console.log("age", this.Cancer_InputArr["LA1_DOB"]);
          this.flag["Inputfields"]["dateofbirth"] = true;
          if (this.flag["Inputfields"]["dateofbirth"] &&
            (this.flag["Inputfields"]["Male"] || this.flag["Inputfields"]["Female"]) &&
            (this.flag["Inputfields"]["TobaccoYes"] || this.flag["Inputfields"]["TobaccoNo"])
          ) {
            this.flag['showHide']['disableBtn'] = false;
          }
          this.calculatePremium();  
          return "";
        } else {
          this.flag["Inputfields"]["dateofbirth"] = false;
          if (this.flag["Inputfields"]["dateofbirth"] &&
            (this.flag["Inputfields"]["Male"] || this.flag["Inputfields"]["Female"]) &&
            (this.flag["Inputfields"]["TobaccoYes"] || this.flag["Inputfields"]["TobaccoNo"])
          ) {
            this.flag['showHide']['disableBtn'] = true;
          }
          return "This plan is available for age 18 to 65";
        }
      }else {
        return "This plan is available for age 18 to 65";
      }
    }else if ("first-name" == fieldName) {
      let letterRegx = /^(?=.{2,20}$)(([a-zA-Z ])\2?(?!\2))+$/;
      // let letterRegx = /^(?!.*([A-Za-z])\1{2})([A-Za-z]*)([A-Za-z]+((\s){0,1}))*([A-Za-z])+$/;

      // var TWO_CONSEC_IDENTICAL_CHAR_VALIDATION = /(.)\1\1/;
      
      if (!letterRegx.test(fieldValue)) {
        // if(TWO_CONSEC_IDENTICAL_CHAR_VALIDATION.test(fieldValue))
        this.flag["Inputfields"]["validName"] = false;
        return "Enter your name";
      }else if(letterRegx.test(fieldValue)) {
        // if(!TWO_CONSEC_IDENTICAL_CHAR_VALIDATION.test(fieldValue))
        this.flag["Inputfields"]["validName"] = true; 
        return "";
      }
    }else if ("mobile-number" === fieldName || fieldName === "mobileNo") {
      let letterRegx = /^[6789]\d{9}$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validMobNo"] = false;
        return "Enter 10 digit mobile number";
      }else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validMobNo"] = true;
        this.userInput['mobile'] = fieldValue; 
        return "";
      }
    }else if ("email" == fieldName) {
      let letterRegx = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validEmail"] = false;
        return "Enter valid email id";
      } else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validEmail"] = true;
        return "";
      }
    }else if ("pincode" == fieldName) {
      let letterRegx = /^[1-9][0-9]{5}$/;
      if (!letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validPincode"] = false;
        return "Enter 6 digit pin code";
      }else if (letterRegx.test(fieldValue)) {
        this.flag["Inputfields"]["validPincode"] = true;
        return "";
      }
    }
  }

  onChangeDropdown(evt, fieldID) {
    let fieldValue = evt.target.value;
    this.userInput["Frequency"] = fieldValue;
    if ("policyterm" == fieldID) {
      this.Cancer_InputArr["PolicyTerm"] = fieldValue;
    } else if ("Frequency" == fieldID) {
      this.Cancer_InputArr["Frequency"] = fieldValue;
    } else if ("annualPackage" == fieldID) {
      this.userInput["annualPackage"] = fieldValue;
    }
    this.calculatePremium();  
  }

  setPolicyTermDropdown(age) {
    let policyterm = [];
    if (75 - age >= 40) {
      for (let i = 5; i <= 40; i++) {
        policyterm.push(i);
      }
    } else {
      for (let i = 5; i <= 75 - age; i++) {
        policyterm.push(i);
      }
      if (75 - age <= 20) {
        this.Cancer_InputArr["PolicyTerm"] = (75 - age).toString();
        // this.subject.next(this.HeartCancer_InputArr);
      }
    }
    return policyterm;
  }

  calculatePremium(): void {
    if(this.Cancer_InputArr['CoverageOption'] === 'Cancer'){
      this.Cancer_InputArr['LA1_HeartSumAssured'] = '0';
      this.Cancer_InputArr['LA2_HeartSumAssured'] = '0';
    }
    else if(this.Cancer_InputArr['CoverageOption'] === 'Heart'){
      this.Cancer_InputArr['LA1_CancerSumAssured'] = '0';
      this.Cancer_InputArr['LA2_CancerSumAssured'] = '0';
    }

    var temp_freq = this.Cancer_InputArr['Frequency'];
    // For yearly monthly premium
    //For nypremium
    var temp_dob = this.Cancer_InputArr["LA1_DOB"] ;
    var temp_dob_split = temp_dob.split("/");
    var plusyr = parseInt(temp_dob_split[2])+1;
    var temp_plusone_dob = temp_dob_split[0]+"/"+temp_dob_split[1]+"/"+plusyr;
    if(this.CallEbi.calculateAge(temp_plusone_dob) >=18){
      this.Cancer_InputArr['LA1_DOB'] = temp_plusone_dob;
    }
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.nypremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    this.Cancer_InputArr['LA1_DOB'] = temp_dob;

    //For Monthlly
    this.Cancer_InputArr['Frequency'] = "Monthly";
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    // this.dataLayerArray.MonthlyPremium = this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]

    //For Yearly
    this.Cancer_InputArr['Frequency'] = "Yearly";
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    
    this.Cancer_InputArr['Frequency'] = temp_freq;

    //For benefits
    this.premiumHeartCancerEbi = this.CallEbi.HeartCancerEBI(
      this.Cancer_InputArr
    );
    this.premium = this.CallEbi.calculateEBI(
      this.Cancer_InputArr,
      this.premiumHeartCancerEbi
    );
    this.premiumAmount = this.commonService.addCommas(
      this.premium["LA1_first_premium"] + this.premium["LA2_first_premium"]
    );
    
    this.sumAssured = this.commonService.valueRoundup(
      parseInt(this.Cancer_InputArr.LA1_HeartSumAssured) +
        parseInt(this.Cancer_InputArr.LA1_CancerSumAssured)
    );

    this.newPremiumAmount = Math.round(this.premium["LA1_first_premium"]);
    this.oldPremiumAmount = Math.round(this.newPremiumAmount / 0.95);
    this.discountAmount = Math.round(this.oldPremiumAmount - this.newPremiumAmount);
  }

  updateHeartCancerInputArrValue(key, value) {
    this.ValidateHSA_LA1();
    this.Cancer_InputArr[key] = value;
    this.Boundary_Conditions[key] = value;
    this.subject.next(this.Cancer_InputArr);
    
    // this.subject.next(this.Boundary_Conditions);
  }

  ValidateHSA_LA1() {
    console.log("inside validate Hsa_la1")
    // sa=parseInt(sa);
    var age = this.age;
    console.log("age>>>>>",age);
    if(age>=18 && age<=50) {
      // console.log("inside below 50 years");
      this.Boundary_Conditions['HSA_Max_LA1'] = 2500000;
      // console.log("boundary_Conditions",this.Boundary_Conditions['HSA_Max_LA1']);
    }else if(age>=51 && age<=55) {
      // console.log("inside above 51 to 55");
      this.Boundary_Conditions['HSA_Max_LA1'] = 2000000;
    }else if(age>=56 && age<=65) {
      // console.log("inside above 56 and 65");
      this.Boundary_Conditions['HSA_Max_LA1'] = 1000000;
    }
  }

  toggle() {
    this.invokeEvent.next('editTaxes');
  }
  
  showOTP() {
    this.invokeEvent1.next('editTaxes1')
  }

  showCongrats() {
    this.invokeEvent2.next('editTaxes2')
  }
}
