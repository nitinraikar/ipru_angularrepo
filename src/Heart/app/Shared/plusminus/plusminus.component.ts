import { Component, OnInit } from '@angular/core';
import { TempServiceService } from '../../Service/temp-service.service';
import { CommonService } from 'src/Common/services/common.service';

@Component({
  selector: 'app-plusminus',
  templateUrl: './plusminus.component.html',
  styleUrls: ['./plusminus.component.css']
})
export class PlusminusComponent implements OnInit {
  sumAssured;
  constructor(public tempservice: TempServiceService, public commonService: CommonService) {    
    this.tempservice.subject.subscribe(() => {
        this.sumAssured = this.commonService.valueRoundup(
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']
        );
        console.log("dghsvbjhdv",this.sumAssured);
    });
  }

  ngOnInit() {
    this.sumAssured = this.commonService.valueRoundup(
      this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']
    );
  }


  incDec(objPrnt): void {
    let incDecVal = 250000;
    let ipVal;
    ipVal = this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'];
    ipVal = parseInt(ipVal);
    if ('plus' === objPrnt) {
      if (ipVal < 100000) {
        incDecVal = 10000;
      } else if (ipVal < 1000000) {
        incDecVal = 100000;
      } else {
        incDecVal = 500000;
      }
      ipVal = parseInt(ipVal) + incDecVal;
    } else if (objPrnt === 'minus') {
      if (ipVal <= 100000) {
        incDecVal = 10000;
      } else if (ipVal <= 1000000) {
        incDecVal = 100000;
      } else {
        incDecVal = 500000;
      }
      ipVal = parseInt(ipVal) - incDecVal;
    }
    this.minMaxValue(ipVal, 'heart');
  }

  onFocusField(evt) {
    console.log("Inside Focus event");
    const fieldValue = evt.target.value;
    this.sumAssured = this.commonService.getValueFromStr(fieldValue);
    console.log("sadsa", this.sumAssured);
  }

  onBlurField(evt) {
    console.log("Inside Blur event");
    var fieldValue = evt.target.value;
    this.minMaxValue(fieldValue, 'heart');
    // this.minMaxValue(evt.target.value, 'heart');
    // this.tempStorage.OnchangeDefaultParam('heart cover')
  }

  onKeyUp(evt) {
    console.log("Inside OnKeyup event");
    let v = evt.target.value;
    this.sumAssured = v.replace(/[^0-9]/g, '');
  }

  minMaxValue(ipVal, fieldID) {
    console.log("inside MinMax Function");
    if ('heart' === fieldID) {
      console.log("Inside Cancer check Fun>>>");
      if (this.tempservice.userInput['age'] == "") {
        console.log("Inside 111+++++++ Condtion");
        if (ipVal == "" || ipVal == undefined) {
          console.log(">>>>>>1")
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'];
        }
        // else if(ipVal > this.tempservice.Boundary_Conditions['HSA_Max_LA1']) {
        //   console.log("inside boundary_CheckCondition ");
        //   // this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.Boundary_Conditions['HSA_Max_LA1']
        //   this.tempservice.updateHeartCancerInputArrValue(
        //     'LA1_HeartSumAssured',
        //     this.tempservice.Boundary_Conditions['HSA_Max_LA1']
        //   );
        // }
        else if (ipVal < this.tempservice.constants['MIN_HeartCover']) {
          console.log(">>>>>>2");
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']
        }
        else if (ipVal > this.tempservice.constants['MAX_HeartCover']) {
          console.log(">>>>>>3");
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.constants['MAX_HeartCover'];
        } else {
          console.log(">>>>>>4");
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = ipVal.toString();
        }
        this.sumAssured = this.commonService.valueRoundup(this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']);
        console.log("this.sumAssured", this.sumAssured);
      }
      else if (this.tempservice.userInput['age'] !== "") {
        console.log("inside 2nd condition");
        if (ipVal == "" || ipVal == undefined) {
          console.log(">>>>>>1")
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'];
        }
        else if (ipVal > this.tempservice.Boundary_Conditions['HSA_Max_LA1']) {
          console.log("inside boundary_CheckCondition ");
          // this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.Boundary_Conditions['HSA_Max_LA1']
          this.tempservice.updateHeartCancerInputArrValue(
            'LA1_HeartSumAssured',
            this.tempservice.Boundary_Conditions['HSA_Max_LA1']
          );
        }
        else if (ipVal < this.tempservice.constants['MIN_HeartCover']) {
          console.log(">>>>>>2");
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']
        }
        else if (ipVal > this.tempservice.constants['MAX_HeartCover']) {
          console.log(">>>>>>3");
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = this.tempservice.constants['MAX_HeartCover'];
        } else {
          console.log(">>>>>>4");
          this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] = ipVal.toString();
        }
        this.sumAssured = this.commonService.valueRoundup(this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']);
        console.log("this.sumAssured", this.sumAssured);
      }
      this.tempservice.calculatePremium();
    }
  }
}
