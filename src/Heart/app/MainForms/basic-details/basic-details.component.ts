import { Component, OnInit } from '@angular/core';
import { TempServiceService } from '../../Service/temp-service.service';

@Component({
  selector: 'app-basic-details',
  templateUrl: './basic-details.component.html',
  styleUrls: ['./basic-details.component.css']
})
export class BasicDetailsComponent implements OnInit {
  dobErrorMsg;

  constructor(public tempservice: TempServiceService) {
    console.log("const basic details called");
    // this.tempservice.subject.subscribe(() => {
    //   this.tempservice.userInput['age'] = this.tempservice.age; 
    // });
   }

  ngOnInit() {
  }

  // function to move into personalPage
  nextPage() {
    this.tempservice.flag['showHide']['personalPage'] = true;
    this.tempservice.flag['showHide']['IEBasicDetailsPage'] = false;
    this.tempservice.flag['showHide']['IEPersonalDetailsPage'] = true;
  }

  backToQuote() {
    this.tempservice.showHideMobile = false;
    this.tempservice.flag['showHide']['quoteSection'] = true;
  }

  // function to check Dob
  onKeyPress(evt, fieldID) {
    const fieldValue = evt.target.value;
    this.dobErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
    if (this.dobErrorMsg == "") {
      this.tempservice.flag['showHide']['disableBtn'] = false;
      this.tempservice.ValidateHSA_LA1();
      if(this.tempservice.userInput['age'] >= 56 && this.tempservice.userInput['age'] <= 65)
      this.tempservice.updateHeartCancerInputArrValue(
        'LA1_HeartSumAssured',
        this.tempservice.Boundary_Conditions['HSA_Max_LA1']
      );
      else if(this.tempservice.userInput['age'] >= 51 && this.tempservice.userInput['age'] <= 55)
      this.tempservice.updateHeartCancerInputArrValue(
        'LA1_HeartSumAssured',
        this.tempservice.Boundary_Conditions['HSA_Max_LA1']
      );
    } else {
      this.tempservice.flag['showHide']['disableBtn'] = true;
    }
  }

  showDialogExistingCustomer() {
    console.log("Inside function call 1");
    // this.banner.showPopUpExistingCustomer();
    if (this.dobErrorMsg == "") {
      this.tempservice.toggle();
      }else {
        this.dobErrorMsg = "Please Enter Date of Birth";
      }
  }
  showDialogExistingCongrats() {
    this.tempservice.showCongrats();
  }

}
