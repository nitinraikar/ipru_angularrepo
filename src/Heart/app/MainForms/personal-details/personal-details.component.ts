import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { TempServiceService } from 'src/app/Service/temp-service.service';
// import { BlowfishEncryptionService } from 'src/app/Service/blowfish-encryption.service';
// import { HttpClient } from '@angular/common/http';
// import{Http} from '@angular/http'
// import { BlowfishEncryptionService } from '../../Service/blowfish-encryption.service';
import { TempServiceService } from '../../Service/temp-service.service';
import { BlowfishEncryptionService } from 'src/Common/services/blowfish-encryption.service';
import { CommonService } from 'src/Common/services/common.service';
// import { HttpErrorResponse } from '@angular/common/http';
// import { RequestOptions } from '@angular/http';
// import { Headers } from '@angular/http';
// import $ from "jquery";
declare const require: any;
var $ = require("jquery");
@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})
export class PersonalDetailsComponent implements OnInit {
  mobileErrorMsg: any = '';
  nameErrorMsg: any = '';
  emailErrorMsg: any = '';
  pincodeErrorMsg: any = '';
  Glb_keyword = "";
  Glb_matchtype = ""
  sumAssured;
  @ViewChild('CancerJsonForm') CancerJsonForm: ElementRef;

  @ViewChild('bolData') bolData: ElementRef;

  /* Instance Variables for BuyNow Process */
  ProceedAppFlag: String = 'No';
  Glb_Staff: String = 'No';
  Glb_UID: String = '480';

  constructor(public tempservice: TempServiceService, private blowfishEncrypt: BlowfishEncryptionService,public commonService: CommonService) {
    console.log("const personal details called");
   }

  ngOnInit() {
    this.sumAssured = this.commonService.valueRoundup(this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']);
  }
  // Function To return To firstPage
  backToFirst() {
    this.tempservice.flag['showHide']['personalPage'] = false;
    this.tempservice.flag['showHide']['IEBasicDetailsPage'] = true;
    this.tempservice.flag['showHide']['IEPersonalDetailsPage'] = false;
  }

  onKeyUp(event, fieldID) {
    const fieldValue = event.target.value;
    if (fieldID === 'first-name') {
      this.nameErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.nameErrorMsg == "") {
        this.tempservice.flag['checkValidate']['nameDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['nameDetail'] = false;
        // this.nameErrorMsg = "Enter full name";
      }
    } else if (fieldID === 'mobile-number') {
      let v = event.target.value;
      event.target.value = v.replace(/[^0-9]/g, '');
      this.mobileErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.mobileErrorMsg == "") {
        this.tempservice.flag['checkValidate']['mobileDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['mobileDetail'] = false;
      }
    } else if (fieldID === 'email') {
      this.emailErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.emailErrorMsg == "") {
        this.tempservice.flag['checkValidate']['emailDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['emailDetail'] = false;
      }
    } else if (fieldID === 'pincode') {
      this.pincodeErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.pincodeErrorMsg == "") {
        this.tempservice.flag['checkValidate']['pincodeDetail'] = true;
      } else {
        this.tempservice.flag['checkValidate']['pincodeDetail'] = false;
      }
    }
  }

  continueToAppform() {
    if (this.tempservice.flag['checkValidate']['mobileDetail'] && this.tempservice.flag['checkValidate']['nameDetail']
      && this.tempservice.flag['checkValidate']['emailDetail'] && this.tempservice.flag['checkValidate']['pincodeDetail'] &&
      this.tempservice.flag['Inputfields']['TermsAndCond']) {
        this.callMethod();
      if (this.tempservice.SEM_WS_call_flag == 0) {
        console.log("inside SEM Flag");
        this.fireLead();
      }
      // this.callMethod();
    } else {
      if (!this.tempservice.flag['checkValidate']['mobileDetail']) {
        this.mobileErrorMsg = "Enter 10 digit mobile number";
      } if (!this.tempservice.flag['checkValidate']['nameDetail']) {
        this.nameErrorMsg = "Enter your name";
      } if (!this.tempservice.flag['checkValidate']['emailDetail']) {
        this.emailErrorMsg = "Enter valid email id";
      } if (!this.tempservice.flag['checkValidate']['pincodeDetail']) {
        this.pincodeErrorMsg = "Enter 6 digit pin code";
      }
    }
  }

  changeCheked(): void {
    this.tempservice.flag['InfoSubmitFlag'] = !this.tempservice.flag['InfoSubmitFlag'];
    this.tempservice.flag['Inputfields']['TermsAndCond'] = this.tempservice.flag['InfoSubmitFlag'];
  }

  callMethod() {
    this.ProceedAppFlag = 'Yes';

    let splitDate = this.tempservice.Cancer_InputArr['LA1_DOB'].split('/');
    let temp_Current_dob = `${splitDate[0]}${'-'}`;

    if (splitDate[1] == '01') temp_Current_dob += 'JAN-';
    else if (splitDate[1] == '02') temp_Current_dob += 'FEB-';
    else if (splitDate[1] == '03') temp_Current_dob += 'MAR-';
    else if (splitDate[1] == '04') temp_Current_dob += 'APR-';
    else if (splitDate[1] == '05') temp_Current_dob += 'MAY-';
    else if (splitDate[1] == '06') temp_Current_dob += 'JUN-';
    else if (splitDate[1] == '07') temp_Current_dob += 'JUL-';
    else if (splitDate[1] == '08') temp_Current_dob += 'AUG-';
    else if (splitDate[1] == '09') temp_Current_dob += 'SEP-';
    else if (splitDate[1] == '10') temp_Current_dob += 'OCT-';
    else if (splitDate[1] == '11') temp_Current_dob += 'NOV-';
    else if (splitDate[1] == '12') temp_Current_dob += 'DEC-';
    temp_Current_dob += splitDate[2];
    const PropInfDob = temp_Current_dob;

    splitDate = this.tempservice.Cancer_InputArr['LA2_DOB'].split('/');
    temp_Current_dob = `${splitDate[0]}${'-'}`;
    if (splitDate[1] == '01') temp_Current_dob += 'JAN-';
    else if (splitDate[1] == '02') temp_Current_dob += 'FEB-';
    else if (splitDate[1] == '03') temp_Current_dob += 'MAR-';
    else if (splitDate[1] == '04') temp_Current_dob += 'APR-';
    else if (splitDate[1] == '05') temp_Current_dob += 'MAY-';
    else if (splitDate[1] == '06') temp_Current_dob += 'JUN-';
    else if (splitDate[1] == '07') temp_Current_dob += 'JUL-';
    else if (splitDate[1] == '08') temp_Current_dob += 'AUG-';
    else if (splitDate[1] == '09') temp_Current_dob += 'SEP-';
    else if (splitDate[1] == '10') temp_Current_dob += 'OCT-';
    else if (splitDate[1] == '11') temp_Current_dob += 'NOV-';
    else if (splitDate[1] == '12') temp_Current_dob += 'DEC-';
    temp_Current_dob = `${temp_Current_dob}${splitDate[2]}`;
    let SpouseDob = temp_Current_dob;
    let JointLifePolicyFor;
    let jointLifeFlag;
    //let Coverage_Option;
    let LoyaltyBenefit;
    let FamilyBenefit;
    //let premiumterm;
    //let ExistingCustomerToPass;
    let spouseOpted;
    let spouseGender = this.tempservice.Cancer_InputArr['LA2_Gender'];
    let LA2_HeartSumAssured = this.tempservice.Cancer_InputArr[
      'LA2_HeartSumAssured'
    ];
    let LA2_CancerSumAssured = this.tempservice.Cancer_InputArr[
      'LA2_CancerSumAssured'
    ];

    if (this.tempservice.Cancer_InputArr['FamilyBenefit'] == 'Yes') {
      jointLifeFlag = 'Y';
      JointLifePolicyFor = 'self';
      spouseOpted = '1';
      FamilyBenefit = 'true';
    } else {
      jointLifeFlag = 'N';
      JointLifePolicyFor = 'false';
      spouseOpted = '0';
      FamilyBenefit = 'false';
      // SpouseDob = '';
      spouseGender = '';
      LA2_HeartSumAssured = '';
      LA2_CancerSumAssured = '';
    }
    const premiumPayingFrequency = this.tempservice.Cancer_InputArr[
      'Frequency'
    ];

    const Primary_FullName = this.tempservice.userInput['name'];
    let Primary_FirstName = Primary_FullName.substr(
      0,
      Primary_FullName.indexOf(' ')
    );
    let Primary_LastName = Primary_FullName.substr(
      Primary_FullName.indexOf(' ') + 1
    );
    if (Primary_FirstName == '') {
      Primary_FirstName = Primary_LastName;
      Primary_LastName = '';
    }

    let premiumPayingTerm = this.tempservice.Cancer_InputArr['PolicyTerm'];
    let productId = 'T49';
    if (this.tempservice.Cancer_InputArr['PPT'] == 'Single Pay') {
      premiumPayingTerm = '1';
      productId = 'T48';
    }
    let isStaff;
    if (this.Glb_Staff == 'Yes') {
      isStaff = 1;
    } else {
      isStaff = 0;
    }

    let coverageOption;
    if (
      this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] != 0 &&
      this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] != 0
    ) {
      coverageOption = 'Heart and Cancer';
    } else if (
      this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] == 0 &&
      this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] != 0
    ) {
      coverageOption = 'Cancer';
    } else if (
      this.tempservice.Cancer_InputArr['LA1_HeartSumAssured'] != 0 &&
      this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'] == 0
    ) {
      coverageOption = 'Heart';
    }
    if (this.tempservice.Cancer_InputArr['LoyaltyBenefit'] == 'Yes')
      LoyaltyBenefit = 'true';
    else LoyaltyBenefit = 'false';

    let TRAI_FLAG;
    if (this.tempservice.flag['InfoSubmitFlag']) TRAI_FLAG = 'YES';
    else TRAI_FLAG = 'NO';

    const tobacco =
      this.tempservice.Cancer_InputArr['LA1_Tobacco'] == 'Yes' ? 1 : 0;
    const tobaccoJL =
      this.tempservice.Cancer_InputArr['LA2_Tobacco'] == 'Yes' ? 1 : 0;
    let IsCalculateXRT = 'false';
    if (tobacco) IsCalculateXRT = 'true';
    else if (tobaccoJL) IsCalculateXRT = 'true';

    let UserDataJSON = {
      source: 'BOLPARTNER',
      sourceKey: 'BOLPARTNERKEY',
      advisorCode: '',
      dependentFlag: 'N',
      jointLifeFlag: jointLifeFlag,
      uidId: this.tempservice.Cancer_InputArr.UID,
      proposerInfos: {
        frstNm: Primary_FirstName,
        lstNm: Primary_LastName,
        mrtlSts: '696',
        dob: PropInfDob,
        gender: this.tempservice.Cancer_InputArr['LA1_Gender'],
        email: this.tempservice.userInput['email'],
        isStaff: isStaff,
        mobNo: this.tempservice.userInput['Mobile'],
        "comunctnAddress": {
          "pincode": this.tempservice.userInput['pincode'],
        },
        kycDoc: {
          idPrf: 'PAN',
          addPrf: 'Bank Letter',
          agePrf: 'PAN',
          itPrf: 'pancard',
          incomePrf: 'ITRETN',
          lddIdOthrDesc: '',
          lddIdNumber: '',
          lddIdExpiryDate: ''
        }
      },
      lifeAssrdInfos: {
        frstNm: '',
        lstNm: '',
        mrtlSts: '696',
        dob: SpouseDob,
        gender: this.tempservice.Cancer_InputArr['LA2_Gender'],
        isStaff: '0',
        mobNo: '',
        kycDoc: {
          idPrf: '',
          addPrf: '',
          agePrf: 'PAN',
          itPrf: ''
        }
      },
      productSelection: {
        premiumPayingFrequency: premiumPayingFrequency,
        policyTerm: this.tempservice.Cancer_InputArr['PolicyTerm'],
        premiumPayingTerm: premiumPayingTerm,
        salesChannel: '',
        productType: 'Health',
        productName: 'HeartCancer',
        productId: productId,
        premiumpaymentoption: this.tempservice.Cancer_InputArr['PPT'],
        storyCode: 'DN',
        tobacco: tobacco,
        tobaccoJL: tobaccoJL,
        DeathBenefitHeart: this.tempservice.Cancer_InputArr[
          'LA1_HeartSumAssured'
        ],
        DeathBenefitCancer: this.tempservice.Cancer_InputArr[
          'LA1_CancerSumAssured'
        ],
        DeathBenefitHeartJL: LA2_HeartSumAssured,
        DeathBenefitCancerJL: LA2_CancerSumAssured,
        IsCalculateXRT: IsCalculateXRT,
        Heart_XRT: String(tobacco),
        Cancer_XRT: String(tobacco),
        HeartJL_XRT: String(tobaccoJL),
        CancerJL_XRT: String(tobaccoJL),
        JointLifePolicyFor: JointLifePolicyFor,
        spouseOpted: spouseOpted,
        spouseDOB: SpouseDob,
        spouseGender: spouseGender,
        coverageOption: coverageOption,
        // additionalBenefits: this.BenefitsString(),
        familyBenefit: FamilyBenefit,
        loyaltyBenefit: LoyaltyBenefit,
        traiFlag: TRAI_FLAG
      },
      "partnerRedirectUrl": window.location.href,
      "paymentData": {
        "pincode": this.tempservice.userInput['pincode'] != ("" || undefined) ? this.tempservice.userInput['pincode'] : "400097",
        "extraAtsInfo": {
          "fname": Primary_FirstName != "" ? Primary_FirstName : "abc",
          "lname": Primary_LastName != "" ? Primary_LastName : "xyz",
          "dob_str": PropInfDob,
          "gender": this.tempservice.Cancer_InputArr['LA1_Gender'],
          "sa": parseInt(this.tempservice.Cancer_InputArr['LA1_HeartSumAssured']) + parseInt(this.tempservice.Cancer_InputArr['LA1_CancerSumAssured'])
        }
      }
    };
    let EncryptedData = this.blowfishEncrypt.encryptData(JSON.stringify(UserDataJSON), '!t@a6@($%(', "encrypt");
    this.bolData.nativeElement.value = EncryptedData;
    let CancerForm = this.CancerJsonForm.nativeElement;
    // CancerForm.action = "https://www.iprusalesbeta.com/digiuat/PartnerIntegration.htm"; /*DigiDrive UAT Temp*/
    CancerForm.action = "https://buy.iciciprulife.com/buy/PartnerIntegration.htm"; /*DigiDrive Prod*/
    CancerForm.submit();
  }

  fireLead() {

    this.tempservice.SEM_WS_call_flag = 1;
    var google_search_q = "";
    //var google_search_q = ipru_getParameterByName('q', document.referrer);
    if (!google_search_q)
      google_search_q = '';

    if (this.tempservice.flag['Inputfields']['traiFlag'])
      var TRAI_FLAG = 'YES';
    else
      var TRAI_FLAG = 'NO';

    var EC_Status = this.tempservice.flag['Inputfields']['existingCustomeradded'] ? "Yes" : "No";

    google_search_q = google_search_q.replace(/[\,\-\'\"\s]/g, '_');
    var product_specific = 'google_search_q-' + google_search_q + ',call-' + TRAI_FLAG + ',ExistingCustomer-' + EC_Status;

    var datyArr = this.tempservice.Cancer_InputArr['LA1_DOB'].split('/');
    // var datyArr = this.tempservice.userInput["dob"].split('/');
    var dob = datyArr[1] + '/' + datyArr[0] + '/' + datyArr[2];
    //var c2c_dob = datyArr[0] + '/' + datyArr[1] + '/' + datyArr[2];

    var tobacco = (this.tempservice.userInput["tobacco"] == "Yes") ? 1 : 0;

    var objdata = {
      'LMS_uid': this.tempservice.Cancer_InputArr["UID"],
      'LMS_product_code': 'T48',
      'LMS_gender': this.tempservice.userInput["gender"],
      'LMS_dob': dob,
      'LMS_mobile': this.tempservice.userInput["mobile"],
      'LMS_email': this.tempservice.userInput["email"],
      'LMS_product_specific': product_specific,
      'LMS_page_name': 'Website-Heart-Calculator',
      'keyword': this.Glb_keyword,
      'matchtype': this.Glb_matchtype,
      'LMS_TRAI_FLAG': TRAI_FLAG,
      'LMS_firstName': this.tempservice.userInput["name"],
      'LMS_lastName': "",
      'LMS_tobacco': tobacco,
      'LMS_premium': this.tempservice.premiumAmount,
      'LMS_policypaymentfrequency': this.tempservice.Cancer_InputArr["Frequency"],
      'LMS_policyTerm': this.tempservice.Cancer_InputArr["PolicyTerm"],
      'LMS_sumassuredPlus': this.tempservice.Cancer_InputArr["LA1_HeartSumAssured"],
      'LMS_sumassuredMinus': this.tempservice.Cancer_InputArr["LA1_CancerSumAssured"],
      'LMS_sumassured': parseInt(this.tempservice.Cancer_InputArr["LA1_HeartSumAssured"]) + parseInt(this.tempservice.Cancer_InputArr["LA1_CancerSumAssured"])
    };
    $.ajax({
      url: "https://buy.iciciprulife.com/buy/leadWS.htm", 
      type:"POST",
      crossDomain: true,
      xhrFields: {
        withCredentials: true
      },
      data: $.param(objdata),
      success: function(res){ 
        console.log('Response=>' + res);
            
      },
      error: function(error){
        console.log('Error Response=> ' + error);
        
      },
    });


    // this.http.post('https://buy.iciciprulife.com/buy/leadWS.htm', objdata).subscribe(
    //       res => {
    //         console.log('Response=>' + res);
    //       },
    //       (err: HttpErrorResponse) => {
    //           console.log('Error Response=> ' + err);
    //       }
    //   );

    // $.ajax({
    //   url: "https://buy.iciciprulife.com/buy/leadWS.htm", 
    //   type:"POST",
    //   crossDomain: true,
    //   xhrFields: {
    //     withCredentials: true
    //   },
    //   data: $.param(objdata),
    //   success: function(res){ 
    //     console.log('Response=>' + res);
    //         // thisobj.otpApiCallFailed = false;
    //         // thisobj.otpGenerated = true;
    //         // thisobj.otpVerificationFailed = false;
    //         // //returnStatus = true;
    //         // var result = JSON.parse(res);
    //         // thisobj.clientId = result.clientId;

    //         // if(result.clientId=="No Client found for parematers passed." || (result.IPruException !== undefined && result.IPruException.status=="ERROR")){
    //         //   thisobj.otpVerificationFailed = true;
    //         //   //returnStatus = false ;
    //         // }else{
    //         //   thisobj.otpGenerated = true;
    //         //   thisobj.encodedmobile = result.mobileNo;
    //         //   //returnStatus = true;
    //         //   //return true;
    //         //   thisobj.Existingcustsucc();

    //         // }
    //   },
    //   error: function(error){
    //     console.log('Error Response=> ' + error);
    //     // thisobj.otpApiCallFailed = true;
    //     // thisobj.otpGenerated = false;
    //     // thisobj.otpVerificationFailed = false;
    //     // //returnStatus = false;
    //   },
    // });



    //var url = "https://www.iprusalesbeta.com/SellOnline_OTC/leadWS.htm"; /*DigiDrive UAT*/
    //var url = "https://www.iprusalesbeta.com/digiuat/leadWS.htm"; /*Temp DigiDrive UAT*/
    //var url = "https://beta.iciciprulife.com/DigiDrive/leadWS.htm"; /*DigiDrive PreProd*/
    // var url = "https://buy.iciciprulife.com/buy/leadWS.htm"; /*Production*/

    // //this.postIframe(url, "POST", objdata);
    // var stringobj = JSON.stringify(objdata);
    // var headers = new Headers();
    // headers.append('Content-Type', 'application/json');
    // headers.append('Content-Type', 'application/json');
    // let options = new RequestOptions({ headers: headers });

    // this.http.post(url, objdata).subscribe(
    //   res => {
    //     console.log('Response=>' + res);
    //   },
    //   (err: HttpErrorResponse) => {
    //     console.log('Error Response=> ' + err);
    //   }
    // );
    this.fireLeadConversionPixels();
    // try{
    //           var req1 = {
    //                    "message":"comition",
    //                    "properties": {
    //                               "amount": "",
    //                               "gender": this.tempservice.userInput["gender"],
    //                               "cardType": "ICICI BANK",
    //                               "mobileNo": "9999999999",
    //                               "emailID": "abc@xyz.com",
    //                               "productCode": "T49",
    //                               "vendorUid": "",
    //                               "associationId": datyArr[2]+this.tempservice.userInput["mobile"].substr(this.tempservice.userInput["mobile"].length - 5),
    //                               "premium": "",
    //                               "message": window.location.hostname+window.location.pathname,
    //                               "deviceId": "19 Jun 1989",
    //                               "ipAddress": "" //ipAddress
    //                           }
    // 			};
    //dataLogging.sendCLog(req1);
    //}catch(e){}
  }
  postIframe(target_url, method, params) {//Added for LMS calling ---- alternate way to implement cross domain ajax call by using hidden iframe -- KAUSHIK
    //Add iframe
    var iframe = document.createElement("iframe");
    document.body.appendChild(iframe);
    iframe.style.display = "none";

    //Give the frame a name
    var frame_name = "frame_name" + (new Date).getTime();
    iframe.contentWindow.name = frame_name;

    //build the form
    var form = document.createElement("form");
    form.target = frame_name;
    form.action = target_url;
    form.method = method;

    //loop through all parameters
    for (var key in params) {
      if (params.hasOwnProperty(key)) {
        var input = document.createElement("input");
        input.type = "hidden";
        input.name = key;
        input.value = params[key];
        form.appendChild(input);
      }
    }

    document.body.appendChild(form);
    form.submit();
  }

  fireLeadConversionPixels(): void {
    var gcc = new Image();
    var gcc1 = new Image();
    var rand_time = (new Date()).getTime();
    // var pixelurl = '';
    // var t;
    // var e = Glb_UID;
    // t = 'xxxxxxxx_xxxx_4xxx_yxxx_xxxxxxxxxxxx'.replace(/[xy]/g, function (e) {
    //   var t = 16 * Math.random() | 0,
    //   o = 'x' == e ? t : 3 & t | 8;
    //   return o.toString(16)
    // }),
    // t = t + '_' + (new Date).getTime();
    // var vCommId = t;
    gcc.src = window.location.protocol + '//www.googleadservices.com/pagead/conversion/964212897/?label=xe2DCLi55XAQofHiywM&amp;guid=ON&amp;script=0&rand_time=' + rand_time;
    gcc1.src = window.location.protocol + '//www.googleadservices.com/pagead/conversion/850517359/?label=T_IjCPaByHMQ77rHlQM&amp;guid=ON&amp;script=0&rand_time=' + rand_time;
  }
}



