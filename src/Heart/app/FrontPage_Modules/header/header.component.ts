import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { PopupComponent } from 'src/Common/components/popup/popup.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  addCss = false;
  visibleSidebar2 = false;
  opened;
  opnscroll = false;
  showDownCalIcon = false;
  isSelected = false;
  showReturnHomeIcon = false;
  showQuoteScroll = true;
  leftDiv = true;
  showpanel;
  expand = false;

  @ViewChild('callRequest') callRequest: PopupComponent;
  constructor() { }

  ngOnInit() {
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    const element = window.pageYOffset;
    if (element > 500) {
      this.opnscroll = true;
      this.showDownCalIcon = true;
      this.isSelected = true;
      this.changeGetQuoteButtonColor(true);
      this.expand = true;
    } else if (element < 500) {
      this.opnscroll = false;
      this.showDownCalIcon = false;
      this.isSelected = false;
      this.expand = false;
    }
    
    const bottomHeight = window.scrollY + window.innerHeight;
    if (bottomHeight > 5200) {
      this.showReturnHomeIcon = true;
    } else {
      this.showReturnHomeIcon = false;
    }
  }

  showHeader(): void {
    this.visibleSidebar2 = true;
    this.leftDiv = true;
    this.showpanel = !this.showpanel;
  }

  toggle(): void {
    this.opened = true;
  }

  show(): void {
    this.scrollFunction();
  }

  close(): void {
    this.showpanel = false;
    this.scrollFunction();
  }

	callRequestMethod(): void {
    this.showpanel = false;
    this.scrollFunction();
    this.callRequest.showDialog();
  }
  scrollFunction(): void {
    document.body.className = "";
  }

  changeGetQuoteButtonColor(add): void {
    this.addCss = add ? true : false;
  }

  Clk_Brochure() {
    //window.open('./../../content/dam/icicipru/brochures/heart_and_cancer.pdf');
    window.open('https://www.iciciprulife.com/content/dam/icicipru/brochures/heart_and_cancer.pdf');
  }
  tracker(){
    window.open("https://buy.iciciprulife.com/buy/apptrackerhomeNew.htm");
  }
  
	closecallRequest() {
     this.callRequest.closeDialog();
  }
}
