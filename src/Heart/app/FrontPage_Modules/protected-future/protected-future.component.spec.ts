import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtectedFutureComponent } from './protected-future.component';

describe('ProtectedFutureComponent', () => {
  let component: ProtectedFutureComponent;
  let fixture: ComponentFixture<ProtectedFutureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtectedFutureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtectedFutureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
