import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { trigger, transition, animate, style } from '@angular/animations';
import { TempServiceService } from '../../Service/temp-service.service';
import $ from 'jquery';
import { PopupComponent } from 'src/Common/components/popup/popup.component';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css'],
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translate3d(100%, 0, 0)' }),
        animate(400)
      ]),
    ]),
  ]
})
export class BannerComponent implements OnInit {
  otpRegenerated = false;
  otpGenerated = false;
  otpVerificationFailed = false;
  clientId;
  otpAttemptFailed = 0;
  otpApiCallFailed = false;

  mobileStructure;
  desktopStructure;
  showHideMobile;
  showHideMobilePersonal;
  quoteSection = true;
  mobileErrorMsg: any = "";
  otpErrorMsg: any = "";
  otpExistingcust = false;
  otpExistingcustval;
  encodedmobile;
  paymentFrequency;
  policyTerm;

  @ViewChild('existingcustdetails') existingcustdetails: PopupComponent;
  @ViewChild('otp') otp: PopupComponent;
  @ViewChild('congratulations') congratulations: PopupComponent;
  @ViewChild('mobileNo') mobileNo: ElementRef;

  constructor(public tempservice: TempServiceService) {
    this.tempservice.invokeEvent.subscribe(value => {
      if (value === 'editTaxes') {
        this.showPopUpExistingCustomer();
      }
    });
    this.tempservice.invokeEvent1.subscribe(value => {
      if (value === 'editTaxes1') {
        this.showPopUpExistingCustomerOPT();
      }
    });
    this.tempservice.invokeEvent2.subscribe(value => {
      if (value === 'editTaxes2') {
        console.log("inside 3 rd popup");
        this.showPopUpExistingCustomerCongrats();
      }
    });
  }

  ngOnInit() {
    this.tempservice.showHideMobile = false;
    this.tempservice.showHideMobilePersonal = false;
    this.tempservice.premiumAmount = 67;
    if ((window.screen.width < 768)) {
      // console.log("inside 76888888888");
      this.tempservice.mobileStructure = true;
      this.tempservice.desktopStructure = false;
    } else {
      // console.log("inside 100000000000");
      this.tempservice.mobileStructure = false;
      this.tempservice.desktopStructure = true;
    }

    let isIE = /msie\s|trident\//i.test(window.navigator.userAgent)
    if (isIE == true) {
      console.log("in only IE browser");
      this.tempservice.IEBrowser = true;
    }
  }

  getQuote() {
    this.tempservice.showHideMobile = true;
    this.tempservice.flag['showHide']['quoteSection'] = false;
  }
  showPopUpExistingCustomer() {
    console.log("Inside function call 2");
    this.existingcustdetails.showDialog();
  }
  showPopUpExistingCustomerOPT() {
    console.log("INSIDe vvvvvv");
    this.otp.showDialog();
  }
  showPopUpExistingCustomerCongrats() {
    console.log("Inside cccccc");
    this.congratulations.showDialog();
  }

  closePopUp() {
    this.existingcustdetails.closeDialog();
  }
  closeOTP() {
    this.otp.closeDialog();
  }
  regenerateotp() {
    this.otpRegenerated = true;
  }
  gotoOTP() {
    console.log("getotp");
    // this.existingcustdetails.closeDialog();  
    // this.otp.showDialog();
    // this.tempservice.showOTP();
    if (this.tempservice.flag['checkValidate']['existingMobile']) {
      this.generateOTP();
      // if(this.generateOTP()){
      //   this.existingcustdetails.closeDialog();  
      //   // this.otp.showDialog();
      //   this.tempservice.showOTP();
      // }
    } else {
      if (!this.tempservice.flag['checkValidate']['existingMobile']) {
        this.mobileErrorMsg = "Please Enter Mobile Number";
      }
    }
  }

  generateOTP() {
    // let returnStatus = false;
    let splitDate = this.tempservice.Cancer_InputArr["LA1_DOB"].split("/");
    let temp_Current_dob = splitDate[0] + "-";

    if (splitDate[1] == "01")
      temp_Current_dob += "JAN-";
    else if (splitDate[1] == "02")
      temp_Current_dob += "FEB-";
    else if (splitDate[1] == "03")
      temp_Current_dob += "MAR-";
    else if (splitDate[1] == "04")
      temp_Current_dob += "APR-";
    else if (splitDate[1] == "05")
      temp_Current_dob += "MAY-";
    else if (splitDate[1] == "06")
      temp_Current_dob += "JUN-";
    else if (splitDate[1] == "07")
      temp_Current_dob += "JUL-";
    else if (splitDate[1] == "08")
      temp_Current_dob += "AUG-";
    else if (splitDate[1] == "09")
      temp_Current_dob += "SEP-";
    else if (splitDate[1] == "10")
      temp_Current_dob += "OCT-";
    else if (splitDate[1] == "11")
      temp_Current_dob += "NOV-";
    else if (splitDate[1] == "12")
      temp_Current_dob += "DEC-";
    temp_Current_dob += splitDate[2];

    // let headers1 = new HttpHeaders();
    // headers1.set('Content-Type', 'application/x-www-form-urlencoded');

    let paramBean = {}
    paramBean["mobile"] = this.mobileNo.nativeElement.value;
    paramBean["isExistingCustomer"] = true;
    paramBean["dob"] = temp_Current_dob;
    paramBean = JSON.stringify(paramBean);
    let parameters = {};
    parameters["paramBean"] = paramBean;
    parameters["transactionName"] = "generateOtp";

    let thisobj = this;
    $.ajax({
      url: "https://buy.iciciprulife.com/buy/bolCustomerOtp.htm",
      type: "POST",
      crossDomain: true,
      xhrFields: {
        withCredentials: true
      },
      data: $.param(parameters),
      success: function (res) {
        console.log('Response=>' + res);
        thisobj.otpApiCallFailed = false;
        thisobj.otpGenerated = true;
        thisobj.otpVerificationFailed = false;
        //returnStatus = true;
        var result = JSON.parse(res);
        thisobj.clientId = result.clientId;

        if (result.clientId == "No Client found for parematers passed." || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
          thisobj.otpVerificationFailed = true;
          //returnStatus = false ;
        } else {
          thisobj.otpGenerated = true;
          thisobj.encodedmobile = result.mobileNo;
          //returnStatus = true;
          //return true;
          thisobj.Existingcustsucc();
        }
      },
      error: function (error) {
        console.log('Error Response=> ' + error);
        thisobj.otpApiCallFailed = true;
        thisobj.otpGenerated = false;
        thisobj.otpVerificationFailed = false;
        //returnStatus = false;
      },
    });

    // this.http.post('https://buy.iciciprulife.com/buy/bolCustomerOtp.htm', parameters, { headers: {'Content-Type': 'application/x-www-form-urlencoded'}, responseType: "text" }).subscribe(
    //   res => {
    //     console.log('Response=>' + res);
    //     this.otpApiCallFailed = false;
    //     this.otpGenerated = true;
    //     this.otpVerificationFailed = false;
    //     returnStatus = true;
    //     var result = JSON.parse(res);
    //     this.clientId = result.clientId;

    //     if (result.clientId == "No Client found for parematers passed." || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
    //       this.otpVerificationFailed = true;
    //       returnStatus = false;
    //     } else {
    //       this.otpGenerated = true;
    //       returnStatus = true;
    //       this.encodedmobile = result.mobileNo;
    //       this.Existingcustsucc();
    //     }
    //   },
    //   (err: HttpErrorResponse) => {
    //     console.log('Error Response=> ' + err);
    //     this.otpApiCallFailed = true;
    //     this.otpGenerated = false;
    //     this.otpVerificationFailed = false;
    //     returnStatus = false;
    //   }
    // );
    // return returnStatus;

  }

  Existingcustsucc() {
    this.existingcustdetails.closeDialog();
    this.tempservice.showOTP();
  }

  gotoCongratulations() {
    if (this.otpExistingcust) {
      this.VerifyOTP();

    } else {
      this.otpErrorMsg = "Please Enter OTP";
    }
  }

  VerifyOTP() {
    var paramBean = {};
    paramBean["otpNumber"] = this.otpExistingcustval;
    paramBean["custId"] = this.clientId;    //From generateOtp request
    paramBean = JSON.stringify(paramBean);
    var parameters = {};
    parameters["paramBean"] = paramBean;
    parameters["transactionName"] = "validateOtp";

    // let headers1 = new HttpHeaders();
    // headers1.set('Content-Type', 'application/x-www-form-urlencoded');

    // this.http.post('https://buy.iciciprulife.com/buy/bolCustomerOtp.htm', parameters, { headers: headers1, responseType: "text" }).subscribe(
    //   resp => {
    //     console.log('Response=>' + resp);
    //     var result = JSON.parse(resp['_body']);
    //     this.clientId = result.clientId;
    //     if (result.OtpVerificationStaus == "Invalid OTP" || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
    //       this.otpErrorMsg = "Uh Oh. The OTP you entered is invalid. Please check and re-enter";
    //     }
    //     else if (result.OtpVerificationStaus == "Valid OTP") {
    //       this.otpErrorMsg = "";
    //       this.Existingcust();
    //     }
    //   },
    // );

    let thisobj = this;
    $.ajax({
      url: "https://buy.iciciprulife.com/buy/bolCustomerOtp.htm",
      type: 'POST',
      crossDomain: true,
      xhrFields: {
        withCredentials: true
      },
      data: $.param(parameters),
      success: function (result) {
        result = JSON.parse(result);
        if (result.OtpVerificationStaus == "Invalid OTP" || (result.IPruException !== undefined && result.IPruException.status == "ERROR")) {
          /*$('.UserMsg').text('Uh Oh. The OTP you entered is invalid. Please check and re-enter');
          $('.StatusUser').show();*/
          //$('.OTPMsg').text('Uh Oh. The OTP you entered is invalid. Please check and re-enter');
          this.otpErrorMsg = "Uh Oh. The OTP you entered is invalid. Please check and re-enter";
          //thisobj.Existingcust();
        } else if (result.OtpVerificationStaus == "Valid OTP") {
          // $('.UserMsg').text('Your details have been verified. Thank You.');
          this.otpErrorMsg = "";
          thisobj.Existingcust();
        }
        //$('.popup_loader').hide();
      }
    })
  }

  Existingcust() {
    this.otp.closeDialog();
    this.tempservice.Cancer_InputArr['LoyaltyBenefit'] = 'Yes';
    this.tempservice.calculatePremium();
    this.tempservice.showCongrats();
    // this.congratulations.showDialog();
  }

  removeExisting() {
    this.congratulations.closeDialog();
    this.tempservice.flag['existingCustomeradded'] = false;
    this.tempservice.flag['showHide']['ExistingCus'] = false;
    this.tempservice.flag['showHide']['notExistingCus'] = true;
    this.tempservice.Cancer_InputArr['LoyaltyBenefit'] = 'No';
    console.log("existingCustomeradded", this.tempservice.flag['existingCustomeradded']);
    console.log("exisCus", this.tempservice.flag['showHide']['ExistingCus']);
    console.log("notexisCus", this.tempservice.flag['showHide']['ExistingCus']);
    this.tempservice.calculatePremium();
  }

  done() {
    this.tempservice.flag['existingCustomeradded'] = true;
    this.tempservice.flag['showHide']['ExistingCus'] = true;
    this.tempservice.flag['showHide']['notExistingCus'] = false;
    console.log("existingCustomeradded", this.tempservice.flag['existingCustomeradded']);
    console.log("exisCus", this.tempservice.flag['showHide']['ExistingCus']);
    console.log("notexisCus", this.tempservice.flag['showHide']['ExistingCus']);
    this.congratulations.closeDialog();
  }

  onKeyUp(event, fieldID) {
    var fieldValue = event.target.value;
    if ("mobileNo" == fieldID) {
      var v = event.target.value;
      event.target.value = v.replace(/[^0-9]/g, '');
      this.mobileErrorMsg = this.tempservice.Fieldvalidation(fieldID, fieldValue);
      if (this.mobileErrorMsg == "") {
        this.tempservice.flag['checkValidate']['existingMobile'] = true;
      } else {
        this.tempservice.flag['checkValidate']['existingMobile'] = false;
      }
    }
    else if ("otp" == fieldID) {
      this.otpErrorMsg = "";
      var v = event.target.value;
      event.target.value = v.replace(/[^0-9]/g, '');
      if (event.target.value.length == 6) {
        event.target.blur();
        this.otpExistingcustval = event.target.value;
        this.otpExistingcust = true;
      } else {
        this.otpErrorMsg = "Please Enter Valid OTP";
      }
    }
  }
}
