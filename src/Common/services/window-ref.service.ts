import { Injectable } from '@angular/core';
import { environment } from 'src/Cancer/environments/environment';
// import { environment } from 'HeartnCancer/src/environments/environment';
// import { environment } from 'src/environments/environment';

function _window(): any {
  return window;
}

@Injectable({
  providedIn: 'root'
})

export class WindowRefService {
  constructor() {}
  get nativeWindow(): any {
    return _window();
  }

  globalTrackingCodes(id,value){

    if (environment.production) {
      try{
        this.nativeWindow.datalayer[id] = value;
        this.nativeWindow._satellite.track("HnC_Dec");
      }catch(e){}
    }
  }

}
