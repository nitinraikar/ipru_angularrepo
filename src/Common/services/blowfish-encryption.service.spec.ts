import { TestBed, inject } from '@angular/core/testing';

import { BlowfishEncryptionService } from './blowfish-encryption.service';

describe('BlowfishEncryptionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlowfishEncryptionService]
    });
  });

  it('should be created', inject([BlowfishEncryptionService], (service: BlowfishEncryptionService) => {
    expect(service).toBeTruthy();
  }));
});
