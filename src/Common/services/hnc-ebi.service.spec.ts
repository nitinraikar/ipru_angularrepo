import { TestBed, inject } from '@angular/core/testing';

import { HncEbiService } from './hnc-ebi.service';

describe('HncEbiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HncEbiService]
    });
  });

  it('should be created', inject([HncEbiService], (service: HncEbiService) => {
    expect(service).toBeTruthy();
  }));
});
