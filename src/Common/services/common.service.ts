import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  dateMask: any;

  constructor() {
    this.dateMask = [
      /[0-9]/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      "/",
      /\d/,
      /\d/,
      /\d/,
      /\d/
    ];
  }


  getValueFromStr(val) {
    let returnvalue = "";
    let slice1 = "";
    if (val.indexOf("crores") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "0000000";
    } else if (val.indexOf("lakhs") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "00000";
    } else if (val.indexOf("crore") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "000000";
    } else if (val.indexOf("lakh") > 0) {
      slice1 = val.replace(/[^0-9]/g, "");
      returnvalue = slice1 + "0000";
    } else {
      returnvalue = val.replace(/\,/g, "");
    }
    return returnvalue;
  }

  /**Method created to convert currency number into words*/
  valueRoundup(numVal) {
    let tempObj = {};
    tempObj["actVal"] = numVal;
    let sadefault = numVal;
    let sa = "" + numVal + ""; //sa to string
    if (sa.length == 6) {
      let safront = sa.substring(0, 1);

      if (parseInt(safront) == 1) {
        tempObj["prefix"] = "lakhs";
      } else {
        tempObj["text"] = safront;
        tempObj["prefix"] = "lakhs";
      }
    } else if (sa.length > 6) {
      sa = "" + sa + "";
      sa = sa.substring(2, sa.length);
      let sum = sadefault;
      sum = "" + sum + "";
      sum = sum.substring(0, 2);
      
      if (sa.length == 5) {
        tempObj["text"] = sum;
        tempObj["prefix"] = "lakhs";
      } else if (sa.length == 6) {
        let newsa = sadefault;
        newsa = "" + newsa + "";
        newsa = newsa.substring(0, 1);
        if (parseInt(newsa) == 1) {
          tempObj["text"] = newsa;
          tempObj["prefix"] = "crore";
        } else {
          tempObj["text"] = newsa;
          tempObj["prefix"] = "crores";
        }
      } else if (sa.length == 7) {
        let newsa = sadefault;
        newsa = "" + newsa + "";
        newsa = newsa.substring(0, 2);
        let newslice = sadefault;
        newslice = "" + newslice + "";
        newslice = newslice.substring(2, 4);
        tempObj["text"] = newsa;
        tempObj["prefix"] = "crores";
      }
    } else {
      tempObj["text"] = numVal;
      tempObj["prefix"] = "";
    }

    tempObj["text"] = "" + tempObj["text"].trim();

    return tempObj["text"] + " " + tempObj["prefix"];
  }

  getAge(dob) {
    let today = new Date();
    let birthDate = new Date(
      dob.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3")
    );
    let age = today.getFullYear() - birthDate.getFullYear();
    console.log("ingetAgeMethod", age);
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  addCommas(nStr) {
    if (nStr != undefined) {
      nStr = nStr.toString().replace(/,/g, "");
      nStr = nStr.replace(/[^0-9 ]/g, "");
      let parts = nStr.toString().split(".");
      let prt1 = parts[0].slice(0, -1);
      let prt2 = parts[0].slice(-1);
      parts[0] = prt1.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + prt2;
      nStr = parts[0];

      return nStr;
    }
  }

  removeCommas(numWidCommas) {
    var numWidoutCommas = numWidCommas.replace(/[,]/g, '');
    return numWidoutCommas;
  }
}

