import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  display = false;
  forallpopup = true;
  inctaxes = false;
  isSelected = true;
  showbreakup = false;

  showDialog(): void {
    this.display = true;
  }

  closeDialog(): void {
    this.display = false;
  }

  ngOnInit(): void {}

  popuplarge(): void {
    this.isSelected = false;
  }

  changebreakup(): void {
    this.showbreakup = true;
  }
}
