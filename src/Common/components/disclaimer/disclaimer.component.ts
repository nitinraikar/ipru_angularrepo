import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.css']
})
export class DisclaimerComponent implements OnInit {
  isExpanded = false;
  constructor() { }

  ngOnInit() {
  }

  toggle(): void {
    if(this.isExpanded) {
      this.isExpanded = false;
    } else {
      this.isExpanded = true;
    }
  }

  scroll(): void {
    setTimeout(() => {
      if (this.isExpanded)
        document.getElementById('bottom')
        // .scrollIntoView();
        window.scrollBy(0,200)
    }, 150);
  }
}
